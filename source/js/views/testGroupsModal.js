define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/testGroupsModal',
    'handlebars',
    'currentApp'
], function($, _, Backbone, template, Handlebars, CurrentApp) {
    'use strict';

    var TestGroups = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "testGroupsModal",
        initialize: function() {
            this.render();
        },
        render: function() {
            var obj = CurrentApp.getModel().get("config").public.groups;
            console.log(obj);
            var groups = [];
            var letters = "ABCDEFGHIJKLMNOPQRSTUWXYZ".split("");
            var mem = 0;
            for (var i = 0; i < obj.names.length; i++) {
                
                groups[i] = {
                    name : obj.names[i],
                    desc : obj.descriptions[i],
                    ratio : Math.round( (obj.ratios[i] - mem) * 100 ),
                    letter: letters[i]
                };
                mem = obj.ratios[i];
            }
            this.$el.html(this.template({
                groups: groups
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#modal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events: {
            "click .add" : "add",
            "click .save" : "save"
        },
        add: function() {
            var t = Handlebars.compile(this.$el.find("script").html());
            this.$el.find(".modal-body").append(t());
        },
        save: function() {
            var groups = this.$el.find(".group");
            var names = [];
            var descriptions = [];
            var ratios = [];
            var mem = 0;
            groups.each(function() {
                if ($(this).find("input").length > 1) {
                    names.push($(this).find(".name").val());
                    descriptions.push($(this).find(".desc").val());
                    mem += Number($(this).find(".percent").val()) / 100
                    ratios.push( mem );
                } else {
                   names.push($(this).find(".name").text());
                    descriptions.push($(this).find(".desc").text());
                    mem += Number($(this).find(".percent").val()) / 100
                    ratios.push( mem ); 
                }
            });
            if (mem !== 1) {
                this.$el.find(".error").show();
                return;
            }
            groups = {
                names: names,
                descriptions: descriptions,
                ratios: ratios
            };
            var config = CurrentApp.getModel().get("config");
            config.public.groups = groups;
            CurrentApp.getModel().set("config", config);
            CurrentApp.getModel().save();
            this.$el.find("#modal").modal("hide");
        }
    });

    return TestGroups;
});