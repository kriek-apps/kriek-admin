/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/queryWizard',
    'handlebars'
], function($, _, Backbone, template, Handlebars) {
    'use strict';

    var QuerywizardView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "queryWizard",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#queryModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events: {
            "click .addFieldBtn": "addField",
            "click .saveChanges": "checkFields"
        },
        addField: function(event) {
            var el = $(event.currentTarget);
            var newTemplate = Handlebars.compile(this.$el.find("script.field-template").html());
            el.before(newTemplate());
        },
        checkFields: function() {
            var jsonFields = this.$el.find(".jsonField");
            for (var i = 0; i < jsonFields.length; i++) {
                var jsonField = $(jsonFields[i]);
                if (jsonField.find(".column").val() === "" && jsonField.find(".fields").val() === "") {
                    jsonField.remove();
                }
                
            }
            this.saveChanges();
        },
        saveChanges: function() {
            var inputs = this.$el.find("input");
            var data = {};
            for (var i = 0; i < inputs.length; i++) {
                var input = $(inputs[i]);
                var key = input.attr("data-key");
                if (typeof key !== "undefined" && key == "data") {
                    var subkey = input.attr("data-subkey");
                    if (subkey == "titles") {
                        data[subkey] = input.val().split(",");
                    } else {
                        data[subkey] = input.val();
                    }
                }
            }
            data.query = this.$el.find("textarea.query").val();
            data.jsonFields = [];
            var jsonFields = this.$el.find(".jsonField");
            for (var i = 0; i < jsonFields.length; i++) {
                var jsonField = $(jsonFields[i]);
                var obj = {};
                obj.column = jsonField.find(".column").val();
                obj.fields = jsonField.find(".fields").val().split(",");
                data.jsonFields[i] = obj;
            }
            var appId = this.$el.find("input.isGlobal").is(":checked") ? 0 : this.model.appId;
            var Public = (this.$el.find("input.isPublic").is(":checked")) ? 1 : 0;
            this.model.set("data", data);
            this.model.set("app_id", appId);
            this.model.set("public", Public);
            var self = this;
            this.model.save({}, {success: function() {
                self.$el.find("#queryModal").modal("hide");
                self.trigger("saved");
            }});

        }
    });

    return QuerywizardView;
});