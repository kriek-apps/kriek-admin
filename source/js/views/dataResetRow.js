/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/dataResetRow',
], function($, _, Backbone, template) {
    'use strict';

    var DataresetrowView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "dataResetRow",
        initialize: function(data) {
            this.appId = data.appId;
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
        },
        events: {
            "click .deleteBtn": "sendDelete"
        },
        sendDelete: function(event) {
            var el = $(event.currentTarget);
            el.attr("disabled", "disabled");
            var module = el.attr("data-module");
            module = module == "all" ? ['quiz', 'invite', 'ugc', 'custom', 'user', 'sweepstake', 'limits', 'activity'] : [module];
            var self = this;
            $.ajax({
                method: "delete",
                data: JSON.stringify({
                    modules: module
                }),
                url: globalConfig.api + this.appId + "/users/" + this.model.get("user_id"),
                contentType: "application/json",
                dataType: "json",
                complete: function(r) {
                    el.removeAttr("disabled");
                    if (el.attr("data-module") == "all") {
                        self.$el.remove();
                    } else {
                        el.parent().parent().parent().addClass("flash");
                        setTimeout(function() {
                            el.parent().parent().parent().removeClass("flash");
                        }, 2000);
                    }
                },
            });
        }
    });

    return DataresetrowView;
});