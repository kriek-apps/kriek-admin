/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/topicList',
  'views/topicListItem',
  'collections/topic',
  'models/topic',
  'views/topicDetails'
], function($, _, Backbone, template, TopicListItemView, TopicCollection, TopicModel, TopicDetailsView) {
  'use strict';

  var TopicListView = Backbone.View.extend({
    template: template,
    tagName: "ul",
    className: "topic",
    initialize: function() {
      this.collection = new TopicCollection();
      this.collection.on("reset add remove change", function() {
        this.render();
      }, this);
      this.collection.fetch({reset: true});
    },
    render: function() {
      this.$el.html(template());
      this.$el.find(".topics").html("");
      this.collection.each(function(item) {
        var topicListItem = new TopicListItemView({
          model: item
        });
        this.$el.find(".topics").append(topicListItem.$el);
      }, this);
    },
    events: {
      "click .newTopicBtn" : "newTopic"
    },
    newTopic: function() {
      var newModel = new TopicModel();
      var modal = new TopicDetailsView({model: newModel});
    }
  });

  return TopicListView;
});