/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/quizList',
  'views/quizListItem',
  'collections/quiz'
], function($, _, Backbone, template, QuizListItemView, QuizCollection) {
  'use strict';

  var QuizListView = Backbone.View.extend({
    template: template,
    tagName: "table",
    className: "quiz table table-striped table-hover",
    initialize: function(data) {
      this.app = data.app;
      this.collection = new QuizCollection(data.id);
      this.collection.on('reset add change remove', function() {
        this.render();
      }, this);
      this.collection.fetch({reset : true});
    },
    render: function() {
      this.$el.html(this.template());
      this.collection.each(function(item) {
        var quizListItem = new QuizListItemView({
          model: item,
          app : this.app
        });
        this.$el.append(quizListItem.$el);
      }, this);
    },
    events: {
      
    }

  });

  return QuizListView;
});