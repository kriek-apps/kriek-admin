/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/queryList',
  'views/queryListItem',
  'collections/report'
], function($, _, Backbone, template, QueryListItemView, ReportCollection) {
  'use strict';

  var QueryListView = Backbone.View.extend({
    template: template,
    tagName: "ul",
    className: "query",
    initialize: function(data) {
      this.appId = data.appId;
      this.collection = new ReportCollection(this.appId);
      this.collection.on("reset add remove change", function() {
        this.render();
      }, this);
      this.collection.fetch({reset : true});
    },
    render: function() {
      this.$el.html("");
      this.collection.each(function(item) {
        var queryListItem = new QueryListItemView({
          model: item
        });
        this.$el.append(queryListItem.$el);
      }, this);
    },
    events: {

    }
  });

  return QueryListView;
});