/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/notificationList',
    'views/notificationListItem',
    'collections/notification'
], function($, _, Backbone, template, NotificationListItemView, NotificationCollection) {
    'use strict';

    var NotificationListView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "notificationList",
        initialize: function(appId) {
            this.appId = appId.appId;
            this.collection = new NotificationCollection(this.appId);
            this.collection.on("reset change add remove", function() {
                this.render();
            }, this);
            this.collection.fetch({
                reset: true
            });
        },
        render: function() {
            this.$el.html("");
            this.collection.each(function(model) {
                var itemView = new NotificationListItemView({model : model});
                this.$el.append(itemView.$el);
            }, this);
        },
        events: {

        }
    });

    return NotificationListView;
});