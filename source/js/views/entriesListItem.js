/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'hbs!templates/entriesListItem',
    'models/entries'
], function($, _, Backbone, Handlebars, template, EntriesModel) {
    'use strict';

    var EntriesListItemView = Backbone.View.extend({
        template: template,
        tagName: "li",
        className: "entries col-md-4",
        initialize: function(data) {
            this.adminTemplate = data.adminTemplate;
            this.render();
            this.model.on("change", function() {
                this.render();
            }, this);
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            var ugcDisplay = Handlebars.compile(this.adminTemplate);
            this.$el.find(".entryDisplay").append(ugcDisplay(this.model.toJSON()));
        },
        events: {
            "click .disableButton": "disable",
            "click .delete": "delete"
        },
        delete: function() {
            this.model.destroy();
            this.$el.remove();
        },
        disable: function(event) {
            var change = this.model.get("disabled") == 1 ? 0 : 1;
            this.model.set("disabled", change);
            this.model.save();
        }
    });

    return EntriesListItemView;
});