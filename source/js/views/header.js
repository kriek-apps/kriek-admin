/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/header',
    'admin'
], function($, _, Backbone, template, Admin) {
    'use strict';

    var HeaderView = Backbone.View.extend({
        template: template,
        tagName: "header",
        className: "header",
        initialize: function() {
            Admin.on("adminChanged", function() {
                this.render();
            }, this);
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                admin: Admin.getData()
            }));
        },
        events: {
            "click .logout": "logout"
        },

        logout: function() {
            Admin.logout();
            window.location.hash = "/login";
        }


    });

    return HeaderView;
});