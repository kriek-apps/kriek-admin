/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/todos',
    'currentApp',
    'views/warning',
    'views/addTodo'
], function($, _, Backbone, template, CurrentApp, WarningsView, AddTodoView) {
    'use strict';

    var ToDoView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "todo",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
            this.list = new WarningsView();
            this.$el.find(".tasks").html(this.list.$el);
            this.renderCompleted();
        },
        events: {
            "click .newTask" : "newTask",
            "click .showCompleted" : "showCompleted",
            "click .restore" : "restore"
        },
        showCompleted: function() {
            this.$el.find(".completed").toggle();
        },
        renderCompleted: function() {
            this.$el.find(".completed").html("");
            var todos = CurrentApp.getModel().get("config").private.warnings || 0;
            for (var i = 0; i < todos.length; i++) {
                this.$el.find(".completed").append("<p>" + (todos[i].text || todos[i].message) + " <a class='restore' data-id='"+i+"'>Restore</a></p>");
            }
        },
        restore : function(event) {
            var el = $(event.currentTarget);
            var id = el.attr("data-id");
            var config = CurrentApp.getModel().get("config");
            config.private.warnings[id].completed = false;
            CurrentApp.getModel().set("config", config);
            CurrentApp.getModel().save();
            this.render();
        },
        newTask: function() {
            var modal = new AddTodoView({
                link : ""
            });
            var self = this;
            modal.$el.find("#todoModal").on("hidden.bs.modal", function() {
                self.list.render();
            });
        }

    });

    return ToDoView;
});