/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/settingsLanguage',
    'views/addLanguageField',
    'handlebars',
    'views/addTodo'
], function($, _, Backbone, template, AddLanguageFieldView, Handlebars, AddToDoView) {
    'use strict';

    var LanguageView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "language",
        initialize: function() {
            this.render();
            this.model.on("change", function() {
                this.render();
                this.unsavedChanges();
            }, this);
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            this.formTemplate = Handlebars.compile(this.$el.find(".template").html());
        },
        events: {
            "click .close": "deleteQuestion",
            "click button.addField": "addField",
            "click button.saveChanges": "saveChanges",
            "keyup input" : "unsavedChanges",
            "click .addTask" : "addTask"
        },
        deleteQuestion: function(event) {
            var el = $(event.currentTarget);
            el.parent().remove();
            this.unsavedChanges();
        },
        addTask: function(event) {
            var el = $(event.currentTarget);
            var anchor = window.location.hash.split(":")[0] + ":" + el.attr("data-anchor");
            var title = "Update the Language unit: " + el.attr("data-name");
            var modal = new AddToDoView({
                link : anchor,
                message : title
            });
        },
        addField: function() {
            var modal = new AddLanguageFieldView();
            modal.on("addField", function(event) {
                var newField = $(this.formTemplate({name : event.text}));
                newField.find("input").attr("data-key", event.text);
                newField.find("label").text(event.text);
                this.$el.find("form").prepend(newField);
                this.unsavedChanges();
            }, this);
        },
        saveChanges: function() {
            var inputs = this.$el.find("form input");
            var obj = {};
            for (var i = 0; i < inputs.length; i++) {
                var input = $(inputs[i]);
                obj[input.attr("data-key")] = input.val();
            }
            this.model.set("lang", obj);
            this.model.save();
            this.$el.find(".unsaved").hide();
        },
        unsavedChanges: function() {
            this.$el.find(".unsaved").css("display", "block");
        }
    });

    return LanguageView;
});