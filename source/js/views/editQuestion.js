/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/editQuestion',
    'handlebars',
    'bootstrap',
    'datepicker'
], function($, _, Backbone, template, Handlebars) {
    'use strict';

    var EditQuestionView = Backbone.View.extend({
        template: template,
        answerTemplate: {},
        tagName: "div",
        className: "editQuestion modal",
        initialize: function(data) {
            this.app = data.app;
            this.render();
        },
        render: function() {
            var extraFields = this.app.toJSON().config.public.modules.quiz.extra_fields.split(",");
            this.$el.html(this.template({
                model: this.model.toJSON(),
                extraFields : extraFields
            }));
            $("body").append(this.$el);

            //Differentiate between editing and adding a question
            if (typeof this.model.get("id") != "undefined") {
                //Editing
                if (this.model.get("question")['type'] == "radio") {
                    this.changeToRadioTemplate();
                } else if (this.model.get("question")['type'] == "text") {
                    this.changeToTextTemplate();
                }
            } else {
                //Adding
            }
            this.initialSetup();
            var self = this;
            this.$el.find("#quizModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
            this.$el.find("input[type=datetime]").appendDtpicker({
                "firstDayOfWeek": 1,
                "dateFormat": "YYYY-MM-DD hh:mm",
                "closeOnSelected": true,
                "todayButton": false,
                "inline" : true,
                "calendarMouseScroll": false
            });

        },
        events: {
            "click .typeSelectionButton": "selectType",
            "click .answerContainer .close": "deleteQuestion",
            "click .addNewAnserBtn": "addNewAnswer",
            "click .saveChanges": "checkFields"
        },
        selectType: function(event) {
            var el = $(event.currentTarget);
            this.$el.find(".typeSelectionButton").removeClass("active");
            el.addClass("active");
            this.$el.find(".typeInputField").val(el.attr("data-attr"));
            if (el.attr("data-attr") == "radio") {
                this.changeToRadioTemplate();
            } else if (el.attr("data-attr") == "text") {
                this.changeToTextTemplate();
            }
        },
        changeToRadioTemplate: function() {
            this.$el.find(".correct-text").hide();
            this.$el.find(".correct-radio").show();
            this.$el.find(".answers-container").show();
        },
        changeToTextTemplate: function() {
            this.$el.find(".correct-radio").hide();
            this.$el.find(".correct-text").show();
            this.$el.find(".answers-container").hide();
        },
        deleteQuestion: function(event) {
            var el = $(event.currentTarget);
            el.parent().remove();
        },
        initialSetup: function() {
            if (typeof this.model.get('question') != "undefined" && this.model.get("question")['type'] == "radio")
                this.$el.find("input.correct-radio:eq(" + this.model.get("correct") + ")").attr('checked', true);
            this.answerTemplate = Handlebars.compile(this.$el.find(".answer-template").html());
            if (typeof this.model.get('question') == "undefined") {
                //on adding new question, set it to radio default
                setTimeout(function() {
                    $(".typeSelectionButton[data-attr='radio']").trigger('click');
                }, 100);
            }
        },
        addNewAnswer: function() {
            var html = this.answerTemplate();
            this.$el.find(".answers").append(html);
        },
        checkFields: function() {
            /* Check wether we have any empty fields */
            //Set dates if they are empty
            if (this.$el.find(".date-from").val() === "") this.$el.find(".date-from").val("2000.01.01 00:00:00");
            if (this.$el.find(".date-till").val() === "") this.$el.find(".date-till").val("2100.01.01 00:00:00");
            this.saveChanges();
        },
        saveChanges: function() {
            //Update the model
            var inputs = this.$el.find("input");
            var question = {};
            var answers = [];
            for (var i = 0; i < inputs.length; i++) {
                var input = $(inputs[i]);
                var key = input.attr("data-key");
                //question object
                if (key == "question") {
                    var subkey = input.attr("data-subkey");
                    question[subkey] = input.val();
                } else
                //answers array
                if (key == "answers") {
                    answers[answers.length] = input.val();
                } else
                //other
                {
                    //not radio
                    if (input.attr("type") != "radio")
                        this.model.set(key, input.val());
                }

            }
            this.model.set("question", question);
            this.model.set("answers", answers);
            var correct = "";
            if (this.model.get("question")['type'] == "radio") {
                correct = this.$el.find(".correct-radio:checked").index(".correct-radio");
            } else if (this.model.get("question")['type'] == "text") {
                correct = this.$el.find(".correct-text input").val().split(",");
            }
            if (correct === "" || correct == -1) correct = 0;
            this.model.set("correct", correct);
            var self = this;
            this.model.save({}, {success: function() {
                self.$el.find("#quizModal").modal("hide");
                self.trigger("editFinish");
            }});
        }
    });

    return EditQuestionView;
});