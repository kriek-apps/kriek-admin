/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/notifications',
    'hbs!templates/notifications-select',
    'hbs!templates/notifications-add',
    'hbs!templates/notifications-finalize',
    'hbs!templates/reportFieldHeaderSearch',
    'views/notificationList',
    'collections/report',
    'views/reportList',
    'views/reportFieldList',
    'models/notification',
    'views/notificationWizard'
], function($, _, Backbone, template, selectTemplate, addTemplate, finalizeTemplate, reportFieldHeaderSearchTemplate, NotificationListView, ReportCollection, ReportListView, ReportFieldListView, NotificationModel, NotificationWizard) {
    'use strict';

    var NotificationsView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "notifications",
        initialize: function(appId) {
            this.appId = appId.appId;
            this.render();
        },
        render: function() {
            //Show the notifications
            this.$el.html(selectTemplate());
            var view = new NotificationListView({
                appId: this.appId
            });
            this.$el.find("#results").html(view.$el);
            this.view = view;
        },
        events: {
            "click .newNotifBtn": "newNotification",
            "change select.report": "changeReport",
            "keyup input.searchField": "filter",
            "click .sendBtn": "nextToDetails",
            "click .saveBtn": "save"
        },
        newNotification: function() {
            this.modal = new NotificationWizard({appId : this.appId});
            this.modal.on("finish", function() {
                this.view.collection.fetch();
            }, this);
        },
        
       
        filter: function() {
            var filters = {};
            var inputs = this.$el.find("input.searchField");
            for (var i = 0; i < inputs.length; i++) {
                var input = $(inputs[i]);
                filters[input.attr("data-key")] = input.val();
            }
            this.currentFilters = filters;
            this.table.conditionsFilter(filters);
        }
        
    });

    return NotificationsView;
});