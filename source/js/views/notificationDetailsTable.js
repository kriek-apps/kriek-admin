/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/reportFieldList',
  'hbs!templates/notificationDetailsTableHeader',
  'hbs!templates/reportPagination',
  'views/reportFieldListItem',
  'collections/reportField'
], function($, _, Backbone, template, ReportFieldListHeaderTemplate, ReportPaginationTemplate, ReportFieldListItemView, ReportFieldCollection) {
  'use strict';

  var ReportFieldListView = Backbone.View.extend({
    template: template,
    tagName: "div",
    className: "reportField",
    data: {},
    filterCollection: {},
    pagination: {
      currentPage: 0,
      pageSize: 10
    },
    initialize: function(data) {
      this.data = data;
      this.collection = new ReportFieldCollection(data);
      this.collection.on('reset', function() {
        this.filterCollection = this.collection.clone();
        if (typeof this.data.notificationModel.get("settings").filters !== "undefined")
          this.conditionsFilter(this.data.notificationModel.get("settings").filters);
        else this.render();
      }, this);
      this.collection.fetch({
        reset: true
      });
    },
    render: function(sort) {
      this.$el.html(template());
      var i = 0;
      $(".numberOfResults").text(this.filterCollection.toJSON().length + " rows total");
      var titles = this.collection.titles;
      var sorted = this.filterCollection.toJSON().splice(this.pagination.currentPage * this.pagination.pageSize);
      sorted = _.first(sorted, this.pagination.pageSize);
      _.each(sorted, function(item) {
        if (i === 0) {
          var model = [];
          var j = 0;
          $.each(item, function(key, value) {
            model[j] = {
              name: titles[j],
              data: key
            };
            j++;
          });
          this.$el.find('table').append(ReportFieldListHeaderTemplate({
            model: model,
            notif : this.data.notificationModel.toJSON()
          }));
        }
        i++;
        var reportFieldListItem = new ReportFieldListItemView({
          model: new Backbone.Model(item)
        });
        this.$el.find('table').append(reportFieldListItem.$el);
      }, this);
      if (typeof sort !== "undefined") {
        var arrow = sort.sort == "sorted-asc" ? "glyphicon-arrow-down" : "glyphicon-arrow-up";
        var span = "<span class='glyphicon " + arrow + "'></span>";
        this.$el.find('th[data-key="' + sort.key + '"]').addClass(sort.sort).append(span);
      }
      this.$el.find('td:first-child').remove();
      //Create Pagination
      var paginationData = {
        current: this.pagination.currentPage,
        last: Math.floor(this.filterCollection.toJSON().length / this.pagination.pageSize) + 1
      };
      this.$el.append(ReportPaginationTemplate({
        data: paginationData
      }));
      if (paginationData.last > 1) {
        for (var i = 1; i <= paginationData.last; i++) {
          var el = this.$el.find("ul.pagination");
          var active = (i - 1) == paginationData.current ? "active" : "";
          if (paginationData.last > 19) {
            var display = false;
            var ellipses = false;

            //Elején
            if (paginationData.current <= 13) {

              if (i <= 15) display = true;
              if (i == 16) ellipses = true;
              if (i > paginationData.last - 2) display = true;

            }

            //Középen
            if (paginationData.current > 13 && paginationData.current <= paginationData.last - 12) {

              if (i < 4) display = true;
              if (i == 4) ellipses = true;

              if (i > paginationData.current - 5 && i < paginationData.current + 5) display = true;

              if (i == paginationData.last - 3) ellipses = true;
              if (i > paginationData.last - 3) display = true;

            }

            //Végén
            if (paginationData.current > paginationData.last - 12) {

              if (i < 4) display = true;
              if (i == 4) ellipses = true;
              if (i > paginationData.last - 14) display = true;

            }

            //render
            if (display) {
              el.append("<li class='" + active + "'><a>" + i + "</a></li>");
            } else if (ellipses) {
              el.append("<li class='disabled'><a>...</a></li>");
            }


          } else {
            el.append("<li class='" + active + "'><a>" + i + "</a></li>");
          }
        }
      }
      this.$el.find("table").removeClass("table-striped");
      this.colorUsers();
      this.trigger("render");
    },
    events: {
      "click .table-header": "sortTable",
      "click ul.pagination li": "switchPage"
    },
    switchPage: function(event) {
      var el = $(event.currentTarget);
      var page = el.text();
      this.pagination.currentPage = page - 1;
      this.render();
    },
    sortTable: function(event) {
      var el = $(event.currentTarget);
      var sort = "sorted-asc";
      if (el.hasClass('sorted-asc')) {
        sort = "sorted-desc";
      }
      $(".table-header").removeClass('sorted-asc').removeClass('sorted-desc');
      el.addClass(sort);
      var key = el.attr('data-key');
      var array = this.filterCollection.sortBy(function(item) {
        var r = item.get(key);
        var numeric = !isNaN(parseFloat(r)) && isFinite(r);
        if (numeric) {
          return r === null ? 0 : r;
        } else {
          return item.get(key) === null ? "" : String(item.get(key)).toLowerCase();
        }
      });
      if (sort == "sorted-desc") array.reverse();
      this.filterCollection = new Backbone.Collection(array);
      this.render({
        key: key,
        sort: sort
      });
    },
    filter: function(string) {
      this.filterCollection = new Backbone.Collection(this.collection.filter(function(item) {
        var obj = item.toJSON();
        var match = false;
        $.each(obj, function(key, value) {
          if (String(value).toLowerCase().indexOf(string.toLowerCase()) >= 0) match = true;
        });
        return match;
      }));
      this.render();
    },
    /* ************************************************************ */
    conditionsFilter: function(filter) {
      this.filterCollection = new Backbone.Collection(this.collection.filter(function(item) {
        var obj = item.toJSON();
        var match = true;
        $.each(obj, function(key, value) {
          var orStatements = filter[key].split("+");
          var orMatch = false;
          for (var i = 0; i < orStatements.length; i++) {
            if (orStatements[i][0] == "!") {
              if (String(value).toLowerCase().indexOf(orStatements[i].substr(1).toLowerCase()) == -1) orMatch = true;
            } else {
              if (String(value).toLowerCase().indexOf(orStatements[i].toLowerCase()) > -1) orMatch = true;
            }
            
          }
          if (orMatch === false) {
            match = false;
          }
        });
        return match;
      }));
      this.render();
    },
    /* ************************************************************ */
    getSelectedUsers: function() {
      return this.filterCollection;
    },
    getFields : function() {
      var model = _.first(this.collection.toJSON());
      return _.keys(model);
    },
    colorUsers : function() {
      var users = this.data.notificationModel.get("sent_to");
      this.$el.find("tr").each(function() {
        var id = $(this).attr("data-id");
        if (typeof id === "undefined") return;
        if (users.indexOf(Number(id)) == -1) {
          $(this).addClass("success");
        } else {
          $(this).addClass("active");
        }
      });
    }

  });

  return ReportFieldListView;
});