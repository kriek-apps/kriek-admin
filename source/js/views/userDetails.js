/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/userDetails',
    'models/user',
    'hbs!templates/activityDetail',
    'currentApp',
    'views/tickerCard',
    'admin'
], function($, _, Backbone, template, User, tActivityDetail, CurrentApp, TickerCardView, Admin) {
    'use strict';

    var UserdetailsView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "userDetails",
        initialize: function(data) {
            this.data = data;
            this.model = new User({
                id: this.data.userId,
                appId: CurrentApp.getID()
            });
            this.model.on("reset change", function() {
                this.render();
            }, this);
            this.model.fetch();
        },
        render: function() {
            //Modify the data for rendering
            //Omit app ID and user ID
            var data = this.model.toJSON();
            for (var key in data) {
                if (data[key] instanceof Array) {
                    for (var i = 0; i < data[key].length; i++) {
                        data[key][i] = _.omit(data[key][i], ['app_id', 'user_id']);
                    }
                }
            }

            //Slice up activities to different sessions
            var sessions = [];
            var currentSession = [];
            for (var i = 0; i < data.activity.length; i++) {
                if ((data.activity[i].activity_type_id === 13 || data.activity[i].activity_type_id === 113) && i !== 0) {
                    sessions.push(currentSession);
                    currentSession = [];
                }
                if (data.activity[i].activity_type_id !== 15 || Admin.isDeveloper()) {
                    currentSession.push(data.activity[i])
                }
            }
            sessions.push(currentSession);
            for (var i = 0; i < sessions.length; i++) {
                for (var j = 0; j < sessions[i].length; j++) {
                    if (j !== 0) {
                        var start = new Date(sessions[i][0].date);
                        var time = new Date(sessions[i][j].date);
                        var minutes = Math.floor((time - start) / 60000);
                        var seconds = ((time - start) - minutes * 60000) / 1000;
                        minutes = minutes < 10 ? "0" + minutes : minutes;
                        seconds = seconds < 10 ? "0" + seconds : seconds;
                        sessions[i][j].date = minutes + ":" + seconds;
                    }
                    if (j === 0) {
                        sessions[i][j].first = true;
                    }
                    if (j === sessions[i].length -1 && j !== 0) {
                        sessions[i][j].last = true;
                    }
                    if (sessions[i].length === 1) {
                        sessions[i][j].single = true;
                    }
                }
            }
            //Render
            this.$el.html(this.template({
                model: data,
                sessions: sessions
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#userDetailsModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events: {
            "click .activityRow" : "showActivityDetails"
        },
        showActivityDetails: function(event) {
            var el = $(event.currentTarget);
            var id = $(el).attr("data-id");
            var type = $(el).attr("data-type");
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + "activity/" + id,
                success: function(r) {
                    var view = new TickerCardView({model : r, type : type});
                    self.$el.find(".activityDetail").remove();
                    var node = tActivityDetail();
                    el.after(node);
                    self.$el.find(".cardContainer").html(view.$el);
                }
            });
                    

        }
    });

    return UserdetailsView;
});