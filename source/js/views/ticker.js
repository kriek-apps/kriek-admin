
/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/ticker',
    'currentApp',
    'views/tickerCard',
    'views/userDetails',
    'admin'
], function($, _, Backbone, template, CurrentApp, TickerCardView, UserDetailsView, Admin) {
    'use strict';

    var TickerView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "ticker",
        lastID: 0,
        duration: 10000,
        queue: [],
        firstRender: true,
        initialize: function() {
            this.render();
            var self = this;
            window.timer = setInterval(function() {
                self.update();
            }, this.duration);
        },
        render: function() {
            this.update();
        },
        update: function() {
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/ticker/" + self.lastID,
                success: function(r) {
                    self.queue = r.concat(self.queue);
                    if (self.firstRender && r.length == 0) {
                        self.$el.prepend("<div class='noActivity'>No activities yet.</div>");
                        self.firstRender = false;
                        return;
                    }
                    try {
                        self.lastID = r[0].id;
                    } catch(err) {}
                    for (var i = 0; i < r.length; i++) {
                        self.$el.find(".noActivity").remove();
                        if (self.firstRender) {
                            self.appendDiv(self.queue.pop());
                        } else {
                            setTimeout(function() {
                                self.tick();
                            }, (self.duration / r.length) * (r.length - i));
                        }
                    }
                    self.firstRender = false;
                }
            });
            this.updateTimes();
        },
        tick: function() {
            this.appendDiv(this.queue.pop());
        },
        appendDiv: function(model) {
            model.ago = this.calculateTime(model.date);
            if (model.type === 15 && !Admin.isDeveloper()) {
                return;
            }
            if (model.type === 3) {
                model.type_name = CurrentApp.getModel().get("config").public.modules.custom.name_feed;
            }
            if (model.type === 4) {
                model.type_name = CurrentApp.getModel().get("config").public.modules.limits.name_feed;
            }
            this.$el.prepend(template({
                model: model
            }));
            this.$el.find(":first").hide().slideDown().fadeIn();
            if (this.$el.find(".panel").length > 10) {
                this.$el.find(".panel:last").remove();
            }
            //Conversion:
            if (model.type === 1) {
                var el = this.$el.find("div[data-session=" + model.session +"][data-type=13]:contains('Unknown')");
                el.find(".panel-body").append("<p>Conversion: <img style='height:30px;' src='http://graph.facebook.com/"+model.user_id+"/picture'> "+model.name+"</p>");
                el = this.$el.find("div[data-session=" + model.session +"][data-type=14]:contains('Unknown')");
                el.find(".panel-body").append("<p>Conversion: <img style='height:30px;' src='http://graph.facebook.com/"+model.user_id+"/picture'> "+model.name+"</p>");
                el = this.$el.find("div[data-session=" + model.session +"][data-type=113]:contains('Unknown')");
                el.find(".panel-body").append("<p>Conversion: <img style='height:30px;' src='http://graph.facebook.com/"+model.user_id+"/picture'> "+model.name+"</p>");
            }
        },
        events: {
            "click .panel": "open",
            "click .userBadge": "showUser"
        },
        showUser: function(event) {
            event.stopImmediatePropagation();
            var el = $(event.currentTarget);
            var id = el.attr("data-id");
            var modal = new UserDetailsView({
                userId: id
            });
        },
        open: function(event) {
            var el = $(event.currentTarget);
            if (el.find(".tickerCard").length > 0) return;
            var id = el.attr("data-id");
            var type = el.attr("data-type");
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api +"activity/" + id,
                success: function(r) {
                    var view = new TickerCardView({
                        model: r,
                        type: type
                    });
                    el.find(".panel-body").append(view.el);
                }
            });
        },
        calculateTime: function(date) {
            var now = new Date();
            var then = new Date(date);

            var d = now - then;
            
            if (d < 1000 * 60) {
                var diff = Math.floor(d / 1000);
                return  diff > 1 ? diff + " seconds ago" : diff + " second ago";
            }
            if (d < 1000 * 60 * 60) {
                var diff = Math.floor(d / (1000 * 60));
                return  diff > 1 ? diff + " minutes ago" : diff + " minute ago";
            }
            if (d < 1000 * 60 * 60 * 24) {
                var diff = Math.floor(d / (1000 * 60 * 60));
                return  diff > 1 ? diff + " hours ago" : diff + " hour ago";
            }
            var diff = Math.floor(d / (1000 * 60 * 60 * 24));
            return  diff > 1 ? diff + " days ago" : diff + " day ago";
        },
        /**
         * Update all times
         */
        updateTimes: function() {
            var self = this;
            this.$el.find(".panel").each(function() {
                var date = $(this).find(".date").attr("data-date");
                $(this).find(".date").html(self.calculateTime(date));
            });
        }
    });

    return TickerView;
});