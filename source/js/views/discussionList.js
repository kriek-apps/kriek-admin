/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/discussionList',
  'views/discussionListItem',
  'collections/discussion'
], function($, _, Backbone, template, DiscussionListItemView, DiscussionCollection) {
  'use strict';

  var DiscussionListView = Backbone.View.extend({
    template: template,
    tagName: "ul",
    className: "discussion",
    currentPage: 0,
    currentFilter: "unanswered",
    initialize: function() {
      this.collection = new DiscussionCollection();
      this.collection.on("reset", function() {
        this.render();
      }, this);
      this.collection.fetch({ reset: true});
    },
    render: function() {
      this.$el.html(template());
      var self = this;

      //** FILTER **//

      var filtered = _(this.collection.filter(function(obj) {
          if (self.currentFilter == "unanswered") {
            if (obj.toJSON().question.answers.length > 0) {
              return obj.toJSON().question.answers[obj.toJSON().question.answers.length-1].admin === undefined;
            } else return true;
          } else if (self.currentFilter == "answered"){
             if (obj.toJSON().question.answers.length > 0) {
              return obj.toJSON().question.answers[obj.toJSON().question.answers.length-1].admin === true;
            } else return false;
          } else {
            return true;
          }
      }));
      /** PAGINATION **/

      var toDisplay = _(this.pagination(filtered, 10,this.currentPage));
      
      /** RENDER **/

      toDisplay.each(function(item) {
        var discussionListItem = new DiscussionListItemView({
          model: item
        });
        this.$el.find(".listContainer").append(discussionListItem.$el);
      }, this);
      this.$el.find(".pager").attr("disabled", "disabled");
      if (this.currentPage > 0) {
        this.$el.find(".pager.left").removeAttr("disabled");
      }
      if (10*(this.currentPage + 1) < filtered.__wrapped__.length) {
        this.$el.find(".pager.right").removeAttr("disabled");
      }
    },
    events: {
      "click .pager.left" : "pageLeft",
      "click .pager.right" : "pageRight"
    },
    pageLeft: function() {
      this.currentPage--;
      this.render();
    },
    pageRight: function() {
      this.currentPage++;
      this.render();
    },
    pagination: function(collection, perPage, page) {
        collection = _(collection.rest(perPage * page));
        collection = _(collection.first(perPage));
        return collection;
    }
  });

  return DiscussionListView;
});