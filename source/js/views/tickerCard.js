/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'currentApp',
    'hbs!templates/activityError',
    'hbs!templates/activityVote',
    'hbs!templates/activityInvite',
    'hbs!templates/activityQuiz',
    'hbs!templates/activityCustom',
    'hbs!templates/activityEntry',
    'hbs!templates/activitySweepstakes',
    'hbs!templates/activityPayment'
], function($, _, Backbone, Handlebars, CurrentApp, tActivityError, tActivityVote, tActivityInvite, tActivityQuiz, tActivityCustom, tActivityEntry, tActivitySweepstakes, tActivityPayment) {
    'use strict';

    var TickerCardView = Backbone.View.extend({
        template: "",
        tagName: "div",
        className: "tickerCard",
        initialize: function(options) {
            this.type = options.type;
            this.model = options.model;
            this.render();
        },
        render: function() {
            //ERROR
            if (this.type == 15) {
                this.template = tActivityError;
            }
            //UGC NEW ENTRY
            if (this.type == 7) {
                this.template = tActivityEntry
            }
            //UGC VOTE
            if (this.type == 8) {
                this.template = tActivityVote
            }
            //INVITE
            if (this.type == 5) {
                this.template = tActivityInvite
            }
            //QUIZ ANSWER
            if (this.type == 2) {
                this.template = tActivityQuiz
            }

            //Sweepstakes
            if (this.type == 12) {
                this.template = tActivitySweepstakes
            }

            //CUSTOM OR LIMIT
            if (this.type == 3 || this.type == 4) {
                this.template = tActivityCustom
            }

            //PAYMENT
            if (this.type == 17) {
                this.template = tActivityPayment;
                var model = this.model;
                this.model.product = _.find(CurrentApp.getModel().get("briefcase")[0].contents, function(o) {
                    return o.id == model.product_id;
                });;
            }

            //RENDER
            this.$el.html( this.template({ model : this.model}) );

            //ADDITIONAL RENDERING
            if (this.type == 7) {
                var template = CurrentApp.getModel().get("config").public.modules.ugc.adminTemplate;
                template = Handlebars.compile(template);
                this.$el.find(".entry").html( template( this.model ) );
            }
            if (this.type == 8) {
                var template = CurrentApp.getModel().get("config").public.modules.ugc.adminTemplate;
                template = Handlebars.compile(template);
                this.$el.find(".entry").html( template( this.model ) );
            }
            if (this.type == 3) {
                var template = CurrentApp.getModel().get("config").public.modules.custom.adminTemplate;
                template = Handlebars.compile(template);
                this.$el.find(".well").html( template( this.model ) );
            }
            if (this.type == 4) {
                var template = CurrentApp.getModel().get("config").public.modules.limits.adminTemplate;
                template = Handlebars.compile(template);
                this.$el.find(".well").html( template( this.model ) );
            }

        },
        events: {
        }
    });

    return TickerCardView;
});