/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/topicListItem',
    'models/topic',
    'views/topicDetails'
], function($, _, Backbone, template, TopicModel, TopicDetailsView) {
    'use strict';

    var TopicListItemView = Backbone.View.extend({
        template: template,
        tagName: "li",
        className: "topic",
        initialize: function() {
            this.render();
            this.model.on("change", function() {
                this.render();
            }, this);
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
        },
        events: {
            "click .detailsBtn": "details",
            "click .toggleBtn": "toggle"
        },
        details: function() {
            var modal = new TopicDetailsView({model: this.model});
        },
        toggle: function() {
            var visible = this.model.get("visible");
            if (visible === 1) {
                this.model.set("visible", 0);
            } else {
                this.model.set("visible", 1);
            }
            this.model.save();
        }
    });

    return TopicListItemView;
});