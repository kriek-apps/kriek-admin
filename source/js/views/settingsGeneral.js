/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/settingsGeneral',
    'admin',
    'views/addTodo',
    'uploadify'
], function($, _, Backbone, template, Admin, AddToDoView) {
    'use strict';

    var SettingsGeneralView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "settingsGeneral",
        initialize: function(data) {
            this.render();
            this.model.on("change", function() {
                this.render();
            }, this);
        },
        render: function() {
            var modelToDisplay = jQuery.extend(true, {}, this.model.toJSON());
            modelToDisplay.config.public = _.omit(modelToDisplay.config.public, 'modules');
            modelToDisplay.config.public = _.omit(modelToDisplay.config.public, 'groups');
            modelToDisplay.config.private = _.omit(modelToDisplay.config.private, 'imageProcesses');
            modelToDisplay.config.private = _.omit(modelToDisplay.config.private, 'warnings');
            this.$el.html(this.template({
                model: modelToDisplay,
            }));
            if (!Admin.isDeveloper()) {
              this.$el.find("div.form-group[data-developer=true]").hide();
            }
            var self = this;
            setTimeout(function() {

                self.$el.find(".uploadButton").each(function() {
                    var button = $(this);
                    $(this).uploadifive({
                        buttonClass : "btn btn-warning",
                        buttonText : "Upload",
                        width: 80,
                        formData: {
                        },
                        onUploadStart: function(file) {
                        },
                        multi: false,
                        uploadScript: globalConfig.api + self.model.get("id") + '/upload?admin_token=' + Admin.getToken(),
                        onUploadComplete: function(file, data, response) {
                            /* Parse data to json */
                            data = $.parseJSON(data);
                            self.$el.find("input[data-key=" + button.attr("data-key") + "]").val(data.file_url);
                            self.unsavedChange();
                        }
                    });
                });
            }, 1000);
        },
        events: {
            "keyup input": "unsavedChange",
            "click .saveChangesButton": "saveChanges",
            "click .on-off": "switch",
            "click .addTask" : "addTask"
        },
        unsavedChange: function() {
            this.$el.find(".unsaved").show();
        },
        addTask: function(event) {
            var el = $(event.currentTarget);
            var anchor = window.location.hash.split(":")[0] + ":" + el.attr("data-anchor");
            var title = "Update the Setting: " + el.attr("data-name");
            var modal = new AddToDoView({
                link : anchor,
                message : title
            });
        },
        saveChanges: function() {
            var inputs = this.$el.find("form input:not([type=file])");
            var config = this.model.get("config");
            for (var i = 0; i < inputs.length; i++) {
                var input = $(inputs[i]);
                var val = input.val();
                if (input.attr("data-type") === "boolean") {
                    val = val === "true" ? true : false;
                }
                config[input.attr("data-group")][input.attr("data-key")]["value"] = val;
            }
            this.model.set("config", config);
            this.model.trigger("change");
            this.model.save();
            this.$el.find(".unsaved").hide();
        },
        switch: function(event) {
            var el = $(event.currentTarget);
            el.toggleClass("on");
            var input = el.siblings().find("input");
            input.val( (input.val() == "true" ? "false" : "true") );
            this.unsavedChange();
        }
    });

    return SettingsGeneralView;
});