/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'facebooksdk',
    'handlebars',
    'hbs!templates/notificationWizard',
    'hbs!templates/notifications-add',
    'hbs!templates/notifications-finalize',
    'collections/report',
    'views/reportList',
    'views/reportFieldList',
    'hbs!templates/reportFieldHeaderSearch',
    'models/notification',
    'hbs!templates/notification-loading',
    'hbs!templates/notifications-warning',
    'models/app',
    'hbs!templates/notification-newfilter',
    'currentApp',
    'vendor/dateFormat'
], function($, _, Backbone, FB, Handlebars, template, addTemplate, finalizeTemplate, ReportCollection, ReportListView, ReportFieldListView, reportFieldHeaderSearchTemplate, NotificationModel, notificationLoadingTemplate, warningTemplate, AppModel, NewFilterTemplate, CurrentApp) {
    'use strict';

    var NotificationWizardView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "notificationWizard",
        initialize: function(appId) {
            
            this.appId = appId.appId;
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#notificationModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
            this.switchToView("add");
        },
        events: {
            "keyup input.searchField": "filter",
            "click .sendBtn": "finalize",
            "click .saveBtn": "warning",
            "click .confirmBtn": "save",
            "change select.report": "changeReport",
            "click .advanced": "showAdvanced",
            "click .addFilter": "addFilter",
            "click .deleteFilter": "deleteFilter"
        },
        addFilter: function() {
            var input = this.$el.find(".filterInput");
            var data = {
                field: input.find("option:selected").val(),
                filter: input.find("input").val()
            };
            input.before(NewFilterTemplate({
                data: data
            }));
            this.filter();
        },
        deleteFilter: function(event) {
            var el = $(event.currentTarget);
            el.parent().remove();
            this.filter();
        },
        switchToView: function(viewName) {

            if (viewName == "warning") {
                //Header
                this.$el.find(".modal-header p").text("Step Three - Confirm");
                //Body
                this.$el.find(".modal-body").html(warningTemplate({
                    users: this.selectedUsers,
                    message: this.currentMessage
                }));
                this.$el.find(".modal-footer .saveBtn").removeClass("saveBtn").addClass("confirmBtn").text("Confirm");
            }
            if (viewName == "add") {
                //Header
                this.$el.find(".modal-header p").text("Step One - Target the recipients of the notification");
                //Table screen
                this.$el.find(".modal-body").html(addTemplate());
                //Add report selection
                this.reportCollection = new ReportCollection(this.appId);
                this.reportCollection.on("reset", function() {
                    var reportListView = new ReportListView({
                        collection: this.reportCollection
                    });
                    this.$el.find(".controls form").append(reportListView.$el);
                    //display currently selected report data
                    var select = this.$el.find(".controls form select");
                    this.displayReportData(select.find("option:selected").attr("data-id"));
                    this.$el.find(".modal-footer").append('<button class="btn export btn-primary sendBtn pull-right">Next</button>');
                }, this);
                this.reportCollection.fetch({
                    reset: true
                });
            }
            if (viewName == "finalize") {
                //Header
                this.$el.find(".modal-header p").text("Step Two - Enter message");
                //Body
                this.$el.find(".modal-body").html(finalizeTemplate({
                    users: this.selectedUsers
                }));
                this.$el.find(".modal-footer .sendBtn").removeClass("sendBtn").addClass("saveBtn");
            }
            if (viewName == "loading") {
                //Header
                this.$el.find(".modal-header p").text("Sending notifications...");
                //Body
                this.$el.find(".modal-body").html(notificationLoadingTemplate({
                    users: this.selectedUsers
                }));
                //Send notifications
                var app = new AppModel({
                    id: this.appId
                });
                app.on("reset change", function() {
                    this.appToken = app.get("config").private.app_token.value;
                    this.sendNotifications();
                }, this);
                app.fetch({
                    reset: true
                });

            }


        },
        displayReportData: function(id) {
            var data = {
                appId: this.appId,
                reportId: id
            };
            this.currentReport = id;
            var view = new ReportFieldListView(data);
            this.$el.find("#results").html(view.$el);
            this.table = view;
            //insert search fields
            view.collection.on("reset", function() {
                var fields = view.getFields();
                this.fields = fields;
                this.$el.find(".well.controls .filterContainer").html(reportFieldHeaderSearchTemplate({
                    fields: fields
                }));
                this.filter();

            }, this);
        },
        filter: function() {
            var filters = {};
            for (var i = 0; i < this.fields.length; i++) {
                filters[this.fields[i]] = "";
            }
            var activeFilters = this.$el.find(".filter");
            for (var i = 0; i < activeFilters.length; i++) {
                var el = $(activeFilters[i]);
                filters[el.find(".field").attr("data-field")] = el.find(".filterText").attr("data-filter");
            }
            this.currentFilters = filters;
            this.table.conditionsFilter(filters);
        },
        finalize: function() {
            this.selectedUsers = _.pluck(this.table.getSelectedUsers().toJSON(), "user_id");
            this.switchToView("finalize");
        },
        changeReport: function(event) {
            var el = $(event.currentTarget);
            this.displayReportData(el.find("option:selected").attr("data-id"));
        },
        warning: function() {
            this.currentMessage = this.$el.find(".messageField").val();
            this.currentName = this.$el.find(".nameField").val();
            this.currentUrl = "";
            if (this.$el.find(".customUrl").is(":checked") && this.$el.find(".customUrl-text").val() !== "") {
                this.currentUrl = "index.php?redirectUrl=" + encodeURI(this.$el.find(".customUrl-text").val());
                this.currentUrl = this.currentUrl.trim();
            }
            if (this.currentName !== "" && this.currentMessage !== "" && this.currentMessage.length < 256) {
                this.switchToView("warning");
            }
        },
        save: function() {

            var model = new NotificationModel({
                appId: this.appId
            });
            model.set("data", {
                date : new Date().getTime()
            });
            model.set("app_id", this.appId);
            model.set("name", this.currentName);
            model.set("sent_to", []);
            var settings = {
                filters: this.currentFilters,
                message: this.currentMessage,
                link: this.currentUrl,
                queryID: this.currentReport
            };
            model.set("settings", settings);
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/insights",
                complete: function(r) {
                    var insights = JSON.parse(r.responseText);
                    var users = 0;
                    var visits = 0;
                    var likes = 0;
                    var invites = 0;
                    var quiz = 0;
                    var entries = 0;
                    for (var i in insights) {
                        if (i !== "activities") {
                            users += insights[i][1] || 0;
                            visits = insights[i][13] || 0;
                            likes += insights[i][14] || 0;
                            invites += insights[i][5] || 0;
                            quiz += insights[i][2] || 0;
                            entries += insights[i][7] || 0;
                        }
                    }
                    model.set("data", {
                        users : users,
                        visits : visits,
                        likes : likes,
                        invites : invites,
                        quiz : quiz,
                        entries : entries,
                        date: new Date().format("yyyy-mm-dd HH:MM:ss")
                    });
                    self.currentModel = model;
                    self.switchToView("loading");
                }
            });
        },
        showAdvanced: function() {
            this.$el.find(".advanced-options").toggle();
        },
        sendNotifications: function() {
            var self = this;
            this.currentModel.save(null, {
                success: function(r) {
                    self.currentModel.set("id", r.attributes.last_id);
                    self.currentModel.url += "/" + r.attributes.last_id;
                    self.currentlyFinished = 0;
                    self.cache = [];
                    self.sendOneNotif();
                }
            });


        },
        sendOneNotif: function() {
            var max = this.selectedUsers.length;
            var step = 100 / this.selectedUsers.length;
            var self = this;
            var i = this.currentlyFinished;
            var currentUser = this.selectedUsers[i];
            console.log(this.currentUrl);
            FB.api('/' + this.selectedUsers[i] + '/notifications/', 'POST', {
                template: this.currentMessage,
                access_token: this.appToken,
                href: this.currentUrl
            }, function(message) {
                if (message.success !== true) {
                    console.log("FB notification error: ", message, " on user: ", currentUser);
                }
                self.cache.push(self.selectedUsers[i]);
                self.currentlyFinished++;
                $(".progress-bar").css("width", step * self.currentlyFinished + "%");
                $(".progress-counter").text(self.currentlyFinished + "/" + max);
                //Save to the database
                if (self.cache.length % 10 === 0 || self.currentlyFinished == max) {
                    var sent_to = self.currentModel.get("sent_to");
                    sent_to = sent_to.concat(self.cache);
                    self.currentModel.set("sent_to", sent_to);
                    self.cache = [];
                    self.currentModel.save();
                }
                if (self.currentlyFinished == max) {
                    self.$el.find("#notificationModal").modal("hide");
                    self.trigger("finish");
                } else {
                    var r = Math.floor(Math.random() * 400) + 200;
                    setTimeout(function() {
                        self.sendOneNotif();
                    }, r);
                }
                
            });
        }
    });

    return NotificationWizardView;
});