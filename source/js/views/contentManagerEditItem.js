/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/contentManagerEditItem',
    'currentApp',
    'admin',
    'hbs!templates/contentManagerItemForm',
    'md5'
], function($, _, Backbone, template, CurrentApp, Admin, itemFormTemplate, md5) {
    'use strict';

    var ContentmanageredititemView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "contentManagerEditItem",
        initialize: function(obj) {
            this.folderID = obj.folderID;
            if (obj !== undefined && obj.id !== undefined) {
                this.id = obj.id;
            }
            this.render();
        },
        render: function() {
            var item;
            var schema = CurrentApp.getModel().get("briefcase")[this.folderID].schema;
            if (this.id !== undefined) {
                item = CurrentApp.getModel().get("briefcase")[this.folderID].contents[this.id];
            } else {
                //create empty schema
                /*item = {};
                for (var i = 0; i < schema.length; i++) {
                    item[schema[i].name] = "";
                }*/
            }
            //Attach values to the schema
            /*for (var i = 0; i < schema.length; i++) {
                schema[i].value = item[schema[i].name];
            }*/
            this.$el.html(this.template({}));
            $("body").append(this.$el);


            //Render the ID field
            if (this.id !== undefined) {
                this.$el.find(".modal-body").append(itemFormTemplate({
                    model: {
                        name : "id",
                        type : "id",
                    },
                    value: item.id
                }));
            }
            //Render the inputs recursively
            this.renderInputs(schema, 0, this.$el.find(".modal-body"), item);

            //Open the modal
            var self = this;
            this.$el.find("#itemEditor").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
            this.uploadifyButtons();
        },
        events: {
            "click .saveBtn": "saveItem"
        },
        renderInputs: function(schema, indent, container, item) {
            //Render ID field
            
            for (var i = 0; i < schema.length; i++) {
                if (schema[i].type == "object") {
                    container.append(itemFormTemplate({
                        model: schema[i],
                        padding: (indent + 1) * 20,
                    }));
                    var object = typeof schema[i].object == "string" ? JSON.parse(schema[i].object) : schema[i].object;
                    var itemObject = item !== undefined ? item[schema[i].name] : undefined;
                    this.renderInputs(object, indent + 1, container.find(".data-group:last"), itemObject);
                } else {

                    var value = item !== undefined ? item[schema[i].name] : "";
                    if (value === "" && item === undefined) {
                        value = schema[i].defaultValue;
                    }

                    // If type is select, get select options
                    var select = [];
                    if (schema[i].type == "select") {
                        select = JSON.parse(schema[i].object);
                        //If the select is pointing to a briefcase folder
                        if (select.briefcase !== undefined) {
                            var folder = select.briefcase.folder;
                            var nameAttr = select.briefcase.name;
                            var valueAttr = select.briefcase.value;
                            folder = _.find(CurrentApp.getModel().get("briefcase"), function(o) {
                                return o.shorthand === folder;
                            }).contents;
                            select = [];
                            for (var j = 0; j < folder.length; j++) {
                                select.push({
                                    name : folder[j][nameAttr],
                                    value : folder[j][valueAttr]
                                });
                            }
                            console.log(select);
                        }
                        //Fill in the value, if it's an edit not a new item
                        if (value !== "") {
                            for (var j = 0; j < select.length; j++) {
                                if (value == select[j].value) {
                                    select[j].selected = true;
                                }
                            }
                        }
                    }
                    // If type is image, get required dimensions
                    var dimensions = [];
                    if (schema[i].type == "image") {
                        dimensions = JSON.parse(schema[i].object);
                    }
                    container.append(itemFormTemplate({
                        model: schema[i],
                        value: value,
                        select: select,
                        dimensions : dimensions
                    }));
                }
            }
        },
        getValues: function(container) {
            var obj = {};
            var dataHolders = container.children(".form-group");
            for (var i = 0; i < dataHolders.length; i++) {
                var el = $(dataHolders[i]).children(".dataHolder");
                if (el.is("input") || el.is("select")) {
                    obj[el.attr("data-name")] = el.val();
                } else {
                    //It's a div, containing an inner structure
                    obj[el.attr("data-name")] = this.getValues(el);
                }
            }
            return obj;
        },
        saveItem: function() {
            var item = this.getValues(this.$el.find(".modal-body"));

            var briefcase = CurrentApp.getModel().get("briefcase");
            if (this.id !== undefined) {
                //Overwrite
                briefcase[this.folderID].contents[this.id] = item;
            } else {
                //New Item
                var hash = md5(JSON.stringify(item) + (new Date()).getTime() );
                item.id = hash;
                briefcase[this.folderID].contents.push(item);
            }
            CurrentApp.getModel().set("briefcase", briefcase);
            CurrentApp.getModel().save();
            this.$el.find("#itemEditor").modal("hide");
            this.trigger("modelChanged");
        },
        uploadifyButtons: function() {
            var counter = 0;
            this.$el.find(".uploadBtn").each(function() {
                $(this).attr("id", "upload" + counter);
                var container = $(this).parent();
                $("#upload" + counter++).uploadifive({
                    buttonClass: "btn btn-warning",
                    buttonText: "Upload",
                    width: 80,
                    formData: {},
                    onUploadStart: function(file) {},
                    multi: false,
                    uploadScript: globalConfig.api + CurrentApp.getID() + '/upload?admin_token=' + Admin.getToken(),
                    onUploadComplete: function(file, data, response) {
                        /* Parse data to json */
                        data = $.parseJSON(data);
                        if (container.find("input").attr("data-type") == "image") {
                            var img = new Image();
                            img.onload = function() {
                                var input = container.find("input");
                                var reqWidth = input.attr("data-width");
                                var reqHeight = input.attr("data-height");
                                if (img.width == reqWidth && img.height == reqHeight) {
                                    container.removeClass("has-error");
                                    input.val(data.file_url);
                                } else {
                                    container.addClass("has-error");
                                }
                            };
                            img.src = data.file_url;
                        } else {
                            container.find("input").val(data.file_url);
                        }
                    }
                });
            });
        }
    });

    return ContentmanageredititemView;
});