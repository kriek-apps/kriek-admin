/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/definition',
    'admin',
    'currentApp',
    'views/confirmationModal',
    'uploadify'

], function($, _, Backbone, template, Admin, CurrentApp, ConfirmationModal) {
    'use strict';

    var DefinitionView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "definition",
        initialize: function(obj) {
            this.key = obj.key;
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model,
                key: this.key
            }));
            var self = this;
            setTimeout(function() {

                self.$el.find(".uploadButton").each(function() {
                    var button = $(this);
                    $(this).uploadifive({
                        buttonClass: "btn btn-warning",
                        buttonText: "Upload",
                        width: 80,
                        formData: {},
                        onUploadStart: function(file) {},
                        multi: false,
                        uploadScript: globalConfig.api + CurrentApp.getID() + '/upload?admin_token=' + Admin.getToken(),
                        onUploadComplete: function(file, data, response) {
                            /* Parse data to json */
                            data = $.parseJSON(data);
                            self.$el.find("input[type=text]").val(data.file_url);
                            self.unsavedChange();
                        }
                    });
                });
                
            }, 1000);
        },
        events: {
            "keyup input" : "unsavedChange",
            "click .close" : "delete",
            "click .openButton" : "open",
            "click .actionButton": "action"
        },
        action: function() {
            var string = this.model.value;
            var method = string.split(' ')[0];
            var url = string.split(' ')[1];

            var confirm = new ConfirmationModal();
            confirm.on('confirmed', function(){

                $.ajax({
                    method : method,
                    url: url,
                    success: function(r) {
                       console.log(r);
                    }   
                });
                
            });
        },
        open: function() {
            var url = this.$el.find("input[type=text]").val();
            window.open(url, "_blank");
        },
        unsavedChange: function() {
            this.$el.parent().parent().find(".unsaved").show();
        },
        delete: function() {
            this.unsavedChange();
            this.$el.remove();
        }
    });

    return DefinitionView;
});