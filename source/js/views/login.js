/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/login',
    'admin'
], function ($, _, Backbone, template, Admin) {
    'use strict';

    var LoginView = Backbone.View.extend({
        template: template,
        tagName : "div",
        className : "login",
        initialize : function() {
            this.render();
            var self = this;
            setTimeout(function(){
                self.$el.find('input.username').focus();
            }, 1);
        },
        render : function() {
           this.$el.html(this.template());
        },
        events : {
        "click .loginBtn" : "login",
        "keypress input.password" : "enterLogin"
        },
        enterLogin : function(e){
            if(e.which === 13){
                this.login();
            }
        },
        login: function() {
            var obj = {};
            obj.username = this.$el.find("input.username").val();
            obj.password = this.$el.find("input.password").val();
            Admin.login(obj, function() {
                if (Admin.previousLocation !== undefined && Admin.previousLocation !== "#/login") {
                    window.location.hash = Admin.previousLocation;
                } else {
                    window.location.hash = "";
                }
            });
        }
    });

    return LoginView;
});