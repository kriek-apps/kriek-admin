/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/userFlow',
    'currentApp',
    'views/circleMeter'
], function($, _, Backbone, template, CurrentApp, CircleMeter) {
    'use strict';

    var UserFlow = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "userFlow",
        initialize: function(data) {
            var id = data === undefined || data.id === undefined ? "" : "/" + data.id;
            console.log(id);
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api+CurrentApp.getID()+"/insights/userflow" + id,
                success: function(r) {
                    self.data = r;
                    self.extendData();
                    self.render();
                }
            });
        },
        extendData: function() {
            var max = _.max(this.data, function(o) {
                return o.q;
            }).q;
            var sort = CurrentApp.getModel().get("config").private.userFlow.value.split(",");
            this.data = _.sortBy(this.data, function(o) {
                return _.indexOf(sort, o.type.toString());
            });
            for (var i = 0; i < this.data.length; i++) {
                this.data[i].ratio = (this.data[i].q / max);
                this.data[i].height = this.data[i].ratio * 100;
                this.data[i].fontSize = this.data[i].ratio * 30;
                this.data[i].percent = Math.floor(this.data[i].ratio * 100);
                if ((i+1) < this.data.length) {
                    this.data[i].next = Math.floor(((this.data[i+1].q || 0) / this.data[i].q) * 100);
                }
            }
        },
        render: function() {
            this.$el.html(this.template({
                data: this.data
            }));
            var self = this;
            this.$el.find(".circle").each(function() {
                var ratio = $(this).attr("data-ratio");
                var meter = new CircleMeter({
                    width: 100,
                    height: 100,
                    percent: ratio
                });
                $(this).append(meter.$el);
            }); 
        },
        events: {

        }
    });

    return UserFlow;
});