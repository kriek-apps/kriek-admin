/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/reportFieldListItem',
    'models/reportField'
], function($, _, Backbone, template, ReportFieldModel) {
    'use strict';

    var ReportFieldListItemView = Backbone.View.extend({
        template: template,
        tagName: "tr",
        className: "reportField",
        initialize: function(data) {
            this.userIdField = data.userIdField;
            this.model.on("change", function() {
                this.render();
            }, this);
            this.render();
        },
        render: function() {
           
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            
            // Marcinál ez vmiért nem működik a hbs csak ezzel a módszerrel
            // for (var key in this.model.toJSON()) {
            //     this.$el.append("<td>" + this.model.toJSON()[key] + "</td>");
            // }

            if (this.userIdField !== undefined && this.userIdField !== "") {
                var id = this.$el.find("td:eq(" + (this.userIdField + 1) + ")").text();
                this.$el.find("td:first-child").html("<img src='//graph.facebook.com/" + id + "/picture' style='height:25px; width:25px;'>");
            }
            if (this.model.user_id !== "undefined") {
                this.$el.attr("data-id", this.model.user_id);
            }
        },
        events: {

        }
    });

    return ReportFieldListItemView;
});