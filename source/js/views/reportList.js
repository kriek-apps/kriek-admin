/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/reportList',
    'views/reportListItem',
], function($, _, Backbone, template, ReportListItemView) {
    'use strict';

    var ReportListView = Backbone.View.extend({
        template: template,
        tagName: "select",
        className: "report form-control",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template()).css("max-width", "185px");
            this.collection.each(function(item) {
                var reportListItem = new ReportListItemView({
                    model: item
                });
                this.$el.append(reportListItem.$el);
            }, this);
            this.trigger("render");
        },
        events: {
            "change": "selectReport"
        },
        selectReport: function() {
            //In the case of reports, do an url rewrite
            if ($(".notificationWizard").length === 0) {
                var id = this.$el.find(":selected").attr('data-id');
                var temp = window.location.hash.split("reports");
                temp[1] = "/"+id;
                window.location.hash = temp.join("reports");
            }
            
        }
    });

    return ReportListView;
});