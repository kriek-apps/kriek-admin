/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/reportListItem',
    'models/report'
], function ($, _, Backbone, template, ReportModel) {
    'use strict';

    var ReportListItemView = Backbone.View.extend({
        template: template,
        tagName : "option",
        className : "report",
        initialize : function() {
           this.render();
        },
        render : function() {
           this.$el.html(this.template({model : this.model.toJSON()}));
           this.$el.attr('data-id', this.model.get('id'));
        },
        events : {
           
        }
    });

    return ReportListItemView;
});