/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/errorDetails',
], function($, _, Backbone, template) {
    'use strict';

    var ErrordetailsView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "errorDetails",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#userDetailsModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events: {

        }
    });

    return ErrordetailsView;
});