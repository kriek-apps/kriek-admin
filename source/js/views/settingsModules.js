/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/settingsModules',
], function($, _, Backbone, template) {
    'use strict';

    var SettingsmodulesView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "settingsModules",
        initialize: function() {
            this.render();
            this.model.on("change", function() {
                this.render();
            }, this);
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            this.$el.find(".settings .form-group:has(input[data-key=admins])").remove();
        },
        events: {
            "click .on-off": "switch",
            "keyup input" : "unsavedChanges",
            "click .saveButton" : "saveChanges"
        },
        switch: function(event) {
            var el = $(event.currentTarget);
            el.toggleClass("on");
            var active = el.hasClass('on');
            var moduleContainer = el.parent().parent().parent();
            moduleContainer.toggleClass("panel-primary").toggleClass("activeModule");
            var moduleName = moduleContainer.attr("data-key");
            var appSettings = this.model.get("config");
            appSettings.public.modules[moduleName].active = active;
            this.model.set("config", appSettings);
            this.model.trigger("change");
            this.model.save();
        },
        unsavedChanges : function(event) {
            var el = $(event.currentTarget);
            el.parent().parent().find(".unsaved").show();
        },
        saveChanges : function(event) {
            var el = $(event.currentTarget);
            var moduleContainer = el.parent();
            var moduleName = el.attr("data-key");
            var appSettings = this.model.get("config");
            var moduleSettings = appSettings.public.modules[moduleName];
            var inputs = moduleContainer.find("input");
            for (var i = 0; i < inputs.length; i++) {
                var input = $(inputs[i]);
                moduleSettings[input.attr("data-key")] = input.val();
            }
            appSettings.public.modules[moduleName] = moduleSettings;
            this.model.set("config", appSettings);
            this.model.trigger("change");
            this.model.save();
            moduleContainer.find(".unsaved").hide();
        }
    });

    return SettingsmodulesView;
});