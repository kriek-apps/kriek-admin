/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/quizListItem',
  'models/quiz',
  'views/editQuestion',
  'views/confirmationModal'
], function($, _, Backbone, template, QuizModel, EditQuestionView, ConfirmationModal) {
  'use strict';

  var QuizListItemView = Backbone.View.extend({
    template: template,
    tagName: "tr",
    className: "quiz",
    initialize: function(data) {
      this.app = data.app;
      this.render();
      this.model.on("change", function() {
        this.render();
      }, this);
    },
    render: function() {
      this.$el.html(this.template({
        model: this.model.toJSON()
      }));
    },
    events: {
      "click": "edit",
      "click .close": "deleteQuestion"
    },
    edit: function(event) {
      var modal = new EditQuestionView({
        model: this.model,
        app : this.app
      });
    },
    deleteQuestion: function(event) {
      event.stopImmediatePropagation();
      var modal = new ConfirmationModal();
      modal.on("confirmed", function() {
        this.model.destroy();
      }, this);
    }
  });

  return QuizListItemView;
});