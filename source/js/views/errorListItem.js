/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/errorListItem',
    'models/error',
    'views/errorDetails',
    'views/userDetails'
], function($, _, Backbone, template, ErrorModel, ErrorDetailsView, UserDetailsView) {
    'use strict';

    var ErrorListItemView = Backbone.View.extend({
        template: template,
        tagName: "tr",
        className: "error",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            this.$el.attr("data-id", this.model.get("id"));
            this.$el.attr("data-origin", this.model.get("origin"));

        },
        events: {
            "click" : "showDetails",
            "click .close" : "deleteError",
            "click .user" : "showUser"
        },
        showDetails: function() {
            var modal = new ErrorDetailsView({model: this.model});
        },
        showUser: function(event) {
            event.stopImmediatePropagation();
            var id = $(event.currentTarget).attr("data-id");
            var modal = new UserDetailsView({
                userId : id
            });
        },
        deleteError: function(e) {
            e.stopImmediatePropagation();
            this.model.destroy();
        }
    });

    return ErrorListItemView;
});