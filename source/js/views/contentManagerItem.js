/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/contentManagerItem',
    'views/contentManagerEditItem',
    'currentApp',
    'handlebars'
], function($, _, Backbone, template, ContentManagerEditItemView, CurrentApp, Handlebars) {
    'use strict';

    var ContentmanageritemView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "contentManagerItem",
        initialize: function(obj) {
            this.folderID = obj.folderID;
            this.id = obj.id;
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model
            }));
            var adminTemplate = CurrentApp.getModel().get("briefcase")[this.folderID].adminTemplate;
            adminTemplate = Handlebars.compile(adminTemplate);
            this.$el.find(".customDisplay").html(adminTemplate({
                model: this.model
            }));
        },
        events: {
            "click .detailsBtn": "details",
            "click .deleteBtn": "deleteItem"
        },
        details: function() {
            var modal = new ContentManagerEditItemView({
                model: this.model,
                id: this.id,
                folderID : this.folderID
            });
            modal.on("modelChanged", function() {
                this.trigger("modelChanged");
            }, this);
        },
        deleteItem: function() {
            //this.$el.remove();
            var briefcase = CurrentApp.getModel().get("briefcase");
            briefcase[this.folderID].contents.splice(this.id, 1);
            CurrentApp.getModel().set("briefcase", briefcase);
            CurrentApp.getModel().save();
            this.trigger("modelChanged");
        }
    });

    return ContentmanageritemView;
});