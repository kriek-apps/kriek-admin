/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/queryListItem',
    'models/report',
    'views/queryWizard',
    'views/confirmationModal'
], function($, _, Backbone, template, QueryModel, QueryWizard, ConfirmationModal) {
    'use strict';

    var QueryListItemView = Backbone.View.extend({
        template: template,
        tagName: "li",
        className: "query",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
        },
        events: {
            "click .panel-heading": "openWizard",
            "click .close": "delete"
        },
        openWizard: function(event) {
            var wizard = new QueryWizard({
                model: this.model
            });
        },
        delete: function(event) {
            event.stopImmediatePropagation();
            var confirm = new ConfirmationModal();
            confirm.on("confirmed", function() {
                this.model.destroy();
            }, this);
        }
    });

    return QueryListItemView;
});