/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/contentUploader',
    'views/contentManagerSchemaEditor',
    'currentApp',
    'views/contentManagerItem',
    'views/contentManagerEditItem',
    'views/addDefinition',
    'views/definition'
], function($, _, Backbone, template, ContentManagerSchemaEditor, CurrentApp, ContentManagerItemView, ContentManagerEditItemView, AddDefinitionView, DefinitionView) {
    'use strict';

    var ContentuploaderView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "contentUploader",
        initialize: function(data) {
            this.folderID = data.id;
            this.render();
        },
        render: function() {
            var self = this;
            var model = this.model.get("briefcase")[this.folderID];
            this.briefcase = model;
            this.$el.html(this.template({
                model: model
            }));
            var items = CurrentApp.getModel().get("briefcase")[this.folderID].contents;
            //Collection
            if (this.briefcase.type == "collection") {
                for (var i = 0; i < items.length; i++) {
                    var item = new ContentManagerItemView({
                        model: items[i],
                        id: i,
                        folderID : this.folderID
                    });
                    item.on("modelChanged", function() {
                        self.render();
                    });
                    this.$el.find(".contents").append(item.$el);
                }
            } else {
            //Definition
                for (var i in items) {
                    var view = new DefinitionView({model : items[i], key : i});
                    this.$el.find(".contents").append(view.$el);
                }
            }
        },
        events: {
            "click .addNewItemBtn": "addNewItem",
            "click .saveBtn" : "saveItems"
        },
        saveItems: function() {
            var contents = {};
            var inputs = this.$el.find(".dataContainer");
            for (var i = 0; i < inputs.length; i++) {
                var el = $(inputs[i]);
                var input = el.find("input");
                contents[input.attr("data-key")] = {
                    type : input.attr("data-type"),
                    description : el.find(".description").text(),
                    value : input.val()
                };
            }
            var briefcase = CurrentApp.getModel().get("briefcase");
            briefcase[this.folderID].contents = contents;
            CurrentApp.getModel().set("briefcase", briefcase);
            CurrentApp.getModel().save();
        },
        addNewItem: function() {
            if (this.briefcase.type == "collection") {
                var modal = new ContentManagerEditItemView({ folderID : this.folderID });
                modal.on("modelChanged", function() {
                    this.render();
                }, this);
            } else {
                var modal = new AddDefinitionView();
                modal.on("addField", function(data) {
                    console.log(data);
                    var view = new DefinitionView({model : {
                        type : data.type,
                        description : data.description,
                        value: ""
                    }, key : data.name});
                    this.$el.find(".contents").append(view.$el);
                    this.$el.find(".unsaved").show();
                }, this);
            }
        }
    });

    return ContentuploaderView;
});