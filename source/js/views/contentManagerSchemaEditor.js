/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/contentManagerSchemaEditor',
    'currentApp',
    'handlebars'
], function($, _, Backbone, template, CurrentApp, Handlebars) {
    'use strict';

    var ContentManagerSchemaEditorView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "contentManagerSchemaEditor",
        initialize: function(obj) {
            this.folderID = obj.folderID;
            this.render();
        },
        render: function() {
            var schema = CurrentApp.getModel().get("briefcase")[this.folderID].schema;
            this.$el.html(this.template({
                fields: schema
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#schemaEditor").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events: {
            "click .addFieldBtn": "addField",
            "click .close": "deleteField",
            "click .saveBtn": "save",
            "change select": "changeType"
        },
        changeType: function(event) {
            var el = $(event.currentTarget);
            var type = $(el).find("option:selected").attr("data-type");
            $(el).siblings("textarea").remove();
            if (type == "object" || type == "select" || type == "image") {
                $(el).siblings("hr").before("<textarea class='form-control' style='margin-top:5px;'></textarea>");
            }
        },
        addField: function() {
            var newTemplate = this.$el.find("script.newFieldTemplate").html();
            newTemplate = Handlebars.compile(newTemplate);
            this.$el.find(".addFieldBtn").before(newTemplate());
        },
        deleteField: function(event) {
            var el = $(event.currentTarget);
            el.parent().remove();
        },
        save: function() {
            var data = [];
            var fields = this.$el.find(".field");
            for (var i = 0; i < fields.length; i++) {
                var name = $(fields[i]).find("input.name").val();
                var title = $(fields[i]).find("input.title").val();
                var description = $(fields[i]).find("input.description").val();
                var defaultValue = $(fields[i]).find("input.default").val();
                var type = $(fields[i]).find("select option:selected").attr("data-type");
                data[i] = {
                    name: name,
                    type: type,
                    description : description,
                    defaultValue : defaultValue,
                    title : title
                };
                if (type == "object" || type == "image" || type == "select") {
                    data[i].object = $(fields[i]).find("textarea").val();
                }
            }
            var briefcase = CurrentApp.getModel().get("briefcase");
            briefcase[this.folderID].schema = data;
            CurrentApp.getModel().set("briefcase", briefcase);
            CurrentApp.getModel().save();
            this.$el.find("#schemaEditor").modal("hide")
        }
    });

    return ContentManagerSchemaEditorView;
});