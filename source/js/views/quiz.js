/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/quiz',
    'views/quizList',
    'views/editQuestion',
    'models/quiz'
], function($, _, Backbone, template, QuizListView, EditQuestionView, QuestionModel) {
    'use strict';

    var QuizView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "quiz",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            this.listView = new QuizListView({
                id: this.model.get('id'),
                app : this.model
            });
            this.listView.collection.on('reset', function() {
                this.$el.find(".results").html(this.listView.$el);
            }, this);
        },
        events: {
            "click .addQuestionBtn": "addQuestion"
        },
        addQuestion: function() {
            var model = new QuestionModel({
                appId: this.model.get('id')
            });

            var modal = new EditQuestionView({
                model: model,
                app: this.model
            });
            modal.on("editFinish", function() {
                this.listView.collection.fetch();
            }, this);
        }
    });

    return QuizView;
});