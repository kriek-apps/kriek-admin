/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/sweepstakes',
    'currentApp',
    'views/sweepstakesAddPrizes'
], function($, _, Backbone, template, CurrentApp, AddPrizesModal) {
    'use strict';

    var SweepstakesView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "sweepstakes",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
        },
        events: {
            "click .addPrizes" : "addPrizes"
        },
        addPrizes: function() {
            var modal = new AddPrizesModal();
        }
    });

    return SweepstakesView;
});