/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/notificationListItem',
    'views/notificationDetailsWizard'
], function($, _, Backbone, template, NotificationDetailsWizard) {
    'use strict';

    var NotificationListItemView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "notificationListItem",
        initialize: function() {
            this.render();
        },
        render: function() {
            var filters = [];
            var settings = this.model.toJSON().settings.filters;
            for (var key in settings) {
                if (settings[key] !== "") {
                    filters.push({
                        key: key,
                        filter: settings[key]
                    });
                }
            }
            this.$el.html(this.template({
                model: this.model.toJSON(),
                filters : filters,
                data : JSON.parse(this.model.toJSON().data || "{}")
            }));
        },
        events: {
            "click .details": "openDetailsWizard"
        },
        openDetailsWizard: function() {
            var wizard = new NotificationDetailsWizard({
                model: this.model
            });
        }
    });

    return NotificationListItemView;
});