/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'facebooksdk',
    'hbs!templates/notificationDetailsWizard',
    'hbs!templates/notifications-add',
    'hbs!templates/notifications-finalize',
    'collections/report',
    'views/reportList',
    'views/notificationDetailsTable',
    'hbs!templates/reportFieldHeaderSearch',
    'models/notification',
    'hbs!templates/notification-loading',
    'collections/reportField',
    'models/app'



], function($, _, Backbone, FB, template, addTemplate, finalizeTemplate, ReportCollection, ReportListView, ReportFieldListView, reportFieldHeaderSearchTemplate, NotificationModel, notificationLoadingTemplate, ReportFieldCollection, AppModel) {
    'use strict';

    var NotificationWizardView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "notificationWizard",
        initialize: function() {
            console.log(this.model.toJSON());
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#notificationModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
            this.switchToView("details");
        },
        events: {
            "click .resend": "resend"
        },
        switchToView: function(viewName) {

            if (viewName == "details") {
                //Add report selection
                this.displayReportData(this.model.get("settings").queryID);
            }
            if (viewName == "loading") {
                //Header
                this.$el.find(".modal-header p").text("Sending notifications...");
                //Body
                this.$el.find(".modal-body").html(notificationLoadingTemplate({
                    users: this.selectedUsers
                }));
                //Send notifications
                var app = new AppModel({
                    id: this.model.get("app_id")
                });
                app.on("reset change", function() {
                    this.appToken = app.get("config").private.app_token.value;
                    this.sendNotifications();
                }, this);
                app.fetch({
                    reset: true
                });

            }


        },
        sendNotifications: function(token) {
            this.currentlyFinished = 0;
            this.cache = [];
            this.sendOneNotif();

        },
        sendOneNotif: function() {
            var max = this.selectedUsers.length;
            var step = 100 / this.selectedUsers.length;
            var self = this;
            var i = this.currentlyFinished;
            var currentUser = this.selectedUsers[i];
            FB.api('/' + this.selectedUsers[i] + '/notifications/', 'POST', {
                template: this.model.get("settings").message,
                access_token: this.appToken,
                href: this.model.get("settings").link
            }, function(message) {
                if (message.success !== true) {
                     console.log("FB notification error: ", message, " on user: ", currentUser);
                }
                self.cache.push(self.selectedUsers[i]);
                self.currentlyFinished++;
                $(".progress-bar").css("width", step * self.currentlyFinished + "%");
                $(".progress-counter").text(self.currentlyFinished + "/" + max);
                //save to the database
                if (self.cache.length % 10 === 0 || self.currentlyFinished == max) {
                    var sent_to = self.model.get("sent_to");
                    sent_to = sent_to.concat(self.cache);
                    self.model.set("sent_to", sent_to);
                    self.cache = [];
                    self.model.save();
                }
                if (self.currentlyFinished == max) {
                    self.$el.find("#notificationModal").modal("hide");
                    self.selectedUsers = "";
                } else {
                    var r = Math.floor(Math.random()*400)+200;
                    setTimeout(function() {self.sendOneNotif()}, r);
                }
            });
        },
        displayReportData: function(id) {
            var data = {
                appId: this.model.get("app_id"),
                reportId: id,
                notificationModel: this.model
            };
            this.currentReport = id;
            var view = new ReportFieldListView(data);
            this.$el.find(".modal-body").html(view.$el);
            this.table = view;
            this.getNumberOfNewUsers();
            //insert search fields
            view.collection.on("reset", function() {


            }, this);
        },
        getNumberOfNewUsers: function() {
            var notifiedUsers = this.model.get("sent_to");
            var allUsers = new ReportFieldCollection({
                appId: this.model.get("app_id"),
                reportId: this.model.get("settings").queryID
            });
            allUsers.on("reset", function() {
                var filters = this.model.get("settings").filters;
                allUsers = _.filter(allUsers.toJSON(), function(e) {
                    /******************************************************************************/
                    var passes = true;

                    for (var key in filters) {
                        var orStatements = filters[key].split("+");
                        var orMatch = false;
                        for (var i = 0; i < orStatements.length; i++) {
                            if (orStatements[i][0] == "!") {
                                if (String(e[key]).toLowerCase().indexOf(String(orStatements[i]).substr(1).toLowerCase()) == -1) orMatch = true;
                            } else {
                                if (String(e[key]).toLowerCase().indexOf(String(orStatements[i]).toLowerCase()) > -1) orMatch = true;
                            }
                        }
                        if (orMatch == false) {
                            passes = false;
                        }
                    }

                    return passes;
                    /*****************************************************************************/
                });
                for (var i = 0; i < notifiedUsers.length; i++) {
                    notifiedUsers[i] = notifiedUsers[i] + "";
                }
                var toNotify = _.filter(allUsers, function(e) {
                    return notifiedUsers.indexOf(e.user_id + "") == -1;
                });
                var newUsers = toNotify.length;
                var string = newUsers == 0 ? "There are no new users for this query." : "There are " + newUsers + " new users for this query.";
                if (newUsers === 0) {
                    this.$el.find(".resend").hide();
                }
                $("div.reportField").before("<p>" + string + "</p>");

            }, this);
            allUsers.fetch({
                reset: true
            });
        },
        filter: function() {
            var filters = {};
            var inputs = this.$el.find("input.searchField");
            for (var i = 0; i < inputs.length; i++) {
                var input = $(inputs[i]);
                filters[input.attr("data-key")] = input.val();
            }
            this.currentFilters = filters;
            this.table.conditionsFilter(filters);
        },
        finalize: function() {
            this.selectedUsers = _.pluck(this.table.getSelectedUsers().toJSON(), "user_id");
            this.switchToView("finalize");
        },
        changeReport: function(event) {
            var el = $(event.currentTarget);
            this.displayReportData(el.find("option:selected").attr("data-id"));
        },

        resend: function() {
            var notifiedUsers = this.model.get("sent_to");
            var allUsers = new ReportFieldCollection({
                appId: this.model.get("app_id"),
                reportId: this.model.get("settings").queryID
            });
            allUsers.on("reset", function() {
                    //filter down the users
                    /*      *******************************************************       */
                    var filters = this.model.get("settings").filters;
                    allUsers = _.filter(allUsers.toJSON(), function(e) {
                        var obj = e;
                        var match = true;
                        $.each(obj, function(key, value) {
                            var orStatements = filters[key].split("+");
                            var orMatch = false;
                            for (var i = 0; i < orStatements.length; i++) {
                                if (orStatements[i][0] == "!") {
                                    if (String(value).toLowerCase().indexOf(orStatements[i].substr(1).toLowerCase()) == -1) orMatch = true;
                                } else {
                                    if (String(value).toLowerCase().indexOf(orStatements[i].toLowerCase()) > -1) orMatch = true;
                                }

                            }
                            if (orMatch === false) {
                                match = false;
                            }
                        });
                        return match;
                    });
                    /*      *******************************************************       */
                    for (var i = 0; i < notifiedUsers.length; i++) {
                        notifiedUsers[i] = notifiedUsers[i] + "";
                    }
                    var toNotify = _.filter(allUsers, function(e) {
                        return notifiedUsers.indexOf(e.user_id + "") == -1;
                    });
                    this.selectedUsers = _.pluck(toNotify, "user_id");
                    this.switchToView("loading");
                },
                this);
            allUsers.fetch({
                reset: true
            });

        }
    });

    return NotificationWizardView;
});