/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/imageProcessWizard',
], function ($, _, Backbone, template) {
    'use strict';

    var ImageprocesswizardView = Backbone.View.extend({
        template: template,
        tagName : "div",
        className : "imageProcessWizard",
        initialize : function(data) {
           this.processes = data.processes;
           this.render();
        },
        render : function() {
           this.$el.html(this.template({processes : this.processes}));
           $("body").append(this.$el);
           this.$el.find("#processWizard").modal("show").show();
        },
        events : {
           
        }
    });

    return ImageprocesswizardView;
});