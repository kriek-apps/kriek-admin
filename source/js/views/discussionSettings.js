/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/discussionSettings',
    'currentApp',
    'admin',
    'views/topicList',
    'handlebars'
], function($, _, Backbone, template, CurrentApp, Admin, TopicListView, Handlebars) {
    'use strict';

    var DiscussionsettingsView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "discussionSettings",
        initialize: function() {
            this.render();
            var self = this;
            this.$el.find('#tabs a').click(function (e) {
              e.preventDefault();
              $(this).tab('show');
              self.$el.find(".tab-pane").hide();
              $("#" + $(this).attr("data-target") ).show();
            });
        },
        render: function() {
            var admins = this.model.toJSON().config.public.modules.discussion.admins;
            this.$el.html(this.template({
                model: this.model.toJSON(),
                admins: admins
            }));
            var topics = new TopicListView();
            this.$el.find("#topics").html(topics.$el);
            var self = this;
            this.$el.find("#topics").show();
            this.uploadifyButtons();
        },
        events: {
            "click .addAdminBtn" : "addNewAdmin",
           "click .deleteBtn" : "deleteAdmin",
           "click .saveChangesBtn" : "saveChanges"
        },
        deleteAdmin: function(event) {
            var el = $(event.currentTarget);
            el.parent().parent().remove();
            this.$el.find(".uploadBtn").uploadifive("destroy");
            this.refreshIDs();
            this.uploadifyButtons();
        },
        addNewAdmin: function() {
            var adminTemplate = this.$el.find("#newAdminTemplate").html();
            adminTemplate = Handlebars.compile(adminTemplate);
            this.$el.find(".uploadBtn").uploadifive("destroy");
            this.$el.find(".addAdminBtn").before(adminTemplate());
            this.refreshIDs();
            this.uploadifyButtons();
        },
        refreshIDs: function() {
            this.$el.find("#admins input.uploadInput").each(function(index, obj) {
                $(this).attr("data-id", index);
            });
        },
        uploadifyButtons: function() {
            
            var self = this;
            this.$el.find(".uploadBtn").each(function() {
                var button = $(this);
                var id = button.parent().find("input").attr("data-id");
                button.attr("id", "upload-btn-" + id);
                $(this).uploadifive({
                    buttonClass : "btn btn-warning",
                    buttonText : "Upload",
                    width: 80,
                    formData: {
                    },
                    onUploadStart: function(file) {
                    },
                    multi: false,
                    uploadScript: globalConfig.api + CurrentApp.getID() + '/upload?admin_token=' + Admin.getToken(),
                    onUploadComplete: function(file, data, response) {
                        /* Parse data to json */
                        data = $.parseJSON(data);
                        self.$el.find("input.uploadInput:eq(" + id + ")").val(data.file_url);
                    }
                });
            });
        },
        saveChanges : function() {
            var admins = [];
            this.$el.find(".adminContainer").each(function() {
                var admin = {};
                $(this).find("input:not([type=file])").each(function() {
                    admin[$(this).attr("data-field")] = $(this).val();
                });
                admins.push(admin);
            });
            var config = this.model.get("config");
            config.public.modules.discussion.admins = admins;
            this.model.set("config", config);
            this.model.save();
        }
    });

    return DiscussionsettingsView;
});