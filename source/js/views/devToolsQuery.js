/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/devToolsQuery',
    'views/queryList',
    'views/queryWizard',
    'models/report'
], function($, _, Backbone, template, QueryListView, QueryWizardView, ReportModel) {
    'use strict';

    var DevtoolsqueryView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "devToolsQuery",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
            this.list = new QueryListView({appId : this.model.get('id')});
            this.$el.find(".results").html(this.list.$el);
        },
        events: {
            "click .addQueryBtn" : "newQuery"
        },
        newQuery : function() {
            var emptyReport = new ReportModel({appId : this.model.get("id")});
            var wizard = new QueryWizardView({model : emptyReport});
            wizard.on("saved", function() {
                this.render();
            }, this);
        }
    });

    return DevtoolsqueryView;
});