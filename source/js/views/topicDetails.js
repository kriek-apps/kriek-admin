/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/topicDetails',
    'currentApp'
], function ($, _, Backbone, template, CurrentApp) {
    'use strict';

    var TopicdetailsView = Backbone.View.extend({
        template: template,
        tagName : "div",
        className : "topicDetails",
        initialize : function() {
           this.render();
        },
        render : function() {
           var extraFields = CurrentApp.getModel().toJSON().config.public.modules.discussion.extra_fields.split(",");
            this.$el.html(this.template({
                model: this.model.toJSON(),
                extraFields : extraFields
            }));
            $("body").append(this.$el);

            var self = this;
            this.$el.find("#quizModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events : {
           "click .saveChanges" : "saveChanges"
        },
        saveChanges: function() {
            var settings = {};
            var inputs = this.$el.find("input");
            for (var i = 0; i < inputs.length; i++) {
                if ($(inputs[i]).attr("data-key") == "question") {
                    settings[$(inputs[i]).attr("data-subkey")] = $(inputs[i]).val();
                } else {
                    settings[$(inputs[i]).attr("data-key")] = $(inputs[i]).val();
                }
            }
            var visible = this.model.get("visible") === undefined ? 1 : this.model.get("visible");
            this.model.set("visible", visible);
            this.model.set("data", settings);
            this.model.save();
            this.$el.find("#quizModal").modal("hide")
        }
    });

    return TopicdetailsView;
});