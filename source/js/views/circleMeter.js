/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
], function ($, _, Backbone) {
    'use strict';

    var CircleMeter = Backbone.View.extend({
        tagName : "canvas",
        className : "circleMeter",
        initialize : function(data) {
            _.extend(this, data);
            this.render();
        },
        render : function() {
            this.$el.attr("width", this.width);
            this.$el.attr("height", this.height);
            var ctx = this.$el[0].getContext("2d");
            //Draw gray circle
            ctx.beginPath();
            ctx.strokeStyle = "cfcfcf";
            ctx.lineWidth = 3;
            ctx.lineCap = "round";
            ctx.arc(this.width/2, this.height/2,(this.height/2) * 0.9,0,2*Math.PI);
            ctx.stroke();
            //Draw arc
            ctx.beginPath();
            var endPoint = 2*Math.PI*this.percent - 0.5*Math.PI === Math.PI * 1.5 ? 4*Math.PI*this.percent : 2*Math.PI*this.percent - 0.5*Math.PI;
            ctx.arc(this.width/2, this.height/2, (this.height/2) * 0.9, Math.PI * 1.5, endPoint);
            ctx.strokeStyle = this.percent >= 0.66 ? "#5cb85c" : "#f0ad4e";
            if (this.percent <= 0.33) {
                ctx.strokeStyle = "#d9534f"
            }
            ctx.lineWidth = 6;
            ctx.lineCap = "round";
            ctx.stroke();
        },
        events : {
        },
        
    });

    return CircleMeter;
});