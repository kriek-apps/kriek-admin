/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'hbs!templates/discussionListItem',
    'models/discussion',
    'currentApp'
], function($, _, Backbone, Handlebars, template, DiscussionModel, CurrentApp) {
    'use strict';

    var DiscussionListItemView = Backbone.View.extend({
        template: template,
        tagName: "li",
        className: "discussion",
        initialize: function() {
            this.render();
            this.model.on("change", function() {
                this.render();
            }, this);
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));

            var adminTemplate = Handlebars.compile(CurrentApp.getModel().get("config").public.modules.discussion.adminTemplate);
            this.$el.find(".templateContainer.question").html(adminTemplate({model: this.model.toJSON()}));

        },
        events: {
            "click .sendAnswerBtn" : "sendAnswer",
            "click .visibilityBtn" : "toggleVisibility",
            "click .deleteAnswerBtn" : "deleteAnswer"
        },
        deleteAnswer: function(event) {
            var el = $(event.currentTarget);
            var id = el.attr("data-id");
            var self = this;
            el.attr("disabled", "disabled");
            $.ajax({
                method: "DELETE",
                url : "/api/" + CurrentApp.getID() + "/chat/questions/" + this.model.get("id") + "/answers/" + id,
                success: function() {
                    self.model.fetch();
                }
            });
        },
        toggleVisibility : function(event) {
            var el = $(event.currentTarget);
            el.attr("disabled", "disabled");
            if (this.model.get("visible") === 1) {
                this.model.set("visible", 0);
            } else {
                this.model.set("visible", 1);
            }
            this.model.save();
        },
        sendAnswer: function() {
            var button = this.$el.find(".sendAnswerBtn");
            button.attr("disabled", "disabled");
            var answerText = this.$el.find(".answerInput").val();
            var adminData = $(".adminSelector option:selected");
            var answerObj = {
                admin : true,
                name : adminData.attr("data-name"),
                picture : adminData.attr("data-picture"),
                txt : answerText
            };
            answerObj = {
                answer : answerObj
            };
            var self = this;
            $.ajax({
                method : "POST",
                contentType : "application/json",
                dataType : "json",
                url : globalConfig.api + CurrentApp.getID() + "/chat/questions/" + this.model.get("id") + "/answers",
                data : JSON.stringify(answerObj),
                success: function() {
                    self.model.fetch();
                    if ($(".filterGroup button:last").hasClass("active")) {
                        setTimeout(function() {
                            self.$el.fadeOut();
                        }, 1500);
                    }
                }
            });
        }
    });

    return DiscussionListItemView;
});