/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/appDetails',
  'views/sidebar'
], function($, _, Backbone, template, SidebarView) {
  'use strict';

  var AppDetailsView = Backbone.View.extend({
    template: template,
    tagName: "div",
    className: "appDetails row",
    sidebarView: {},
    initialize: function() {
      this.render();
    },
    render: function() {
      this.$el.html(this.template({
        model: this.model.toJSON()
      }));
      this.sidebarView = new SidebarView({
        model: this.model
      });
      this.$el.find("aside").html(this.sidebarView.$el);
      this.activeLink();
    },
    events: {

    },
    activeLink: function() {
      var hash = window.location.hash.split('/');
      var page = hash[2];
      if (typeof page === "undefined") {
        this.$el.find('.information').addClass('active');
      } else {
        this.$el.find('.' + page).addClass('active');
      }
      
    },
    openSettings : function(option) {
      this.sidebarView.openSettings(option);
    },
    openDevTools : function(option) {
      this.sidebarView.openDevTools(option);
    },
    openDiscussion: function(option) {
      this.sidebarView.openDiscussion(option);
    },
    openBriefcase: function(option) {
      this.sidebarView.openBriefcase(option);
    },
    openInsights: function(option) {
      this.sidebarView.openInsights(option);
    },
    showBriefcaseFolders : function() {
      this.sidebarView.showBriefcaseFolders();
    }
  });

  return AppDetailsView;
});