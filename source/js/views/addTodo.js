/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/addTodo',
    'currentApp',
    'datepicker'
], function ($, _, Backbone, template, CurrentApp) {
    'use strict';

    var AddToDoView = Backbone.View.extend({
        template: template,
        tagName : "div",
        className : "addTodo",
        initialize : function(data) {
            this.link = data.link || "";
            this.message = data.message || "";
            this.render();
        },
        render : function() {
           this.$el.html(this.template({
                link : this.link,
                message: this.message,
                appID: CurrentApp.getID()
           }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#todoModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
            if (this.link === "") {
                this.$el.find("input.link").val("#/" + CurrentApp.getID());
            }
            this.$el.find("input[type=datetime]").appendDtpicker({
                "firstDayOfWeek": 1,
                "dateFormat": "YYYY-MM-DD hh:mm",
                "closeOnSelected": true,
                "todayButton": false,
                "inline" : true,
                "calendarMouseScroll": false
            });
        },
        events : {
            "click .saveChanges" : "save",
            "change .linkSelect" : "selectLink"
        },
        save: function() {
            if (this.$el.find(".message").val() == "") return;
            var task = {
                text : this.$el.find(".message").val(),
                type : "task",
                link : this.$el.find(".link").val(),
                date : this.$el.find(".date").val(),
                completed : false
            };
            var config = CurrentApp.getModel().get("config");
            config.private.warnings.push(task);
            CurrentApp.getModel().set("config", config);
            CurrentApp.getModel().save();
            this.$el.find("#todoModal").modal("hide");
            this.sendEmail();

        },
        sendEmail: function() {
            var email = this.$el.find("select.notif").val();
            console.log(CurrentApp.getModel().toJSON().name);
            if (email !== "") {
                $.ajax({
                    method: "POST",
                    data: {
                        from: "admin@cnvs.io",
                        to: email,
                        subject: "Admin notification regarding " + CurrentApp.getModel().toJSON().name,
                        html: "Someone tagged you with the following task:<br><a href='"+window.location.origin + "/admin/" + this.$el.find(".link").val()+"'>" + this.$el.find(".message").val() + "</a>"
                    },
                    url: globalConfig.api + "email/send"
                })
            }
        },
        selectLink: function(event) {
            var link = $(event.currentTarget).val();
            this.$el.find(".link").val(link);

        }
    });

    return AddToDoView;
});