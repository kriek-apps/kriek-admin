/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/sweepstakesAddPrizes',
    'currentApp',
    'md5',
    'datepicker',
    'vendor/dateFormat'
], function($, _, Backbone, template, CurrentApp, md5) {
    'use strict';

    var SweepstakesAddPrizesModal = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "sweepstakesAddPrizes",
        initialize: function() {
            this.render();
        },
        render: function() {
            var prizes = _.find(CurrentApp.getModel().get("briefcase"), function(o) {
                return o.shorthand === "prizes"
            }).contents;
            this.$el.html(this.template({
                prizes: prizes
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#addPrizes").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
            this.$el.find("input[type=datetime]").appendDtpicker({
                "firstDayOfWeek": 1,
                "dateFormat": "YYYY-MM-DD hh:mm",
                "closeOnSelected": true,
                "todayButton": false,
                "inline" : true,
                "calendarMouseScroll": false
            });
        },
        events: {
            "click button.add" : "add"
        },
        add: function() {
            var params = {
                prize: this.$el.find(".prize").val(),
                amount: this.$el.find(".amount").val(),
                start: this.$el.find(".start").val(),
                end: this.$el.find(".end").val()
            }
            if (params.amount < 1) {
                return;
            }
            var dates = this.generateDates(params.amount, params.start, params.end);
            var array = [];
            for (var i = 0; i < dates.length; i++) {
                array[i] = {
                    prize: params.prize,
                    date: dates[i].format("yyyy-mm-dd HH:MM:ss"),
                    available: 1
                }
                array[i].id = md5(JSON.stringify(array[i]) + new Date() + Math.random());
            }
            console.log(array);
            var briefcase = CurrentApp.getModel().get("briefcase");
            var folder;
            for (var i = 0; i < briefcase.length; i++) {
                if (briefcase[i].shorthand === "sweepstakes") {
                    folder = i;
                }
            }
            briefcase[folder].contents = briefcase[folder].contents.concat(array);
            CurrentApp.getModel().set("briefcase", briefcase);
            CurrentApp.getModel().save();
            this.$el.find("#addPrizes").modal("hide");

        },
        generateDates: function(n, start, end) {
            start = new Date(start).getTime();
            end = new Date(end).getTime();
            var d = end - start;
            var k = Math.floor(d / n);
            var array = [];
            for (var i = 0; i < n; i++) {
                array.push(start + (k/2) + i * k);
            }
            for (var i = 0; i < array.length; i++) {
                var diff = Math.floor( (Math.random() - 0.5) * (k/2) );
                if (i === 0 && diff < 0) {
                    diff = diff + (k/2);
                }
                if (i === array.length - 1 && diff > 0) {
                    diff = diff - (k/2);
                } 
                array[i] = array[i] + diff;
                array[i] = new Date(array[i]);
            }
            return array;
        }
    });

    return SweepstakesAddPrizesModal;
});