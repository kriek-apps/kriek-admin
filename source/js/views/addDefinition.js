/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/addDefinition',
    'bootstrap'
], function($, _, Backbone, template) {
    'use strict';

    var AddDefinitionView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "addLanguageField",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template());
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#definitionModal").modal("show").show().on('hidden.bs.modal', function() {
                self.$el.remove();
            });
        },
        events: {
            "click .done": "done"
        },
        done: function() {
            if (this.$el.find("input").val() !== "") {
                this.trigger("addField", {
                    name: this.$el.find("input.fieldName").val(),
                    description: this.$el.find("input.description").val(),
                    type : this.$el.find("select option:selected").attr("data-type")
                });
            }
            this.$el.find("#definitionModal").modal("hide");
        }
    });

    return AddDefinitionView;
});