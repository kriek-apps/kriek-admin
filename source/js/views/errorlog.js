/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/errorlog',
    'views/errorList',
    'views/confirmationModal',
    'currentApp'
], function ($, _, Backbone, template, ErrorListView, ConfirmationModal, CurrentApp) {
    'use strict';

    var ErrorlogView = Backbone.View.extend({
        template: template,
        tagName : "div",
        className : "errorlog",
        initialize : function() {
            this.render();
        },
        render : function() {
            this.$el.html(template());
            this.table = new ErrorListView();
            this.$el.find(".results").html(this.table.$el);
        },
        events : {
           "click .filterBtn" : "filter",
           "keyup .search-field" : "search",
           "click .resetBtn" : "reset"
        },
        reset: function() {
            var self = this;
            var modal = new ConfirmationModal();
            modal.on("confirmed", function() {
                $.ajax({
                    url: globalConfig.api + CurrentApp.getID() + "/errorlog",
                    method: "DELETE",
                    complete : function(msg) {
                        self.render();
                    }
                });
            });
        },
        filter: function(event) {
            var el = $(event.currentTarget);
            this.$el.find(".filterBtn").removeClass("active");
            el.addClass("active");
            this.table.filter(el.attr("data-filter"));
        },
        search : function() {
            var term = this.$el.find(".search-field").val();
            this.table.search(term);
        }
    });

    return ErrorlogView;
});