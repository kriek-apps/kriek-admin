/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/settingsImage',
    'views/imageProcessWizard'
], function ($, _, Backbone, template, ImageProcessWizardTemplate) {
    'use strict';

    var SettingsimageView = Backbone.View.extend({
        template: template,
        tagName : "div",
        className : "settingsImage",
        initialize : function() {
           this.render();
        },
        render : function() {
           this.$el.html(this.template({model : this.model.toJSON()}));
        },
        events : {
           "click h3" : "openDetails"
        },
        openDetails : function(event) {
            var el = $(event.currentTarget).parent().parent();
            var name = el.attr("data-name");
            var processes = this.model.get("config")['private']['imageProcesses'][name];
            var view = new ImageProcessWizardTemplate({processes : processes});
        }
    });

    return SettingsimageView;
});