/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/folderEditor',
    'currentApp',
    'admin'
], function($, _, Backbone, template, CurrentApp, Admin) {
    'use strict';

    var FolderEditorView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "contentManagerEditItem",
        initialize: function(obj) {
            if (obj !== undefined && obj.folderID !== undefined) {
                this.folderID = obj.folderID;
            }
            this.render();
        },
        render: function() {
            var folder;
            if (this.folderID !== undefined) {
                folder = CurrentApp.getModel().get("briefcase")[this.folderID];
            } else {
                //create new, empty folder
                folder = {
                    name : "",
                    description: "",
                    adminTemplate : "Default Admin Template",
                    type: "collection",
                    access: "public",
                    schema: [],
                    contents: [],
                    shorthand: ""
                };
            }
            this.folder = folder;
            this.$el.html(this.template({
                folder: folder
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#folderEditor").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events: {
            "click .saveBtn": "saveItem",
            "click .btn-group .btn" : "changeSwitch"
        },
        changeSwitch: function(event) {
            var el = $(event.currentTarget);
            el.siblings().removeClass("active");
            el.addClass("active");
        },
        saveItem: function() {
            var folder = {
                name : this.$el.find("input[data-name]").val(),
                description : this.$el.find("input[data-description]").val(),
                shorthand : this.$el.find("input[data-shorthand]").val(),
                adminTemplate : this.$el.find("input[data-adminTemplate]").val(),
                type : this.$el.find(".btn-group.type .active").attr("data-type"),
                access : this.$el.find(".btn-group.access .active").attr("data-access"),
                schema : this.folder.schema,
                contents : this.folder.contents
            };
            
            var briefcase = CurrentApp.getModel().get("briefcase");
            if (this.folderID !== undefined) {
                //Overwrite
                briefcase[this.folderID] = folder;
            } else {
                //New Item
                briefcase.push(folder);
            }
            CurrentApp.getModel().set("briefcase", briefcase);
            CurrentApp.getModel().save();
            this.$el.find("#folderEditor").modal("hide");
            this.trigger("modelChanged");
        }
    });

    return FolderEditorView;
});