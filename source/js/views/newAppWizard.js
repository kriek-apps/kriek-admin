/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/newAppWizard'
], function ($, _, Backbone, template) {
    'use strict';

    var NewappwizardView = Backbone.View.extend({
        template: template,
        tagName : "div",
        className : "newAppWizard",
        initialize : function() {
            var self = this;
            $.get(globalConfig.api + "clients", function(r) {
                self.clients = r;
                self.render();
            });
        },
        render : function() {
           this.$el.html(this.template({
                clients : this.clients
            }));
           this.$el.find("select.clients option:first").attr("selected", "selected");
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#newAppModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events : {
           "click .registerAppBtn" : "registerApp",
           "keyup .idInput" : "appIdEntered"
        },
        registerApp : function(event) {
            $(event.currentTarget).attr("disabled", "disabled");
            var self = this;
            $.ajax({
                url : "app_settings.json",
                method : "get",
                dataType : "json",
                contentType : "application/json",
                success: function(r) {
                    var appSettings = r;
                    $.ajax({
                        url : "language.json",
                        method: "get",
                        dataType : "json",
                        contentType : "application/json",
                        success : function(s) {
                            var language = s;
                            self.createApp(appSettings, language);
                        }
                    });
                },
                error: function(r) {
                    console.log(r);
                }
            });
        },
        appIdEntered : function() {
            var id = this.$el.find(".idInput").val();
            var self = this;
            if (Number(id) > 10000000) {
                FB.api("/" + id, "get", {}, function(r) {
                    self.appData = r;
                    self.$el.find("input[data-field=name]").val(r.name).removeAttr("disabled");
                    self.$el.find("input[data-field=path]").val("https://dev.cnvs.io:9000/");
                });
            }
        },
        createApp : function(appSettings, language) {
            appSettings.public.app_url.value = this.$el.find("input[data-field=path]").val();
            appSettings.public.app_fb_url.value = "http://apps.facebook.com/" + this.appData.namespace + "/";
            appSettings.public.app_fb_page_url.value = "http://facebook.com/KriekSandbox/app/" + this.$el.find("input[data-field=id]").val();
            appSettings.private.app_secret.value = this.$el.find("input[data-field=secret]").val();
            appSettings.private.app_token.value = this.$el.find("input[data-field=token]").val();

            var settings = {
                id : this.$el.find("input[data-field=id]").val(),
                name : this.$el.find("input[data-field=name]").val(),
                client_id : this.$el.find("select.clients option:selected").attr("data-id"),
                config : appSettings,
                briefcase : [],
                language : language
            };
            var self = this;
            $.ajax({
                method : "post",
                url : globalConfig.api + "apps",
                data : JSON.stringify(settings),
                contentType : "application/json",
                dataType : "json",
                success : function(r) {
                    self.trigger("appAdded");
                    self.$el.find("#newAppModal").modal("hide");
                    window.location.hash = "/" + settings.id + "/information"; 
                },
                error : function(s) {
                    console.log(s);
                }
            });
        }
    });

    return NewappwizardView;
});