/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/folderManager',
    'currentApp',
    'hbs!templates/folder',
    'views/folderEditor',
    'views/confirmationModal',
    'views/contentManagerSchemaEditor'
], function($, _, Backbone, template, CurrentApp, folderTemplate, FolderEditorView, ConfirmationModal, ContentManagerSchemaEditor) {
    'use strict';

    var FolderManagerView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "folderManager",
        initialize: function() {
            this.render();
            CurrentApp.getModel().on("change", function() {
                this.render();
            }, this);
        },
        render: function() {
            var model = CurrentApp.getModel().get("briefcase");
            this.$el.html(this.template());
            for (var i = 0; i < model.length; i++) {
                model[i].index = i;
                this.$el.find(".contents").append(folderTemplate({model : model[i]}));
            }
        },
        events: {
            "click .addNewFolderBtn" : "addNewFolder",
            "click .deleteBtn" : "deleteFolder",
            "click .editBtn" : "editFolder",
            "click .editSchemaBtn" : "editSchema"
        },
        addNewFolder: function() {
            var modal = new FolderEditorView();
            modal.on("modelChanged", function() {
                this.render();
            }, this);
        },
        editSchema: function(event) {
            var el = $(event.currentTarget);
            var folderID = $(el).parent().attr("data-index");
            var modal = new ContentManagerSchemaEditor({ folderID : folderID });
        },
        deleteFolder: function(event) {
            var el = $(event.currentTarget);
            var index = $(el).parent().attr("data-index");
            var confirm = new ConfirmationModal();
            confirm.on("confirmed", function() {
                var briefcase = CurrentApp.getModel().get("briefcase");
                briefcase.splice(index, 1);
                CurrentApp.getModel().save();
            }, this);
        },
        editFolder: function(event) {
            var el = $(event.currentTarget);
            var index = $(el).parent().attr("data-index");
            var modal = new FolderEditorView({folderID : index});
            modal.on("modelChanged", function() {
                this.render();
            }, this);
        }
    });

    return FolderManagerView;
});