/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/confirmationModal',
], function($, _, Backbone, template) {
    'use strict';

    var ConfirmationmodalView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "confirmationModal",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template({}));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#confirmationModal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events: {
            "click .confirm": "confirm"
        },
        confirm: function() {
            this.trigger("confirmed");
            this.$el.find("#confirmationModal").modal("hide");
        }
    });

    return ConfirmationmodalView;
});