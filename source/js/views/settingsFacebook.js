/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/settingsFacebook',
], function($, _, Backbone, template) {
    'use strict';

    var SettingsFacebookView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "settingsFacebook",
        data: {},
        initialize: function() {
            var self = this;
            this.loadData(function() {
                self.render();
            });
        },
        loadData : function(callback) {
            var self = this;
            //Documentation: https://developers.facebook.com/docs/graph-api/reference/app
            var url = "https://graph.facebook.com/" + this.model.get("id") + "?fields=";
            var fields = [
                "page_tab_url",
                "secure_page_tab_url",
                "app_domains",
                "website_url",
                "canvas_url",
                "secure_canvas_url",
                "privacy_policy_url",
                "terms_of_service_url",
                "mobile_web_url",
                "page_tab_default_name"
            ];
            url = url + fields.join(",") + "&method=get&format=json";
            FB.api(url, {
                access_token : this.model.get("config")["private"]["app_token"]["value"]
            }, function(r) {
                self.data = r;
                for (var i = 0; i < fields.length; i++) {
                    if (self.data[fields[i]] === undefined) {
                        self.data[fields[i]] = "";
                    }
                }
                self.data = _.omit(self.data, "id");
                callback();
            });
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON(),
                facebookData : this.data
            }));
            $(".unsaved").hide();
        },
        events: {
            "keyup input": "unsavedChange",
            "click .saveButton": "saveChanges",
            "click .autofillBtn" : "autofill"
        },
        unsavedChange: function() {
            this.$el.find(".unsaved").show();
        },
        saveChanges: function() {
            var data = {};
            var inputs = this.$el.find("input");
            for (var i = 0; i < inputs.length; i++) {
                var input = $(inputs[i]);
                data[input.attr("data-key")] = input.val();
            }
            var obj = {};
            for (var i in this.data) {
                if (i == "app_domains") {
                    obj[i] = data[i].split(",");
                } else {
                    obj[i] = data[i];
                }
            }
            obj.access_token = this.model.get("config")["private"]["app_token"]["value"];
            var self = this;
            FB.api("/" + this.model.get("id"), 'post', obj, function(r) {
                if (r.error === undefined) {
                    console.log("Ok", r);
                    self.loadData(function() {
                        self.render();
                    });
                } else {
                    console.log("Error", r);
                }
            });

        },
        autofill : function() {
            var settings = this.model.toJSON();
            this.$el.find("input[data-key=page_tab_url]").val("http://cnvs.eu1.frbit.net/"+settings.id+"/redirectToFacebookTab/");
            this.$el.find("input[data-key=secure_page_tab_url]").val("https://cnvs.eu1.frbit.net/"+settings.id+"/redirectToFacebookTab/");
            this.$el.find("input[data-key=app_domains]").val("cnvs.io");
            this.$el.find("input[data-key=website_url]").val("http://cnvs.io");
            this.$el.find("input[data-key=canvas_url]").val("http://cnvs.eu1.frbit.net/"+settings.id+"/redirectFromCanvas/");
            this.$el.find("input[data-key=secure_canvas_url]").val("https://cnvs.eu1.frbit.net/"+settings.id+"/redirectFromCanvas/");
            this.$el.find("input[data-key=privacy_policy_url]").val(settings.config.public.terms_and_conditions_url.value);
            this.$el.find("input[data-key=terms_of_service_url]").val(settings.config.public.terms_and_conditions_url.value);
            this.$el.find("input[data-key=mobile_web_url]").val(settings.config.public.app_url.value);
            this.$el.find("input[data-key=page_tab_default_name]").val(settings.name);
            this.unsavedChange();
        }
    });

    return SettingsFacebookView;
});