/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/spotlight',
    'collections/app',
    'admin'
], function($, _, Backbone, template, AppCollection, Admin) {
    'use strict';

    var SpotlightView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "spotlight",
        active: 0,
        initialize: function() {
            this.collection = new AppCollection();
            this.collection.on("reset", function() {
                this.createCommandList();
                this.render();
            }, this);
            this.collection.fetch({reset : true});
            Admin.on("adminChanged", function() {
                this.collection.fetch({reset : true});
                console.log("refetching Apps");
            }, this);
        },
        render: function() {
            this.$el.html( template() );
        },
        createCommandList: function() {
            var apps = this.collection.toJSON();
            this.commandList = [];
            for (var i = 0; i < apps.length; i++) {
                //Add App name
                this.commandList.push({
                    string : apps[i].name,
                    location : "/" + apps[i].id
                });
            }
            for (var i = 0; i < apps.length; i++) {
                //Settings
                this.commandList.push({
                    string : apps[i].name + " Settings",
                    location : "/" + apps[i].id + "/settings"
                });
                //Reports
                this.commandList.push({
                    string : apps[i].name + " Reports",
                    location : "/" + apps[i].id + "/reports"
                });
                //Modules
                this.commandList.push({
                    string : apps[i].name + " Modules",
                    location : "/" + apps[i].id + "/settings/modules"
                });
                //Notifications
                this.commandList.push({
                    string : apps[i].name + " Notifications",
                    location : "/" + apps[i].id + "/notifications"
                });
                //Briefcase
                this.commandList.push({
                    string : apps[i].name + " Briefcase",
                    location : "/" + apps[i].id + "/briefcase"
                });
                //Data Reset
                this.commandList.push({
                    string : apps[i].name + " Data Reset",
                    location : "/" + apps[i].id + "/dev/reset"
                });
                //Language
                this.commandList.push({
                    string : apps[i].name + " Language",
                    location : "/" + apps[i].id + "/settings/language"
                });
                //Error log
                this.commandList.push({
                    string : apps[i].name + " Error Log",
                    location : "/" + apps[i].id + "/dev/errorlog"
                });
                //Query Editor
                this.commandList.push({
                    string : apps[i].name + " Query Editor",
                    location : "/" + apps[i].id + "/dev/query"
                });
                //Open
                try {
                    this.commandList.push({
                        string : apps[i].name + " Open",
                        location : "external::"+apps[i].config.app_fb_url.value
                    });
                } catch(error) {}

                if (Admin.isDeveloper()) {
                    //Open dev
                    try {
                        this.commandList.push({
                            string : apps[i].name + " Open Dev",
                            location : "external::"+apps[i].config.public.app_url.value
                        });
                    } catch(error) {}
                }

                //Open Mobile
                try {
                    this.commandList.push({
                        string : apps[i].name + " Open Mobile",
                        location : "external::"+apps[i].config.public.app_url.value + "mobile/"
                    });
                } catch(error) {}

                //Open Terms and Conditions
                try {

                    if (apps[i].config.terms_and_conditions_url.value !== "") {
                        this.commandList.push({
                            string : apps[i].name + " Open Terms and Conditions",
                            location : "external::"+apps[i].config.terms_and_conditions_url.value
                        });
                    }
                } catch(err) {}


                //Basecamp
                if (Admin.isDeveloper()) {
                    try {
                    this.commandList.push({
                            string : apps[i].name + " Basecamp",
                            location : "external::"+apps[i].config.basecamp_url.value
                        });
                    } catch(error) {}
                }


                //Modules
                if (apps[i].config === undefined) continue;
                if (apps[i].config.public === undefined) continue;
                if (apps[i].config.public.modules === undefined) continue;
                for (var j in apps[i].config.public.modules) {
                    try {
                        if (apps[i].config.public.modules[j].active === true) {
                            //Add module
                            if (j == "sweepstake" || j == "custom" || j == "contentManager") continue;
                            var prettyName = j;
                            if (prettyName == "ugc") prettyName = "Contest";
                            if (prettyName == "quiz") prettyName = "Quiz";
                            if (prettyName == "discussion") prettyName = "Discussion";
                            this.commandList.push({
                                string : apps[i].name + " " + prettyName,
                                location : "/" + apps[i].id + "/" + j
                            });
                        }
                    } catch(err) {}
                }

            }
            //Endpoint Manager
            if (Admin.isDeveloper()) {
                this.commandList.push({
                    string : "Endpoint Manager",
                    location : "external::http://dev.kriekapps.com/admin/source/endpoints/?admin_token=" + Admin.getToken()
                });
            }
            //Live admin
            if (Admin.isDeveloper() && window.location.origin.indexOf("dev") > -1) {
                this.commandList.push({
                    string : "Live Admin",
                    location : "external::http://kriekapps.com/admin/"
                });
            }
            //Dev admin
            if (Admin.isDeveloper() && window.location.origin.indexOf("dev") == -1) {
                this.commandList.push({
                    string : "Development Admin",
                    location : "external::http://dev.kriekapps.com/admin/"
                });
            }
        },
        events: {
            "keyup input" : "typing"
        },
        show: function() {
            this.$el.find("input").val("");
            this.$el.show();
        },
        hide: function() {
            this.$el.hide();
        },
        typing: function(event) {
            //ENTER
            if (event.keyCode === 13) {
                this.navigate();
                //ESCAPE
            } else if (event.keyCode === 27) {
                this.hide();
                //DOWN
            } else if (event.keyCode === 40) {
                event.preventDefault();
                this.down();
                //UP
            } else if (event.keyCode === 38) {
                event.preventDefault();
                this.up();
            } else {
                this.fuzzySearch();
            }
        },
        down: function() {
            this.active = this.active == 5 ? 0 : this.active + 1;
            this.refreshActive();
        },
        up: function() {
            this.active = this.active == 0 ? 5 : this.active - 1;
            this.refreshActive();
        },
        refreshActive : function() {
            this.$el.find(".result").removeClass("active");
            this.$el.find(".result:eq(" + this.active + ")").addClass("active");
        },
        fuzzySearch : function() {
            var apps = this.collection.toJSON();
            this.$el.find(".results").html("");
            var count = 0;
            for (var i = 0; i < this.commandList.length; i++) {
                if (this.fuzzyMatch(this.commandList[i].string, this.$el.find("input").val()) && count < 6) {
                    this.$el.find(".results").append("<div class='result' data-location='"+this.commandList[i].location+"'>" + this.commandList[i].string + "</div>");
                    count++;
                }
            }
            this.$el.find(".result:first").addClass("active");
            this.active = 0;
        },
        navigate: function() {
            $(".spotlight").hide();
            if (this.$el.find(".result").length > 0) {
                var location = this.$el.find(".result:eq(" + this.active +")").attr("data-location");
                if (location.indexOf("external::") > -1) {
                    window.open(location.replace("external::", ""));
                } else {
                    window.location.hash = location;
                }
            }
        },

        fuzzyMatch: function(str, pattern){
            pattern = pattern.split("").reduce(function(a,b){ return a+".*"+b; });
            return (new RegExp(pattern, "i")).test(str);
        }
        
    });

    return SpotlightView;
});