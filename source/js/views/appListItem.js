/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/appListItem',
    'models/app'
], function($, _, Backbone, template, AppModel) {
    'use strict';

    var AppListItemView = Backbone.View.extend({
        template: template,
        tagName: "li",
        className: "app",
        initialize: function() {
            this.render();
        },
        render: function() {
            var errorPercent = 0;
            try {
                errorPercent = Math.floor((this.model.activity[15].q / this.model.activity[13].q) * 100);
            } catch(err) {};
            this.$el.html(this.template({
                model: this.model,
                durationString: this.durationString(),
                errorPercent: errorPercent
            }));
        },
        events: {},
        durationString: function() {
            var active_until = (this.model.config.active_until || 0).value || 0;
            if (active_until === 0) {
                return "";
            } else {
                var now = new Date();
                var end = new Date(active_until);

                if (end < now) {
                    return "ended";
                } else {
                    var d = end - now;
                    d = Math.floor(d / (1000 * 60 * 60 * 24)); //in days
                    if (d > 30) return "";
                    if (d > 6) return "ends on " + (end.getMonth() + 1 < 10 ? "0" + (end.getMonth() + 1) : (end.getMonth() + 1)) + "-" + (end.getDate() < 10 ? "0" + (end.getDate()) : (end.getDate()));
                    else {
                        end.setHours(0);
                        end.setMinutes(0);
                        end.setSeconds(0);
                        end.setMilliseconds(0);
                        now.setHours(0);
                        now.setMinutes(0);
                        now.setSeconds(0);
                        now.setMilliseconds(0);
                        var d = end - now;
                        d = Math.floor(d / (1000 * 60 * 60 * 24)); //in days
                        if (d === 0) {
                            return "ends today"
                        }
                        if (d === 1) {
                            return "ends tomorrow"
                        }
                        var days = "Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday".split(",");
                        return "ends " + days[end.getDay()];

                    }
                }
            }
        },
        

    });

    return AppListItemView;
});