/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/chart',
    'currentApp',
], function($, _, Backbone, template, CurrentApp) {
    'use strict';

    var ChartView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "chart",
        colors: {
            1 : "#f0ad4e", //register
            3 : "#428bca", //custom post
            4 : "#428bca", //limits post
            6 : "#428bca", //invite confirm
            7 : "#a28bca", //ugc new entry
            8 : "#428bca", //ugc vote
            9 : "#428bca", //chat question
            10 : "#428bca", //chat answer
            11 : "#428bca", //fileupload
            12 : "#428bca", //sweepstake submit
            13 : "#5cb85c", //visit
            14 : "#5bc0de", //like
            15 : "#d9534f", //error log
            16 : "#428bca" //reminder

        },
        initialize: function(data, id, type, name, options) {
            this.data = data;
            this.id = id;
            var self = this;
            require(['goog!visualization,1,packages:[corechart]'], function() {
                self.render();
            });
            this.type = type || "line";
            this.name = name || "Name Missing";
            this.options = options || {};
        },
        render: function() {
            //LINE
            if (this.type === "line") {
                var data = google.visualization.arrayToDataTable(this.data);
                var sum = _.reduce(this.data, function(memo, obj) {
                    if (isNaN(parseFloat(obj[1]))) return memo;
                    return memo + obj[1];
                }, 0);
                var options = {
                    title: this.data[0][1] + ": " + sum,
                    legend: {
                        position: "none"
                    },
                    animation: {
                        duration: 2000
                    },
                    pointSize: 5,
                    width: this.width,
                    height: this.height,
                    series : [{color : this.colors[this.id]}]
                };
                options = _.extend(options, this.options);
                var chart = new google.visualization.AreaChart(this.$el[0]);
                chart.draw(data, options);
            } else if (this.type === "pie") {
                var data = new google.visualization.DataTable();
                data.addColumn('string', '');
                data.addColumn('number', '');
                data.addRows(this.data);
                var options = {
                        title : this.name + ": " + _.reduce(this.data, function(memo, obj) {
                            return memo + obj[1];
                        }, 0),
                       'width':700,
                       'height':400};
                var chart = new google.visualization.PieChart(this.$el[0]);
                chart.draw(data, options);
            }

        }

    });

    return ChartView;
});