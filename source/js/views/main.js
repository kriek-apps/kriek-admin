/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/main',
    'views/header'
], function($, _, Backbone, template, HeaderView) {
    'use strict';

    var MainView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "main",
        header: {},
        initialize: function() {
            this.render();
            this.header = new HeaderView();
            this.$el.prepend(this.header.el);
        },
        render: function() {
            this.$el.html(this.template());
        },
        events: {

        }
    });

    return MainView;
});