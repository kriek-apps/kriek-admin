/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/insightsBehavior',
    'currentApp',
    'views/userFlow',
    'views/testGroupsModal'
], function($, _, Backbone, template, CurrentApp, UserFlow, TestGroupModalView) {
    'use strict';

    var InsightsAnalyze = Backbone.View.extend({
        tagName: "div",
        className: "behavior",
        initialize: function() {
            this.render();
        },
        render: function() {
            var obj = CurrentApp.getModel().get("config").public.groups;
            var groups = [];
            var letters = "ABCDEFGHIJKLMNOPQRSTUWXYZ".split("");
            for (var i = 0; i < obj.names.length; i++) {
                groups[i] = {
                    name : obj.names[i],
                    desc : obj.descriptions[i],
                    letter: letters[i]
                };
            }
            this.groups = groups;
            this.$el.html(template({
                groups: groups
            }));
            var flow = new UserFlow();
            this.$el.find(".combinedFlow").html(flow.$el);
            for (var i = 0; i < groups.length; i++) {
                var view = new UserFlow({id : i});
                this.$el.find(".flow:eq(" + i + ")").html(view.$el);
            }
        },
        events: {
            "click .edit" : "editGroups"
        },
        editGroups: function() {
            var modal = new TestGroupModalView();
        }
    });

    return InsightsAnalyze;
});