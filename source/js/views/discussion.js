/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/discussion',
    'views/discussionList',
    'views/discussionListItem',
    'currentApp',
    'hbs!templates/discussionSingleQuestion',
    'models/discussion'
], function($, _, Backbone, template, DiscussionListView, DiscussionListItemView, CurrentApp, singleQuestionTemplate, DiscussionModel) {
    'use strict';

    var DiscussionView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "discussion",
        initialize: function(data) {
            this.specificQuestion = false;
            if (data.questionID !== undefined) {
                this.questionID = data.questionID;
                this.specificQuestion = true;
            }
            this.render();
            var self = this;
            $.ajax({
                method: "GET",
                url: "/api/" + CurrentApp.getID() + "/chat/topics",
                success: function(r) {
                    self.topics = r;
                    self.insertTopics();
                }
            });
        },
        render: function() {
            var admins = this.model.toJSON().config.public.modules.discussion.admins;
            if (this.specificQuestion) {
                this.$el.html(singleQuestionTemplate({
                    admins: admins,
                    model: this.model.toJSON()
                }));
                var model = new DiscussionModel({ id : this.questionID });
                model.on("sync", function() {
                    this.singleQuestion = new DiscussionListItemView({model : model});
                    this.$el.find(".results").html(this.singleQuestion.$el);
                }, this);
                model.fetch({reset : true});
            } else {
                this.$el.html(this.template({
                    admins: admins,
                    model: this.model.toJSON()
                }));
                this.list = new DiscussionListView();
                this.$el.append(this.list.$el);
                var self = this;
                this.list.collection.on("reset", function() {
                    self.$el.find(".filterGroup button:last").click();
                });
            }
        },
        events: {
            "click .filterGroup button" : "changeFilter",
            "keyup .search-field" : "search",
            "change .topicSelector" : "changeTopic",
            "click .backBtn" : "goBack"
        },
        goBack : function() {
            var arr = window.location.hash.split("/");
            window.location.hash = arr.splice(0,arr.length-1).join("/");
        },
        insertTopics: function() {
            for (var i = 0; i < this.topics.length; i++) {
                $(".topicSelector").append("<option data-id='"+this.topics[i].id+"'>"+this.topics[i].data.name+"</option>")
            }
        },
        changeTopic: function() {
            var topicToShow = $(".topicSelector option:selected").attr("data-id");
            this.list.collection.setTopic(topicToShow);
            this.list.collection.fetch({reset: true});
            $(".filterGroup button").removeClass("active");
            $(".filterGroup button:first").addClass("active");
        },
        search: function() {
            var term = $(".search-field").val();
            if (term == "") {
                this.$el.find(".discussion-entry").show();
                return;
            }
            this.$el.find(".discussion-entry").hide();
            this.$el.find(".discussion-entry:contains(" + term + ")").show();
        },
        changeFilter : function(event) {
            var el = $(event.currentTarget);
            $(".filterGroup button").removeClass("active");
            var filter = el.addClass("active").attr("data-filter");
            this.list.currentFilter = filter;
            this.list.currentPage = 0;
            this.list.render();
        }
        
    });

    return DiscussionView;
});