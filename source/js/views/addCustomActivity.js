define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/addCustomActivity',
], function($, _, Backbone, template) {
    'use strict';

    var AddCustomActivity = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "addCustomActivityModal",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
            }));
            $("body").append(this.$el);
            var self = this;
            this.$el.find("#modal").modal("show").on('hidden.bs.modal', function() {
                self.$el.remove();
            });
            this.$el.show();
        },
        events: {
            "click .save" : "save",
            "click .exit" : "exit"
        },
        save: function() {
           var data = {};
           var inputs = this.$el.find("input");
           for (var i = 0; i < inputs.length; i++) {
            data[$(inputs[i]).attr("data-key")] = $(inputs[i]).val();
           }
           data.type = data.name_short;
           data.ref = "";
           data.collection = 0;
           var modal = this.$el;
           $.ajax({
                method: "POST",
                url: globalConfig.api + "activities",
                data: data,
                complete: function(r) {
                    var id = r.responseJSON.last_id;
                    modal.find(".modal-body").html("Success. The ID of your custom activity is " + id + ".");
                    modal.find(".save").removeClass("save").addClass("exit").text("Done");
                }
           })
        },
        exit: function() {
            this.$el.find("#modal").modal("hide");
        }
    });

    return AddCustomActivity;
});