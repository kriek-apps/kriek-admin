    /*global define*/

    define([
        'jquery',
        'underscore',
        'backbone',
        'hbs!templates/reports',
        'collections/report',
        'views/reportList',
        'collections/reportField',
        'views/reportFieldList',
        'admin',
        'facebooksdk',
        'models/app'
    ], function($, _, Backbone, template, ReportCollection, ReportListView, ReportFieldCollection, ReportFieldListView, Admin, FB, AppModel) {
        'use strict';

        var ReportsView = Backbone.View.extend({
            template: template,
            tagName: "div",
            className: "reports",
            table: {},
            initialize: function(data) {
                this.appId = data.appId;
                this.reportId = data.reportId;
                this.collection = new ReportCollection(this.appId);
                this.collection.on("reset", function() {
                    this.render();
                }, this);
                this.collection.fetch({
                    reset: true
                });
            },
            render: function() {
                this.$el.html(this.template({
                    report: this.reportId
                }));
                var reportListView = new ReportListView({
                    collection: this.collection
                });
                this.$el.find(".controls div.controlForm").prepend(reportListView.$el);
                reportListView.$el.find("option[data-id=" + this.reportId + "]").prop('selected', true);
                if (typeof this.reportId !== "undefined") {
                    this.renderReport(this.reportId);
                } else {
                    var firstReportId = 0;
                    firstReportId = this.collection.first().get('id');
                    window.location.hash += "/" + firstReportId;
                }
            },
            events: {
                "keyup .search-field": "search",
                "click button.export": "export",
                "click button.update": "update"
            },
            /*
                Update the missing email fields
            */
            update: function(event) {
                event.stopImmediatePropagation();
                if (this.table.collection.toJSON().length > 0) {
                    var missing = this.table.collection.filter(function(o) {
                        return o.get("email") === null || o.get("email") === "";
                    });
                    if (missing.length === 0) return;
                    var self = this;
                    //Local object to handle the updating
                    var updater = {
                        needed: missing.length,
                        finished: 0,
                        i: 0,
                        saveEmail: function(response) {
                            var url = globalConfig.api + self.appId + "/users/" + response.id + "/email";
                            $.ajax({
                                method: "PUT",
                                data: {
                                    email: response.email
                                },
                                url: url,
                                dataType: "json",
                                success: function(r) {
                                }
                            });
                        },
                        //Called every time an email is fetched
                        oneUpdated: function(response) {
                            //Update the model, thus updating the view
                            var model = self.table.collection.find(function(o) {
                                return o.get("user_id") == response.id;
                            }).set("email", response.email);
                            //Then update the database. We can't use the model here, sadly
                            if (response.email !== undefined) this.saveEmail(response);
                            //Then check if the update is complated
                            this.finished++;
                            if (this.finished == this.needed) {
                                this.complete();
                            } else {
                                this.fetchOne();
                            }
                        },
                        //Called when the whole updating process is finished
                        complete: function() {
                            self.$el.find(".update").removeAttr("disabled").html("Fetch Email");
                        },
                        //Start the fetching process
                        fetchOne: function() {
                            var id = missing[this.i].toJSON()["user_id"];//Get the user's id
                            this.i++;
                            FB.api("/" + id + "?fields=email", "GET", {
                                access_token: self.appToken
                            }, function(response) {
                                updater.oneUpdated({
                                    id: id,
                                    email: response.email
                                });
                            });
                        },
                        startFetching: function() {
                            self.$el.find(".update").removeAttr("disabled", "disabled").html("Fetching...");
                            this.fetchOne();
                        }
                    };
                    //Get the app access token
                    var app = new AppModel({
                        id: this.appId
                    });
                    app.on("reset change", function() {
                        this.appToken = app.get("config").private.app_token.value;
                        updater.startFetching();
                    }, this);
                    app.fetch({
                        reset: true
                    });

                }
            },
            renderReport: function(id) {
                this.$el.find(".results").html("");
                var report = this.collection.find(function(model) {
                    return model.get("id") == id;
                });
                var userIdField = report.get("data").user_id;
                this.table = new ReportFieldListView({
                    appId: this.appId,
                    reportId: id,
                    userIdField: userIdField
                });

                this.$el.find(".results").html(this.table.$el);
            },
            search: function() {
                var string = this.$el.find(".search-field").val();
                if (string === "") {
                    this.$el.find("button.export").removeAttr("disabled");
                } else {
                    this.$el.find("button.export").attr("disabled", "disabled");
                }
                this.table.pagination.currentPage = 0;
                this.table.filter(string);

            },
            export: function() {
                var url = window.globalConfig.api + window.location.hash.substr(1, window.location.hash.length - 1).replace('reports', 'queries') + "/export?admin_token=" + Admin.getToken();
                var win = window.open(url, '_blank');
                win.focus();
            }
        });

        return ReportsView;
    });