/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/insightsAnalyze',
    'currentApp',
    'datepicker'
], function($, _, Backbone, template, CurrentApp) {
    'use strict';

    var InsightsAnalyze = Backbone.View.extend({
        tagName: "div",
        className: "analyze",
        initialize: function() {
            this.startDate = CurrentApp.getModel().get("config").public.active_from.value ||  "2014-01-01";
            this.endDate = CurrentApp.getModel().get("config").public.active_until.value ||  "2020-01-01";
            this.filters = CurrentApp.getModel().get("config").private.dashboardStats.value.split(",") || [1,13,14];
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/insights",
                success: function(r) {
                    self.data = r;
                    self.formatData();
                    self.render();
                }
            });
        },
        render: function() {
            //Get available switches
            this.$el.html(template({
                activities : this.data.activities,
                startDate : this.startDate,
                endDate : this.endDate
            }));
            $(".switches li:first").hide();
            var self = this;
            this.$el.find("input[type=datetime]").appendDtpicker({
                "firstDayOfWeek": 1,
                "dateFormat": "YYYY-MM-DD",
                "closeOnSelected": true,
                "todayButton": false,
                "inline" : false,
                "calendarMouseScroll": false,
                "dateOnly" : true
            });
            require(['goog!visualization,1,packages:[corechart]'], function() {
                self.renderChart();
                for (var i = 0; i < self.filters.length; i++) {
                     $(".switches a[value="+self.filters[i]+"]").addClass("active");
                }
            });
            this.renderSummaries();
        },
        formatData: function() {
            this.filteredData = [];
            var array = ["Date"];
            for (var i = 0; i < this.filters.length; i++) {
                var activity = _.find(this.data.activities, function(o) {
                    return o.id == this.filters[i];
                }, this);
                activity.q = 0;
                array.push(activity.name);
            }
            this.filteredData[0] = array;
            for (var i in this.data) {
                if (i !== "activities") {
                    if ((new Date(i)) >= new Date(this.startDate) && (new Date(i)) <= new Date(this.endDate)) {
                        var array = [];
                        array[0] = i.substr(5, 9);
                        for (var j = 0; j < this.filters.length; j++) {
                            array.push(this.data[i][this.filters[j]] || 0);
                            //Create a summary of the values for the corresponding activity
                            var activity = _.find(this.data.activities, function(o) {
                                return o.id == this.filters[j];
                            }, this);
                            activity.q = activity.q + Number(this.data[i][this.filters[j]] || 0);
                        }
                        this.filteredData.push(array);
                    }
                }
            }
        },
        renderChart: function() {
            var data = google.visualization.arrayToDataTable(this.filteredData);
            var options = {
                legend: {},
                pointSize: 5,
                series: [],
                areaOpacity : 0
            };
            var chart = new google.visualization.AreaChart(this.$el.find(".chart")[0]);
            chart.draw(data, options);
        },



        events: {
            "click .dropdown a" : "changeFilter",
            "change input" : "changeDate"
        },

        changeDate: function() {
            $(".datepicker").hide();
            this.filters = [];
            var checkboxes = this.$el.find(".switches a");
            for (var i = 0; i < checkboxes.length; i++) {
                if ($(checkboxes[i]).hasClass("active")) {
                    this.filters.push($(checkboxes[i]).attr("value"));
                }
            }
            this.startDate = this.$el.find(".start").val()
            this.endDate = this.$el.find(".end").val()
            this.formatData();
            this.render();
        },
        renderSummaries: function() {
            var activities = this.data.activities;
            for (var i = 0; i < this.filters.length; i++) {
                var activity = _.find(activities, function(o) {
                    return o.id == this.filters[i];
                }, this);
                this.$el.find(".summary").append("<div style='display:inline-block; vertical-align:top; margin-right:15px; text-align:right; width:60px; min-height:60px;'><strong style='font-size:18px;'>" + activity.q + "</strong><br>" + activity.name + "</div>");
            }
        },

        changeFilter: function(event) {
            var el = $(event.currentTarget);
            el.toggleClass("active");
            this.filters = [];
            var checkboxes = this.$el.find(".switches a");
            for (var i = 0; i < checkboxes.length; i++) {
                if ($(checkboxes[i]).hasClass("active")) {
                    this.filters.push($(checkboxes[i]).attr("value"));
                }
            }
            this.startDate = this.$el.find(".start").val()
            this.endDate = this.$el.find(".end").val()
            this.formatData();
            this.render();
        }

    });

    return InsightsAnalyze;
});