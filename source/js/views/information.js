/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/information',
    'admin',
    'views/chart',
    'currentApp',
    'views/ticker',
    'views/appListItem',
    'views/todos',
    'views/userFlow'
], function($, _, Backbone, template, Admin, ChartView, CurrentApp, TickerView, StatsView, TodosView, UserFlowView) {
    'use strict';

    var InformationView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "information",
        initialize: function() {
            this.render();
            this.getActiveUsers();
            var self = this;
            window.timer2 = setInterval(function() {
                self.getActiveUsers();
            }, 10000);
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON(),
                admin: Admin.getData()
            }));
            this.renderTicker();
            this.renderChart();
            this.renderStats();
            this.renderTasks();
            this.renderUserFlow();
        },
        renderTicker: function() {
            var view = new TickerView();
            this.$el.find(".ticker").html(view.$el);
        },
        renderUserFlow: function() {
            var view = new UserFlowView();
            this.$el.find(".userFlow").html(view.$el);
        },
        renderChart: function() {
            var self = this;
            var dataToShow = [13,14,1];
            try {
                dataToShow = CurrentApp.getModel().get("config").private.dashboardStats.value.split(",");
            } catch(err) {}
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/insights",
                success: function(r) {
                    if (Object.keys(r).length < 2) {
                        self.$el.find(".insights").html("<div>No data yet.</div>");
                        return;
                    }
                    var data = [];
                    data[0] = ["Date"];
                    for (var i = 0; i < dataToShow.length; i++) {
                        data[0].push((_.find(r.activities, function(o) {
                            return o.id == dataToShow[i]
                        }) || 0).name);
                    }
                    for (var i in r) {
                        if (i !== "activities") {
                            var array = [];
                            array[0] = i.substr(5, 6);
                            for (var j = 0; j < dataToShow.length; j++) {
                                array.push(r[i][dataToShow[j]] || 0);
                            }
                            data.push( array );
                        }
                    }
                    if (data.length === 2) {
                        data.splice(1,0,[" ", 0, 0, 0]);
                    }
                    var view = new ChartView(data, 13, "line", "Visits", {
                        width : 800,
                        height : 300,
                        legend : {
                            position : "right"
                        },
                        title : "",
                        series: [{color:"orange"}, {color:"blue"}, {color:"green"}, {color:"red"}, {color:"purple"}, {color:"pink"}],
                        areaOpacity : 0
                    });
                    self.$el.find(".insights").html(view.$el);
                }
            });
        },
        renderStats: function() {
            var model = CurrentApp.getModel().toJSON();
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/insights",
                success: function(r) {
                    if (Object.keys(r).length < 2) {
                        self.$el.find(".stats").html("<div>No data yet.</div>");
                        return;
                    }
                    model.activity = {};
                    for (var i in r) {
                        if (i !== "activities") {
                            for (var j in r[i]) {
                                if (model.activity[ j ] === undefined) {
                                    model.activity[ j ] = {
                                        name : _.find(r.activities, function(obj) {
                                            return obj.id == j;
                                        }).name,
                                        q : 0
                                    };
                                }
                                model.activity[ j ].q += r[i][j];
                            }
                        }
                    }
                    var view = new StatsView({
                       model : model
                    });
                    self.$el.find(".stats").html(view.$el.find(".details"));
                    try {
                        self.$el.find(".stats .title:contains('Custom Posts')").html(CurrentApp.getModel().get("config").public.modules.custom.name_chart);
                    } catch(err) {}
                    try {
                        self.$el.find(".stats .title:contains('Limited Posts')").html(CurrentApp.getModel().get("config").public.modules.limits.name_chart);
                    } catch(err) {}
                }
            });
        },
        renderTasks: function() {
            var view = new TodosView();
            this.$el.find(".tasks").html(view.$el.find(".tasks"));
        },
        getActiveUsers: function() {
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + "liveusers",
                success: function(r) {
                    for (var i = 0; i < r.length; i++) {
                        if (r[i].app_id === CurrentApp.getID()) {
                            self.$el.find(".liveUsers .number").html(r[i].q);
                        }
                    }
                }
            });
        },
        events: {

        }
    });

    return InformationView;
});