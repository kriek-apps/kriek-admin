/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/entriesList',
  'views/entriesListItem',
  'collections/entries'
], function($, _, Backbone, template, EntriesListItemView, EntriesCollection) {
  'use strict';

  var EntriesListView = Backbone.View.extend({
    template: template,
    tagName: "ul",
    className: "entries row loading",
    filtering : "all",
    category : "all",
    searchTerm : "",
    initialize: function(data) {
      this.adminTemplate = data.adminTemplate;
      this.collection = new EntriesCollection(data.id);
      this.collection.on('reset', function() {
        this.render();
      }, this);
      this.collection.fetch({reset : true});
    },
    render: function() {
      this.$el.html("").removeClass("loading");

      //Filter
      var filteredCollection = this.collection.filter(function(item) {
        var r = true;
        var disabled = item.get("disabled");
        if (this.filtering == "all") r = true;
        if (this.filtering == "disabled") r = disabled == 1;
        if (this.filtering == "enabled") r = disabled === 0;

        var category = item.get("category");
        if (this.category !== "all" && r) {
          r = Number(this.category) === category;
        }

        return r;
      }, this);
      filteredCollection = new Backbone.Collection(filteredCollection);

      //Search
      filteredCollection = filteredCollection.filter(function(item) {
        if (this.searchTerm === "") return true;
        return (item.get("user_name").toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
      }, this);
      filteredCollection = new Backbone.Collection(filteredCollection);

      //Render
      filteredCollection.each(function(item) {
        var entriesListItem = new EntriesListItemView({
          model: item,
          adminTemplate : this.adminTemplate
        });
        this.$el.append(entriesListItem.$el);
      }, this);
    },
    events: {
    },
    filter : function(filter) {
      this.filtering = filter;
      this.render();
    },
    search : function(string) {
      this.searchTerm = string;
      this.render();
    },
    viewCategory : function(category) {
      this.category = category;
      this.render();
    }
  });

  return EntriesListView;
});