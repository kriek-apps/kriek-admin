/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/sidebar',
    'admin',
    'currentApp',
    'views/warning'
], function($, _, Backbone, template, Admin, CurrentApp, WarningView) {
    'use strict';

    var SidebarView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "sidebar",
        initialize: function() {
            this.render();
            this.model.on("change", function() {
                this.render();
            }, this);
        },
        render: function() {
            var admin = Admin.getData();
            admin.admin_token = Admin.getToken();
            var briefcase = this.model.get("briefcase");
            _.each(briefcase, function(o) {
                o.appID = CurrentApp.getID();
            });

            var briefcaseStart = briefcase.length > 0 ? "0" : "settings";

            this.$el.html(this.template({
                model: this.model.toJSON(),
                admin: admin,
                briefcase : briefcase,
                briefcaseStart : briefcaseStart
            }));

            var warnings = new WarningView().getErrors();
            if (warnings.length > 0)
                this.$el.find(".todos").append("<span class='badge pull-right'>" + warnings.length + "</span>");

        },
        events: {

        },
        openSettings: function(option) {
            this.$el.find(".settings-child").css("display", "block");
            this.$el.find(".settings-child[data-option=" + option + "]").addClass("activeSubItem");
        },
        openDevTools: function(option) {
            this.$el.find(".devTools-child").css("display", "block");
            this.$el.find(".devTools-child[data-option=" + option + "]").addClass("activeSubItem");
        },
        openDiscussion: function(option) {
            this.$el.find(".discussion-child").css("display", "block");
            this.$el.find(".discussion-child[data-option=" + option + "]").addClass("activeSubItem");
        },
        openBriefcase: function(option) {
            this.$el.find(".briefcase-child").css("display", "block");
            this.$el.find(".briefcase-child:eq("+ option + ")").addClass("activeSubItem");
        },
        openInsights: function(option) {
            this.$el.find(".insights-child").css("display", "block");
            this.$el.find(".insights-child[data-option=" + option + "]").addClass("activeSubItem");
        },
        showBriefcaseFolders: function() {
            this.$el.find(".briefcase-child").css("display", "block");
            this.$el.find(".briefcase-child[data-option=settings]").addClass("activeSubItem");
        }
    });

    return SidebarView;
});