/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/addLanguageField',
    'bootstrap'
], function ($, _, Backbone, template) {
    'use strict';

    var AddLanguageFieldView = Backbone.View.extend({
        template: template,
        tagName : "div",
        className : "addLanguageField",
        initialize : function() {
           this.render();
        },
        render : function() {
           this.$el.html(this.template());
           $("body").append(this.$el);
           var self = this;
           this.$el.find("#languageModal").modal("show").show().on('hidden.bs.modal', function() {
                self.$el.remove();
            });
        },
        events : {
           "click .done" : "done"
        },
        done : function() {
            if (this.$el.find("input").val() !== "") {
                this.trigger("addField", {text : this.$el.find("input").val()});
            }
            this.$el.find("#languageModal").modal("hide");
        }
    });

    return AddLanguageFieldView;
});