/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/appList',
    'hbs!templates/appListItem-create',
    'views/appListItem',
    'collections/app',
    'admin',
    'views/newAppWizard',
    'models/app'
], function($, _, Backbone, template, newAppTemplate, AppListItemView, AppCollection, Admin, NewAppWizardView, AppModel) {
    'use strict';

    var AppListView = Backbone.View.extend({
        template: template,
        tagName: "ul",
        className: "app row",
        collection: {},
        initialize: function() {
            this.collection = new AppCollection();
            this.collection.on('reset change', function() {
                this.render();
            }, this);
            this.collection.fetch({
                reset: true
            });
            var self = this;
            window.timer = setInterval(function() {
                self.getActiveUsers();
            }, 30000);
        },
        render: function() {
            this.$el.html("");
            this.$el.html(template());
            if (Admin.isDeveloper()) this.$el.find(".newApp").append(newAppTemplate());

            var apps = this.collection.toJSON();
            var model = {
                production: [],
                development: [],
                archived: []
            };


            //Catalog items
            for (var i = 0; i < apps.length; i++) {
                if (((apps[i].config || 0).production || 0).value && apps[i].config.archive.value === false) {
                    model.production.push(apps[i]);
                } else if ((apps[i].config || 0).archive !== null && apps[i].config.archive.value === false) {
                    model.development.push(apps[i]);
                } else {
                    model.archived.push(apps[i]);
                }
            }

            //Render items
            if (model.production.length === 0) {
                this.$el.find(".production").html("There are no live apps at the moment.");
            } else {
                for (var i = 0; i < model.production.length; i++) {
                    var appListItem = new AppListItemView({
                        model: model.production[i]
                    });
                    this.$el.find(".production").append(appListItem.$el);
                }
            }

            if (model.development.length === 0) {
                this.$el.find(".development").html("There are no apps in development right now.");
            } else {
                for (var i = 0; i < model.development.length; i++) {
                    var appListItem = new AppListItemView({
                        model: model.development[i]
                    });
                    this.$el.find(".development").append(appListItem.$el);
                }
            }

            for (var i = 0; i < model.archived.length; i++) {
                var appListItem = new AppListItemView({
                    model: model.archived[i]
                });
                this.$el.find(".archived").append(appListItem.$el);
            }

            this.$el.find("span.number").html(model.archived.length);
            this.getActiveUsers();

        },
        events: {
            "click .createNewApp": "createNewApp",
            "click .archiveTrigger": "showArchived",
            "click .update": "updateApps"
        },
        createNewApp: function() {
            var modal = new NewAppWizardView();
            modal.on("appAdded", function() {
                this.collection.fetch({
                    reset: true
                });
            }, this);
        },
        updateApps: function(event) {
            var el = $(event.currentTarget);
            el.attr("disabled", "disabled").text("Updating...");
            setTimeout(function() {
                el.fadeOut();
            }, 1000)
            //Get the config
            var self = this;
            $.ajax({
                url: "app_settings.json",
                method: "get",
                dataType: "json",
                contentType: "application/json",
                success: function(r) {
                    self.defaultSettings = r;
                    self.collection.each(function(model) {
                        //Get the actual model
                        var app = new AppModel({id: model.toJSON().id});
                        app.on("change", function() {
                            var changed = false;
                            var config = app.get("config");
                            //do public
                            for (var key in self.defaultSettings.public) {
                                if (config.public[key] === undefined) {
                                    config.public[key] = self.defaultSettings.public[key];
                                    changed = true;
                                }
                            }
                            //do private
                            for (var key in self.defaultSettings.private) {
                                if (config.private[key] === undefined) {
                                    config.private[key] = self.defaultSettings.private[key];
                                    changed = true;
                                }
                            }
                            //do modules
                            for (var key in self.defaultSettings.public.modules) {
                                if (config.public.modules[key] === undefined) {
                                    config.public.modules[key] = self.defaultSettings.public.modules[key];
                                    changed = true;
                                }
                            }
                            if (changed) {
                                console.log(config);
                                app.set("config", config);
                                app.save();
                            }
                        });
                        app.fetch();

                    });
                }
            });

        },
        showArchived: function() {
            this.$el.find(".archiveTrigger").hide();
            this.$el.find(".archived").show();
        },
        /**
         * Get the number of active users in the last 30 seconds
         */
        getActiveUsers: function() {
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + "liveusers",
                success: function(r) {
                    self.$el.find(".activeUsers").html("0");
                    for (var i = 0; i < r.length; i++) {
                        self.$el.find(".activeUsers[data-id='" + r[i].app_id + "']").html(r[i].q);
                    }
                }
            });
        }
    });

    return AppListView;
});