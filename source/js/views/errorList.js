/*global define*/

define([
  'jquery',
  'underscore',
  'backbone',
  'hbs!templates/errorList',
  'views/errorListItem',
  'collections/error'
], function($, _, Backbone, template, ErrorListItemView, ErrorCollection) {
  'use strict';

  var ErrorListView = Backbone.View.extend({
    template: template,
    tagName: "table",
    className: "error table-striped",
    initialize: function() {
      this.collection = new ErrorCollection();
      this.collection.on("reset change remove add", function() {
        this.render();
      }, this);
      this.collection.fetch({reset: true});
      var self = this;
      // Auto refresh
      /*var a = setInterval(function() {
        self.collection.fetch();
      }, 8000);*/
      Backbone.history.on("all", function() {
        clearInterval(a);
      });
    },
    render: function() {
      this.$el.html(template());
      this.collection.each(function(item) {
        var errorListItem = new ErrorListItemView({
          model: item
        });
        this.$el.append(errorListItem.$el);
      }, this);
    },
    events: {

    },
    filter: function(filterBy) {
      if (filterBy == "all") {
        this.$el.find("tr").show();
      } else {
        this.$el.find("tr:not(:first-child)").hide();
        this.$el.find("tr[data-origin="+filterBy+"]").show();
      }
    },
    search: function(term) {
      var ids = [];
      this.collection.each(function(model) {
        model = model.toJSON();
        var include = false;
        for (var key in model) {
          if (JSON.stringify(model[key]).toLowerCase().indexOf(term.toLowerCase()) > -1) {
            include = true;
          }
        }
        if (include) {
          ids.push(model.id);
        }
      });
      this.$el.find("tr:not(:first-child)").each(function() {
        $(this).hide();
        if (_.indexOf(ids, Number($(this).attr("data-id"))) > -1) {
          $(this).show();
        }
      });
      if (term === "") {
        this.$el.find("tr").show();
      }
    } 
  });

  return ErrorListView;
});