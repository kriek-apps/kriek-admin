/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/ugc',
    'views/entriesList',
    'currentApp',
    'views/ugcEntryWizard'
], function($, _, Backbone, template, EntriesListView, CurrentApp, UgcEntryWizard) {
    'use strict';

    var UgcView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "ugc",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(this.template({
                model: this.model.toJSON()
            }));
            this.listView = new EntriesListView({
                id: this.model.get('id'),
                adminTemplate : this.model.get("config").public.modules.ugc.adminTemplate
            });
            this.listView.collection.on('reset', function() {
                this.$el.find(".results").html(this.listView.$el);
                this.$el.find(".filterBtn:eq(1)").click();
                var select = this.$el.find(".category-selector");
                var categories = CurrentApp.getModel().get("config").public.modules.ugc.rounds[0].categories;
                select.html("<option data-id='all'>All</option>");
                for (var i = 0; i < categories.length; i++) {
                    select.append("<option data-id='" + i + "'>" + categories[i] + "</option>");
                }

            }, this);
        },
        events: {
            "click .filterBtn" : "filter",
            "keyup .search-field" : "search",
            "change .category-selector" : "filterByCategory",
            "click .uploadEntry": "upload"
        },
        filterByCategory : function(event) {
            var selectedCategory = this.$el.find(".category-selector").find(":selected").attr("data-id");
            this.listView.viewCategory(selectedCategory);
        },
        filter : function(event) {
            var el = $(event.currentTarget);
            this.$el.find(".filterBtn").removeClass("active");
            el.addClass("active");
            var filter = el.attr("data-filter");
            this.listView.filter(filter);
        },
        search : function(event) {
            var el = $(event.currentTarget);
            this.listView.search(el.val());
        },
        upload: function() {
            var wizard = new UgcEntryWizard({
                model: this.model.get("config").public.modules.ugc
            });
            var self = this;
            wizard.on("newEntry", function() {
                self.render();
            });
        }
    });

    return UgcView;
});