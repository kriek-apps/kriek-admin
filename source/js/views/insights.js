/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/insights',
    'currentApp',
    'views/chart',
    'admin',
    'views/addCustomActivity'
], function($, _, Backbone, template, CurrentApp, ChartView, Admin, CustomActivityModal) {
    'use strict';

    var Insights = Backbone.View.extend({
        tagName: "div",
        className: "insights",
        initialize: function() {
            this.render();
        },
        render: function() {
            this.$el.html(template());
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/insights",
                success: function(r) {
                    self.renderCharts(r);
                }
            });
        },
        /**
         * Renders all charts based on data from the server
         * @param  {Object} data
         */
        renderCharts: function(data) {
            var meta = data.activities;
            //Each chart
            for (var i = 1; i < meta.length; i++) {
                var id = meta[i].id;
                if (id === 15 && !Admin.isDeveloper()) continue; //Show the errors only to developers
                if (id === 5 || id === 2) continue; //Don't show invites sent statistics because it's misleading
                var name = meta[i].name;
                if (id === 3) name = CurrentApp.getModel().get("config").public.modules.custom.name_chart || name;
                if (id === 4) name = CurrentApp.getModel().get("config").public.modules.limits.name_chart || name;
                var parsedData = []
                parsedData[0] = ["Date", name];
                var k = 1;
                for (var j in data) {
                    if (j === "activities") continue;
                    //Format date
                    var date = j.substr(5, 6);
                    parsedData[k] = [date, data[j][id] || 0];
                    k++;
                }
                //If we only have one day's woth of data, add a previous day
                if (parsedData.length == 2) {
                    parsedData.splice(1, 0, ["", 0]);
                }
                //Render the chart
                var chart = new ChartView(parsedData, id);
                this.$el.find(".charts").append(chart.$el);
            }

            //Custom charts
            //Origin of visits
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/activity/13",
                success: function(r) {
                    var array = [];
                    for (var i = 0; i < r.length; i++) {
                        array.push([ r[i].ref, r[i].q ])
                    }
                    var chart = new ChartView(array, 0, "pie", "Visits");
                    self.$el.find(".charts").append(chart.$el);
                }
            });
            //Origin of Likes
            var self = this;
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/activity/14",
                success: function(r) {
                    var array = [];
                    for (var i = 0; i < r.length; i++) {
                        array.push([ r[i].ref, r[i].q ])
                    }
                    var chart = new ChartView(array, 0, "pie", "Likes");
                    self.$el.find(".charts").append(chart.$el);
                }
            });
            //Invites sent and confirmed
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/insights/invites",
                success: function(r) {
                    if (r.length == 0) return;
                    var allInvites = _.reduce(r, function(memo, obj) {
                        return memo + obj.invites;
                    }, 0);
                    var confirms = _.reduce(r, function(memo, obj) {
                        return memo + obj.confirmed;
                    }, 0);
                    var notConfirmed = allInvites - confirms;
                    var array = [
                        ["Not Confirmed", notConfirmed],
                        ["Confirmed", confirms]
                    ];
                    var chart = new ChartView(array, 0, "pie", "Invites");
                    self.$el.find(".charts").append(chart.$el);
                }
            });
            //QUIZ
            $.ajax({
                method: "GET",
                url: globalConfig.api + CurrentApp.getID() + "/insights/quiz",
                success: function(r) {
                    if (r.length == 0) return;
                    var array = [];
                    array[0] = ["Date", "Answers", "Correct"];
                    for (var i = 0; i < r.length; i++) {
                        array[i+1] = [ r[i].date.substr(5, 6), r[i].answers, r[i].correct  ];
                    }
                    var chart = new ChartView(array, 0);
                    self.$el.find(".charts").append(chart.$el);
                }
            });
        },
        events: {
            "click .add" : "addActivity"
        },
        addActivity: function() {
            var modal = new CustomActivityModal();
        }

    });

    return Insights;
});