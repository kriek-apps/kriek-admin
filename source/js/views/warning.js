/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/warning',
    'currentApp'
], function($, _, Backbone, template, CurrentApp) {
    'use strict';

    var WarningView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "warning",
        initialize: function() {
            this.render();
            CurrentApp.getModel().on("sync", function() {
                this.render();
            }, this);
        },
        getErrors: function() {
            var warnings = [];
            //Collect all warnings

            //Terms and conditions
            var app = CurrentApp.getModel().toJSON();
            if (app.config.public.terms_and_conditions_url.value === "") {
                warnings.push({
                    message: "The Terms and Conditions is not uploaded yet.",
                    type: "danger",
                    link: "/" + CurrentApp.getID() + "/settings/general"
                });
            }
            //Production mode
            try {
                if (app.config.public.production.value === false) {
                    warnings.push({
                        message: "Production mode is turned off.",
                        type: "danger",
                        link: "/" + CurrentApp.getID() + "/settings/general"
                    });
                }
            } catch(err) {}
            //Facebook url
            if (app.config.public.app_fb_page_url.value.indexOf("Kriek") > -1) {
                warnings.push({
                    message: "The Facebook url still points to the development page.",
                    type: "danger",
                    link: "/" + CurrentApp.getID() + "/settings/general"
                });
            }
            //App url
            if (app.config.public.app_url.value.indexOf("dev.kriek") > -1) {
                warnings.push({
                    message: "The App url still points to the development server.",
                    type: "danger",
                    link: "/" + CurrentApp.getID() + "/settings/general"
                });
            }
            //Mobile reminder
            try {
                if (app.config.public.mobile_header_img.value === "") {
                    warnings.push({
                        message: "The mobile reminder image is not uploaded yet.",
                        type: "danger",
                        link: "/" + CurrentApp.getID() + "/settings/general"
                    });
                }
            } catch(err) {}
            //Timing
            var modules = app.config.public.modules;
            var missingTiming = false;
            for (var key in modules) {
                if (modules[key].active === true && key !== "discussion") {
                    if (key == "ugc") {
                        if (modules[key].voting_from === "" || modules[key].voting_until === "" || modules[key].upload_from === "" || modules[key].upload_until === "") missingTiming = true;
                    }
                    else if (modules[key].active_from === "" || modules[key].active_until === "") {
                        missingTiming = true;
                    }
                }
            }
            if (missingTiming) warnings.push({
                message: "Some module timings are missing.",
                type: "danger",
                link: "/" + CurrentApp.getID() + "/settings/modules"
            });
            //App timing
            try {
                if (app.config.public.active_from.value === "" || app.config.public.active_until.value === "") {
                    warnings.push({
                        message: "The app timing is missing.",
                        type: "danger",
                        link: "/" + CurrentApp.getID() + "/settings/general"
                    });
                }
            } catch(err) {}

            //Deployment warnings
            var deploymentWarnings = app.config.private.warnings;
            try {
                for (var i = 0; i < deploymentWarnings.length; i++) {
                    if (deploymentWarnings[i].completed === false) {
                        warnings.push({
                            message : deploymentWarnings[i].text || "Text Missing",
                            type : deploymentWarnings[i].type || "warning",
                            date : deploymentWarnings[i].date || "",
                            link : deploymentWarnings[i].link || "",
                            index : i
                        });
                    }
                }
            } catch(err) {}


            return warnings;
        },
        render: function() {
            var warnings = this.getErrors();
            this.$el.html(template({
                warnings: warnings
            }));
        },
        events: {
            "click .close" : "dismiss"
        },
        dismiss: function(event) {
            var el = $(event.currentTarget);
            var id = el.attr("data-id");
            var config = CurrentApp.getModel().get("config");
            config.private.warnings[id].completed = true;
            CurrentApp.getModel().set("config", config);
            CurrentApp.getModel().save();
        }
    });

    return WarningView;
});