/*global define*/

define([
	'jquery',
	'underscore',
	'backbone',
	'hbs!templates/ugcEntryWizard',
	'hbs!templates/contentManagerItemForm',
	'currentApp',
	'admin'
], function($, _, Backbone, template, itemFormTemplate, CurrentApp, Admin) {
	'use strict';

	var UgcEntryWizard = Backbone.View.extend({
		template: template,
		tagName: "div",
		className: "ugcEntryWizard",
		initialize: function() {
			this.render();
		},
		render: function() {
			this.$el.html(this.template());
			$("body").append(this.$el);
			var self = this;
			this.$el.find("#ugcEntryModal").modal("show").on('hidden.bs.modal', function() {
				self.$el.remove();
			});
			this.$el.show();
			var schema = JSON.parse(this.model.extra);
			this.renderInputs(schema, 0, this.$el.find(".extra"), {});
			this.uploadifyButtons();
		},
		renderInputs: function(schema, indent, container, item) {
			//Render ID field

			for (var i = 0; i < schema.length; i++) {
				if (schema[i].type == "object") {
					container.append(itemFormTemplate({
						model: schema[i],
						padding: (indent + 1) * 20,
					}));
					var object = typeof schema[i].object == "string" ? JSON.parse(schema[i].object) : schema[i].object;
					var itemObject = item !== undefined ? item[schema[i].name] : undefined;
					this.renderInputs(object, indent + 1, container.find(".data-group:last"), itemObject);
				} else {

					var value = item !== undefined ? item[schema[i].name] : "";
					if (value === "" && item === undefined) {
						value = schema[i].defaultValue;
					}

					// If type is select, get select options
					var select = [];
					if (schema[i].type == "select") {
						select = JSON.parse(schema[i].object);
						//If the select is pointing to a briefcase folder
						if (select.briefcase !== undefined) {
							var folder = select.briefcase.folder;
							var nameAttr = select.briefcase.name;
							var valueAttr = select.briefcase.value;
							folder = _.find(CurrentApp.getModel().get("briefcase"), function(o) {
								return o.shorthand === folder;
							}).contents;
							select = [];
							for (var j = 0; j < folder.length; j++) {
								select.push({
									name: folder[j][nameAttr],
									value: folder[j][valueAttr]
								});
							}
							console.log(select);
						}
						//Fill in the value, if it's an edit not a new item
						if (value !== "") {
							for (var j = 0; j < select.length; j++) {
								if (value == select[j].value) {
									select[j].selected = true;
								}
							}
						}
					}
					// If type is image, get required dimensions
					var dimensions = [];
					if (schema[i].type == "image") {
						dimensions = JSON.parse(schema[i].object);
					}
					container.append(itemFormTemplate({
						model: schema[i],
						value: value,
						select: select,
						dimensions: dimensions
					}));
				}
			}
		},
		getValues: function(container) {
			var obj = {};
			var dataHolders = container.children(".form-group");
			for (var i = 0; i < dataHolders.length; i++) {
				var el = $(dataHolders[i]).children(".dataHolder");
				if (el.is("input") || el.is("select")) {
					obj[el.attr("data-name")] = el.val();
					try {
						obj[el.attr("data-name")] = JSON.parse(obj[el.attr("data-name")]);
					} catch(err) {};
				} else {
					//It's a div, containing an inner structure
					obj[el.attr("data-name")] = this.getValues(el);
				}
			}
			return obj;
		},
		uploadifyButtons: function() {
            var counter = 0;
            this.$el.find(".uploadBtn").each(function() {
                $(this).attr("id", "upload" + counter);
                var container = $(this).parent();
                $("#upload" + counter++).uploadifive({
                    buttonClass: "btn btn-warning",
                    buttonText: "Upload",
                    width: 80,
                    formData: {},
                    onUploadStart: function(file) {},
                    multi: false,
                    uploadScript: globalConfig.api + CurrentApp.getID() + '/upload?admin_token=' + Admin.getToken(),
                    onUploadComplete: function(file, data, response) {
                        /* Parse data to json */
                        data = $.parseJSON(data);
                        data = {
							image: data.file_url
						};


                        $.ajax({
			                method : "POST",
			                url: globalConfig.api + CurrentApp.getID() + "/processimage/standard",
			                data: {
			                	data: data
			                },
			                success: function(r) {
			                	console.log(r)
			                	container.find("input").val(JSON.stringify(r));
			                }   
			            });

                        // if (container.find("input").attr("data-type") == "image") {
                        //     var img = new Image();
                        //     img.onload = function() {
                        //         var input = container.find("input");
                        //         var reqWidth = input.attr("data-width");
                        //         var reqHeight = input.attr("data-height");
                        //         if (img.width == reqWidth && img.height == reqHeight) {
                        //             container.removeClass("has-error");
                        //             input.val(data.file_url);
                        //         } else {
                        //             container.addClass("has-error");
                        //         }
                        //     };
                        //     img.src = data.file_url;
                        // } else {
                        //     container.find("input").val(data.file_url);
                        // }
                    }
                });
            });
        },
		events: {
			"click .save": "save"
		},
		save: function() {
			var self = this;
			var data = this.getValues(this.$el.find(".extra"));
			var userID = this.$(".userID").val();
			var roundID = this.$(".roundID").val();
			var self = this;
            $.ajax({
                method : "POST",
                url: globalConfig.api + CurrentApp.getID() + "/entries",
                data: {
                	data: data,
                	uploader_id: userID,
                	round: roundID,
                	category: 0
                },
                success: function() {
					self.$el.find("#ugcEntryModal").modal("hide");
					self.trigger("newEntry");
                }   
            });
		}
	});

	return UgcEntryWizard;
});