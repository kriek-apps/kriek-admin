/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'hbs!templates/dataReset',
    'collections/reportField',
    'views/dataResetRow',
    'currentApp'
], function($, _, Backbone, template, ReportFieldCollection, DataResetRowView, CurrentApp) {
    'use strict';

    var DataresetView = Backbone.View.extend({
        template: template,
        tagName: "div",
        className: "dataReset container",
        initialize: function() {
            this.collection = new ReportFieldCollection({
                appId: this.model.get('id'),
                reportId: 1
            });
            this.collection.on("reset change add remove", function() {
                this.render();
            }, this);
            this.collection.fetch({
                reset: true
            });
        },
        render: function() {
            this.$el.html(this.template({
                model: CurrentApp.getModel().toJSON()
            }));
            this.collection.each(function(model) {
                var row = new DataResetRowView({
                    model: model,
                    appId : this.model.get("id")
                });
                this.$el.append(row.$el);
            }, this);
        },
        events: {
            "click .resetActivity" : "resetActivity"
        },
        resetActivity: function() {
            var self = this;
            $.ajax({
                method : "DELETE",
                url: globalConfig.api + CurrentApp.getID() + "/activity",
                success: function() {
                    var el = self.$el.find(".resetActivity").attr("disabled", "disabled").text("Done");
                    setTimeout(function() {
                        el.fadeOut();
                    }, 800);
                }   
            })
        }
    });

    return DataresetView;
});