define([
	'jquery',
	'underscore',
	'backbone',
	'facebooksdk',
	'router',
	'views/main',
	'admin',
	'spotlight'
], function($, _, Backbone, FB, Router, MainView, Admin, Spotlight) {

	var App = {

		mainView: {},
		initialize: function() {

			//Backbone.emulateJSON = true;

			Admin.init(function() {
				/*	*/
				FB.init({
					appId: '429694133805914',
					status: true, // check login status
					cookie: true, // enable cookies to allow the server to access the session
					xfbml: true // parse XFBML
				});
				this.mainView = new MainView();
				Router.setMainView(this.mainView);
				$("body").append(this.mainView.el);
				Backbone.history.start();
				Spotlight.initialize();
			});




		},
		getCookie: function(name) {
          var parts = document.cookie.split(name + "=");
          if (parts.length == 2) return parts.pop().split(";").shift();
        }

	};

	return App;
});