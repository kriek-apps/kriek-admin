define([
	'jquery',
	'underscore',
	'backbone',
	'views/spotlight'
], function($, _, Backbone, SpotlightView) {

	/**
	 * Awesome object that makes navigation in the admin a super fun experience
	 * @type {Object}
	 */
	var Spotlight = {

		initialize: function() {

			this.view = new SpotlightView();
			$("body").append(this.view.$el);
			var self = this;
			$(document).keydown(function(event) {
				if (!$(document.activeElement).is("input") && !$(document.activeElement).is("textarea") && !$(document.activeElement).attr("contenteditable") && !event.ctrlKey && !event.altKey && !event.metaKey) {
					self.view.show();
					self.view.$el.find("input").focus();
				}
			});

			$(document).click(function() {
				self.view.hide();
			});
		}
	};

	return Spotlight;

});