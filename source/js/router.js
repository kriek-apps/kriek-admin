/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'views/appList',
    'views/appDetails',
    'models/app',
    'views/sidebar',
    'views/reports',
    'views/settingsGeneral',
    'views/settingsModules',
    'views/settingsLanguage',
    'views/settingsImage',
    'views/information',
    'views/ugc',
    'views/quiz',
    'views/notifications',
    'views/devToolsQuery',
    'views/login',
    'views/settingsFacebook',
    'views/dataReset',
    'admin',
    'views/errorlog',
    'currentApp',
    'views/discussion',
    'views/discussionSettings',
    'views/contentUploader',
    'views/folderManager',
    'views/ticker',
    'views/todos',
    'views/insights',
    'views/insightsAnalyze',
    'views/insightsBehavior',
    'views/sweepstakes'
], function($, _, Backbone, AppListView, AppDetailsView, AppModel, SidebarView, ReportsView, SettingsGeneralView, SettingsModulesView, SettingsLanguageView, SettingsImageView, InformationView, UGCView, QuizView, NotificationsView, QueryDevToolsView, LoginView, SettingsFacebookView, DataResetView, Admin, ErrorLogView, CurrentApp, DiscussionView, DiscussionSettingsView, ContentUploaderView, FolderManagerView, TickerView, ToDosView, InsightsView, InsightsAnalyzeView, InsightsBehaviorView, SweepstakesView) {
    'use strict';

    var Router = Backbone.Router.extend({

        routes: {
            "login": "login",
            ":appid": "appMainPage",
            ":appid/information": "information",
            ":appid/ugc": "ugc",
            ":appid/discussion": "discussionGeneral",
            ":appid/discussion/reply": "discussionReply",
            ":appid/discussion/reply/:questionid": "discussionQuestion",
            ":appid/discussion/settings": "discussionSettings",
            ":appid/quiz": "quiz",
            ":appid/settings": "settingsMain",
            ":appid/settings/general": "settingsGeneralNoAnchor",
            ":appid/settings/general:anchor": "settingsGeneral",
            ":appid/settings/facebook": "settingsFacebook",
            ":appid/settings/modules": "settingsModules",
            ":appid/settings/language": "settingsLanguageNoAnchor",
            ":appid/settings/language:anchor": "settingsLanguage",
            ":appid/settings/imageprocessing": "settingsImageProcessing",
            ":appid/reports": "reportsMainPage",
            ":appid/reports/:reportId": "report",
            ":appid/notifications": "notificationsMainPage",
            ":appid/notifications/:reportId": "notifications",
            ":appid/dev": "devMain",
            ":appid/dev/query": "devQuery",
            ":appid/dev/reset": "dataReset",
            ":appid/dev/errorlog": "errorLog",
            ":appid/briefcase": "briefcaseMain",
            ":appid/briefcase/settings": "contentManager",
            ":appid/briefcase/:folderId": "briefcaseFolder",
            ":appid/ticker": "ticker",
            ":appid/todos": "todos",
            ":appid/sweepstakes": "sweepstakes",
            ":appid/insights": "insights",
            ":appid/insights/overview": "insightsOverview",
            ":appid/insights/analyze": "insightsAnalyze",
            ":appid/insights/behavior": "insightsBehavior",
            "*path": "main"
        },
        mainView: {},
        currentAppModel: {},

        onBeforeRoute: function() {
            clearInterval(window.timer);
            clearInterval(window.timer2);
        },

        login: function() {
            this.onBeforeRoute();
            var view = new LoginView();
            $(".breadcrumb").html("");
            this.mainView.$el.find("#main").html(view.$el);

        },

        setMainView: function(view) {
            this.mainView = view;
        },

        main: function() {
            var appListView = new AppListView();
            this.mainView.$el.find("#main").html(appListView.$el);
            this.setBreadCrumbs([]);
        },

        appMainPage: function(appID) {
            window.location.hash += "/information";
        },

        reportsMainPage: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var reportsView = new ReportsView({
                    appId: appID
                });
                appDetailsView.$el.find("#content").html(reportsView.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Reports',
                    link: '#/' + this.currentAppModel.get('id') + '/reports'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        ticker: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var tickerView = new TickerView({
                    appId: appID
                });
                appDetailsView.$el.find("#content").html(tickerView.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Activity Feed',
                    link: '#/' + this.currentAppModel.get('id') + '/ticker'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        todos: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var todosView = new ToDosView();
                appDetailsView.$el.find("#content").html(todosView.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'ToDos',
                    link: '#/' + this.currentAppModel.get('id') + '/todos'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        sweepstakes: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new SweepstakesView();
                appDetailsView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Sweepstakes',
                    link: '#/' + this.currentAppModel.get('id') + '/sweepstakes'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        insights: function(appID) {
            window.location.hash += "/overview";
        },

        insightsOverview: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                appDetailsView.openInsights("overview");
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var insightsView = new InsightsView();
                appDetailsView.$el.find("#content").html(insightsView.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Insights',
                    link: '#/' + this.currentAppModel.get('id') + '/insights'
                }, {
                    name: 'Overview',
                    link: '#/' + this.currentAppModel.get('id') + '/insights/overview'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        insightsAnalyze: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                appDetailsView.openInsights("analyze");
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var insightsAnalyzeView = new InsightsAnalyzeView();
                appDetailsView.$el.find("#content").html(insightsAnalyzeView.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Insights',
                    link: '#/' + this.currentAppModel.get('id') + '/insights'
                }, {
                    name: 'Analyze',
                    link: '#/' + this.currentAppModel.get('id') + '/insights/analyze'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        insightsBehavior: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                appDetailsView.openInsights("behavior");
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new InsightsBehaviorView();
                appDetailsView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Insights',
                    link: '#/' + this.currentAppModel.get('id') + '/insights'
                }, {
                    name: 'User Behavior',
                    link: '#/' + this.currentAppModel.get('id') + '/insights/behavior'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        report: function(appID, reportID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var reportsView = new ReportsView({
                    appId: appID,
                    reportId: reportID
                });
                appDetailsView.$el.find("#content").html(reportsView.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Reports',
                    link: '#/' + this.currentAppModel.get('id') + '/reports'
                }, {
                    name: reportID,
                    link: '#/' + this.currentAppModel.get('id') + '/reports/' + reportID
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        settingsMain: function(appID) {
            window.location.hash = window.location.hash + "/general";
        },

        settingsGeneralNoAnchor: function(appID) {
            this.settingsGeneral(appID, "");
        },

        settingsGeneral: function(appID, anchor) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.openSettings("general");
                var view = new SettingsGeneralView({
                    model: this.currentAppModel,
                    anchor : anchor
                });

                this.mainView.$el.find("#content").html(view.$el);
                if (anchor !== "") {
                    anchor = anchor.replace(":", "");
                    $('a[name=' + anchor + "]").next().find("input").focus();
                }
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Settings',
                    link: '#/' + this.currentAppModel.get('id') + '/settings'
                }, {
                    name: 'General',
                    link: '#/' + this.currentAppModel.get('id') + '/settings/general'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        settingsModules: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.openSettings("modules");
                var view = new SettingsModulesView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Settings',
                    link: '#/' + this.currentAppModel.get('id') + '/settings'
                }, {
                    name: 'Modules',
                    link: '#/' + this.currentAppModel.get('id') + '/settings/modules'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        settingsFacebook: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.openSettings("facebook");
                var view = new SettingsFacebookView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Settings',
                    link: '#/' + this.currentAppModel.get('id') + '/settings'
                }, {
                    name: 'Facebook',
                    link: '#/' + this.currentAppModel.get('id') + '/settings/facebook'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        settingsLanguageNoAnchor: function(appID) {
            this.settingsLanguage(appID, "");
        },

        settingsLanguage: function(appID, anchor) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.openSettings("language");
                var view = new SettingsLanguageView({
                    model: this.currentAppModel,
                    anchor: anchor
                });
                this.mainView.$el.find("#content").html(view.$el);
                if (anchor !== "") {
                    anchor = anchor.replace(":", "");
                    $('a[name=' + anchor + "]").siblings("input").focus();
                }
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Settings',
                    link: '#/' + this.currentAppModel.get('id') + '/settings'
                }, {
                    name: 'Language',
                    link: '#/' + this.currentAppModel.get('id') + '/settings/language'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        settingsImageProcessing: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.openSettings("imageprocessing");
                var view = new SettingsImageView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Settings',
                    link: '#/' + this.currentAppModel.get('id') + '/settings'
                }, {
                    name: 'Image Processing',
                    link: '#/' + this.currentAppModel.get('id') + '/settings/imageprocessing'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        information: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new InformationView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Information',
                    link: '#/' + this.currentAppModel.get('id') + '/information'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        ugc: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new UGCView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'UGC',
                    link: '#/' + this.currentAppModel.get('id') + '/ugc'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        discussionGeneral: function() {
            window.location.hash = window.location.hash += "/reply";
        },

        discussionReply: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                appDetailsView.openDiscussion("reply");
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new DiscussionView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Discussion',
                    link: '#/' + this.currentAppModel.get('id') + '/discussion'
                }, {
                    name: 'Reply',
                    link: '#/' + this.currentAppModel.get('id') + '/discussion/reply'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        discussionQuestion: function(appID, questionID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                appDetailsView.openDiscussion("reply");
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new DiscussionView({
                    model: this.currentAppModel,
                    questionID: questionID
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Discussion',
                    link: '#/' + this.currentAppModel.get('id') + '/discussion'
                }, {
                    name: 'Reply',
                    link: '#/' + this.currentAppModel.get('id') + '/discussion/reply'
                }, {
                    name: 'Question #' + questionID,
                    link: '#/' + this.currentAppModel.get('id') + '/discussion/reply/' + questionID
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        discussionSettings: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                appDetailsView.openDiscussion("settings");
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new DiscussionSettingsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Discussion',
                    link: '#/' + this.currentAppModel.get('id') + '/discussion'
                }, {
                    name: 'Settings',
                    link: '#/' + this.currentAppModel.get('id') + '/discussion/settings'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        quiz: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new QuizView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Quiz',
                    link: '#/' + this.currentAppModel.get('id') + '/quiz'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        briefcaseMain: function(appID) {
            window.location.hash = window.location.hash + "/settings";
        },

        contentManager: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.showBriefcaseFolders();
                var view = new FolderManagerView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Briefcase',
                    link: '#/' + this.currentAppModel.get('id') + '/briefcase'
                }, {
                    name: 'Settings',
                    link: '#/' + this.currentAppModel.get('id') + '/briefcase/settings'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        briefcaseFolder: function(appID, folderID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                appDetailsView.openBriefcase(folderID);
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new ContentUploaderView({
                    model: this.currentAppModel,
                    id: folderID
                });
                this.mainView.$el.find("#content").html(view.$el);
                var folderName = CurrentApp.getModel().get("briefcase")[folderID].name;
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Briefcase',
                    link: '#/' + this.currentAppModel.get('id') + '/briefcase'
                }, {
                    name: folderName,
                    link: '#/' + this.currentAppModel.get('id') + '/briefcase/' + folderID
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        notificationsMainPage: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new NotificationsView({
                    appId: appID
                });
                appDetailsView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Notifications',
                    link: '#/' + this.currentAppModel.get('id') + '/notifications'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        notifications: function(appID, reportID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                var view = new NotificationsView({
                    appId: appID,
                    reportId: reportID
                });
                appDetailsView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Notifications',
                    link: '#/' + this.currentAppModel.get('id') + '/notifications'
                }, {
                    name: reportID,
                    link: '#/' + this.currentAppModel.get('id') + '/notifications/' + reportID
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });
        },

        devMain: function(appID) {
            window.location.hash = window.location.hash + "/query";
        },

        devQuery: function(appID) {
            this.onBeforeRoute();
            //if (!Admin.checkAuth()) return;
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.openDevTools("query");
                var view = new QueryDevToolsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Developer Tools',
                    link: '#/' + this.currentAppModel.get('id') + '/dev'
                }, {
                    name: 'Query Editor',
                    link: '#/' + this.currentAppModel.get('id') + '/dev/query'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },
        dataReset: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.openDevTools("reset");
                var view = new DataResetView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Developer Tools',
                    link: '#/' + this.currentAppModel.get('id') + '/dev'
                }, {
                    name: 'Data Reset',
                    link: '#/' + this.currentAppModel.get('id') + '/dev/reset'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        errorLog: function(appID) {
            this.onBeforeRoute();
            this.currentAppModel = new AppModel({
                id: appID
            });
            CurrentApp.setModel(this.currentAppModel);
            this.currentAppModel.on("change", function() {
                var appDetailsView = new AppDetailsView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#main").html(appDetailsView.$el);
                appDetailsView.openDevTools("errorlog");
                var view = new ErrorLogView({
                    model: this.currentAppModel
                });
                this.mainView.$el.find("#content").html(view.$el);
                this.setBreadCrumbs([{
                    name: 'Home',
                    link: '#'
                }, {
                    name: this.currentAppModel.get('name'),
                    link: '#/' + this.currentAppModel.get('id')
                }, {
                    name: 'Developer Tools',
                    link: '#/' + this.currentAppModel.get('id') + '/dev'
                }, {
                    name: 'Error Log',
                    link: '#/' + this.currentAppModel.get('id') + '/dev/errorlog'
                }]);
            }, this);
            this.currentAppModel.fetch({
                reset: true
            });

        },

        setBreadCrumbs: function(array) {
            var breadcrumbs = this.mainView.$el.find("ol.breadcrumb");
            breadcrumbs.html("");
            for (var i = 0; i < array.length; i++) {
                breadcrumbs.append("<li><a href='" + array[i].link + "'>" + array[i].name + "</a></li>");
            }
        },

        getCookie: function(name) {
            var parts = document.cookie.split(name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        },

        deleteCookie: function(name) {
            document.cookie = name + '=; expires=Thu, 01 Jan 1999 00:00:01 GMT; path=/admin/';
        },


    });
    var router = new Router();
    return router;
});