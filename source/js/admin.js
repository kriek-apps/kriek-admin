/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    'use strict';

    var Admin = Backbone.Model.extend({

        initialize: function() {
        },

        init: function(callback) {
            this.appendURLs();
            this.checkAuth(callback);
        },

        checkAuth: function(callback) {
            if (typeof this.getCookie("admin_token") === "undefined") {
                this.previousLocation = window.location.hash;
                window.location.hash = "/login";
                if (typeof callback !== "undefined") {
                    callback();
                }
            } else {
                this.token = this.getCookie("admin_token");
                this.me(function() {
                    if (typeof callback !== "undefined") {
                        callback();
                    }
                });
            }
        },

        me: function(callback) {
            var self = this;
            $.ajax({
                type: "GET",
                url: globalConfig.api +"auth/me",
                dataType: "json",
                success: function(r) {
                    self.set("name", r.data.name);
                    self.set("id", r.data.id);
                    if (typeof callback !== "undefined") {
                        callback();

                    }
                }
            });
        },

        login: function(credentials, callback) {
            credentials = JSON.stringify(credentials);
            var self = this;
            $.ajax({
                type: "POST",
                data: credentials,
                url: globalConfig.api + "auth/login",
                dataType: "json",
                contentType: "application/json",
                success: function(r) {
                    self.set("name", r.data.name);
                    self.set("id", r.data.id);
                    self.createCookie("admin_token", r.data.admin_token, 1);
                    if (typeof callback !== "undefined") {
                        callback();
                        self.trigger("adminChanged");
                    }
                },
                error: function(a,b,c){
                    console.log(a,b,c);
                }
            });
        },

        logout: function() {
            this.deleteCookie("admin_token");
            this.set("name", "");
            this.set("id", "");
            this.trigger("adminChanged");
            //window.location.hash = "/login";
            window.location.reload();
        },

        isLoggedIn: function() {
            return this.getToken() !== "";
        },

        isDeveloper: function() {
            return this.id === 1 && this.isLoggedIn();
        },

        appendURLs: function() {
            var self = this;
            $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
                options.url += '?admin_token=' + self.getToken();
                options.error = function(r) {
                    if (typeof self.getCookie("admin_token") === "undefined" && window.location.hash !== "#/login") {
                        clearInterval(window.timer);
                        clearInterval(window.timer2);
                        self.previousLocation = window.location.hash;
                        window.location.hash = "/login";
                    } else {
                        if (originalOptions.error !== undefined)
                            originalOptions.error();
                    }
                }
            });
        },

        createCookie: function(name, value, days) {
            var expires;
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "";
            }
            document.cookie = name + "=" + value + expires + ";path=/";
        },

        getToken: function() {
            if (typeof this.getCookie("admin_token") !== "undefined") {
                return this.getCookie("admin_token");
            } else {
                return "";
            }
        },

        getCookie: function(name) {
            var parts = document.cookie.split(name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        },
        deleteCookie: function(name) {
            document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
        },
        getData: function() {
            return {
                name: this.get("name"),
                id: this.get("id"),
                developer: this.isDeveloper(),
                loggedIn: this.isLoggedIn(),
                token: this.getToken()
            };
        }
    });
    var admin = new Admin();
    return admin;
});