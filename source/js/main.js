window.globalConfig = {
	api: "//cnvs.eu1.frbit.net/"
};
require.config({
	shim: {
		'facebooksdk': {
			exports: 'FB'
		},
		'backbone': {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		/*'underscore': {
			exports: '_'
		},*/
		"bootstrap": {
			deps: ["jquery"]
		},
		/*'handlebars': {
			exports: 'Handlebars'
		},*/
		/*'json2': {
			exports: 'JSON'
		},*/
		"uploadify": {
			deps: ["jquery"]
		},
		"datepicker": {
			deps: ["jquery"]
		}

	},

	paths: {
		facebooksdk: '//connect.facebook.net/en_US/all',
		jquery: '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min',
		//underscore: '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.1/underscore-min',
		underscore: 'vendor/lodash.underscore',
		backbone: '//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.0.0/backbone-min',
		//handlebars: '//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.0.0/handlebars.min',
		handlebars: 'vendor/Handlebars',
		hbs: 'vendor/hbs',
		bootstrap: '//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min',
		//json2: '//cdnjs.cloudflare.com/ajax/libs/json2/20121008/json2',
		json2: 'vendor/json2',
		i18nprecompile: 'vendor/i18nprecompile',
		uploadify: 'vendor/uploadify/jquery.uploadifive.min',
		datepicker: 'vendor/jquery.simple-dtpicker',

		async: 'vendor/plugins/async',
        font: 'vendor/plugins/font',
        goog: 'vendor/plugins/goog',
        image: 'vendor/plugins/image',
        json: 'vendor/plugins/json',
        propertyParser : 'vendor/plugins/propertyParser',
        md5 : 'vendor/md5'

	},

	hbs: {
		disableI18n: true,
		helperPathCallback: function(name) {
			return 'hbshelpers/' + name;
		}
	}
});

require(['app', 'facebooksdk'], function(App) {
	App.initialize();
});