/*global define*/

define([
    'underscore',
    'backbone',
], function (_, Backbone) {
    'use strict';

    var EntriesModel = Backbone.Model.extend({
        defaults: {
        },
        initialize : function() {
        }
    });

    return EntriesModel;
});