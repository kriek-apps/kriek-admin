/*global define*/

define([
	'underscore',
	'backbone',
], function(_, Backbone) {
	'use strict';

	var ReportModel = Backbone.Model.extend({
		defaults: {},
		initialize: function(data) {
			if (typeof this.collection != "undefined") {

			} else {
				this.url = globalConfig.api + data.appId + '/queries';
				this.appId = data.appId;
			}
		}
	});

	return ReportModel;
});