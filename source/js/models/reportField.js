/*global define*/

define([
    'underscore',
    'backbone',
], function (_, Backbone) {
    'use strict';

    var ReportFieldModel = Backbone.Model.extend({
        defaults: {
        }
    });

    return ReportFieldModel;
});