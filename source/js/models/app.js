/*global define*/

define([
	'underscore',
	'backbone'
], function(_, Backbone) {
	'use strict';

	var AppModel = Backbone.Model.extend({
		initialize: function() {
			if (typeof this.collection !== "undefined") {

			} else {

				this.url = globalConfig.api + 'apps/' + this.id;
			}
		},
		parse : function(obj) {
			if (typeof this.collection !== "undefined") {
				return obj;
			} else {

				return obj;
			}
			
		}

	});

	return AppModel;
});