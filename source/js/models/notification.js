/*global define*/

define([
	'underscore',
	'backbone',
], function(_, Backbone) {
	'use strict';

	var QuizModel = Backbone.Model.extend({
		defaults: {},
		initialize: function(data) {
			if (typeof this.collection != "undefined") {}
			else {

				this.url = globalConfig.api + data.appId + '/notifications';

			}
		}
	});

	return QuizModel;
});