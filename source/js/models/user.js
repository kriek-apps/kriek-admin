/*global define*/

define([
	'underscore',
	'backbone',
], function(_, Backbone) {
	'use strict';

	var UserModel = Backbone.Model.extend({
		defaults: {},
		initialize: function(data) {
			this.url = globalConfig.api + data.appId + "/users/" + this.id;
		}
	});

	return UserModel;
});
