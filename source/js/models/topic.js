/*global define*/

define([
	'underscore',
	'backbone',
	'currentApp'
], function(_, Backbone, CurrentApp) {
	'use strict';

	var TopicModel = Backbone.Model.extend({
		initialize: function() {
			if (typeof this.collection !== "undefined") {
			} else {
				this.url = globalConfig.api +CurrentApp.getID()+'/chat/topics';
			}
		},
		defaults: {}
	});

	return TopicModel;
});