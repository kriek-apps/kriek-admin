/*global define*/

define([
	'underscore',
	'backbone',
	'currentApp'
], function(_, Backbone, CurrentApp) {
	'use strict';

	var DiscussionModel = Backbone.Model.extend({
		initialize: function() {
			if (this.collection === undefined) {
				this.url = globalConfig.api + CurrentApp.getID() + "/chat/questions/" + this.id;
			}
		},
		defaults: {},
		parse: function(obj) {

			if (this.isNew()) {
				return obj;
			} else {
				return obj[0];
			}
		}
	});

	return DiscussionModel;
});