/*global define*/

define([
	'underscore',
	'backbone',
	'models/topic',
	'currentApp'
], function(_, Backbone, TopicModel, CurrentApp) {
	'use strict';

	var TopicCollection = Backbone.Collection.extend({
		model: TopicModel,
		initialize: function() {
			this.url = globalConfig.api + CurrentApp.getID() + "/chat/topics";
		}
	});

	return TopicCollection;
});