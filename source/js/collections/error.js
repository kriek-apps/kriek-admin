/*global define*/

define([
    'underscore',
    'backbone',
    'models/error',
    'currentApp'
], function (_, Backbone, ErrorModel, CurrentApp) {
    'use strict';

    var ErrorCollection = Backbone.Collection.extend({
        initialize: function() {
        	this.url = globalConfig.api + CurrentApp.getID() + "/errorlog"
        },
        model: ErrorModel

    });

    return ErrorCollection;
});