/*global define*/

define([
    'underscore',
    'backbone',
    'models/quiz'
], function (_, Backbone, QuizModel) {
    'use strict';

    var QuizCollection = Backbone.Collection.extend({
        model: QuizModel,
        initialize : function(appId) {
			this.url = globalConfig.api + appId + '/questions';
		},
		/*parse : function(response) {
			return response.avaiable;
		}*/
    });

    return QuizCollection;
});