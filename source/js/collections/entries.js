/*global define*/

define([
	'underscore',
	'backbone',
	'models/entries'
], function(_, Backbone, EntriesModel) {
	'use strict';

	var EntriesCollection = Backbone.Collection.extend({
		url: '',
		model: EntriesModel,
		initialize : function(appId) {
			this.url = globalConfig.api + appId + '/entries';
		}
	});

	return EntriesCollection;
});