/*global define*/

define([
    'underscore',
    'backbone',
    'models/discussion',
    'currentApp'
], function(_, Backbone, DiscussionModel, CurrentApp) {
    'use strict';

    var DiscussionCollection = Backbone.Collection.extend({
        model: DiscussionModel,
        initialize: function() {
            this.url = globalConfig.api + CurrentApp.getID() + "/chat/questions";
        },
        setTopic: function(id) {
            id = Number(id);
            if (id === 0) {
                this.url = globalConfig.api + CurrentApp.getID() + "/chat/questions";
            } else {
                this.url = globalConfig.api + CurrentApp.getID() + "/chat/questions/topics/" + id;
            }
        },
        

    });

    return DiscussionCollection;
});