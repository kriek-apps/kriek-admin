/*global define*/

define([
    'underscore',
    'backbone',
    'models/notification'
], function (_, Backbone, NotificationModel) {
    'use strict';

    var QuizCollection = Backbone.Collection.extend({
        model: NotificationModel,
        initialize : function(appId) {
			this.url = globalConfig.api + appId + '/notifications';
		},
		
    });

    return QuizCollection;
});
