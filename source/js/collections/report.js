/*global define*/

define([
	'underscore',
	'backbone',
	'models/report'
], function(_, Backbone, ReportModel) {
	'use strict';

	var ReportCollection = Backbone.Collection.extend({
		model: ReportModel,
		initialize: function(appId) {
			this.url = globalConfig.api + appId + '/queries';
		}
	});

	return ReportCollection;
});