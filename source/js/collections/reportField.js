/*global define*/

define([
	'underscore',
	'backbone',
	'models/reportField'
], function(_, Backbone, ReportFieldModel) {
	'use strict';

	var ReportFieldCollection = Backbone.Collection.extend({
		model: ReportFieldModel,
		initialize: function(data) {
			var url = globalConfig.api + data.appId + '/queries/'+data.reportId;
			this.url = url;
		},
		parse : function(response) {
			this.titles = response.titles;
			return response.data;
		}
	});

	return ReportFieldCollection;
});