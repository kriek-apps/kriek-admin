({
	appDir: "../",
	baseUrl: "js",
	mainConfigFile: 'main.js',
	removeCombined: true,
	dir: "../../build",
	modules: [{
		name: "main"
	}]
})