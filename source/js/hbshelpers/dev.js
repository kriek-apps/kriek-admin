define('hbshelpers/dev', ['handlebars'], function ( Handlebars ) {
  function dev (par, options) {
    
    if( window.location.host.indexOf("dev") > -1 ) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
  }
  Handlebars.registerHelper( 'dev', dev );
  return dev;
});