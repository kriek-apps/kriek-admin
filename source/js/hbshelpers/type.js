define('hbshelpers/type', ['handlebars'], function(Handlebars) {
    function type(lvalue, rvalue, options) {
        var t = whatIsIt(lvalue);
        if( t == rvalue ) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    }


    var stringConstructor = "test".constructor;
    var arrayConstructor = [].constructor;
    var objectConstructor = {}.constructor;
    function whatIsIt(object) {
        if (object === null) {
            return "null";
        }
        else if (object === undefined) {
            return "undefined";
        }
        else if (object.constructor === stringConstructor) {
            return "String";
        }
        else if (object.constructor === arrayConstructor) {
            return "Array";
        }
        else if (object.constructor === objectConstructor) {
            return "Object";
        }
        else {
            return "don't know";
        }
    }

    Handlebars.registerHelper('type', type);
    return type;
});