define('hbshelpers/jsonStringify', ['handlebars'], function(Handlebars) {
	function jsonStringify(variable) {
		var patt = /^"|"$/g;
		return JSON.stringify(variable, null, '\t').replace(patt, "");
	}
	Handlebars.registerHelper('jsonStringify', jsonStringify);
	return jsonStringify;
});