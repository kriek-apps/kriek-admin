define('hbshelpers/admin', ['handlebars', 'admin'], function ( Handlebars, Admin ) {
  function admin ( a, options ) {
    if(Admin.isDeveloper()) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
  }
  Handlebars.registerHelper( 'admin', admin );
  return admin;
});