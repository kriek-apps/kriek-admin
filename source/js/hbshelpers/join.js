define('hbshelpers/join', ['handlebars'], function(Handlebars) {
	function join(array, delimiter) {
		if (typeof array === "undefined") return "";
		else if (typeof array === "number") return array;
		else {
			if (typeof array !== "object") array = JSON.parse(array);
			return array.join(delimiter);
		}

		
	}
	Handlebars.registerHelper('join', join);
	return join;
});