define('hbshelpers/getFromArray', ['handlebars'], function( Handlebars ) {
	function getFromArray(array, index) {
		return array[index];
	}
	Handlebars.registerHelper('getFromArray', getFromArray);
	return getFromArray;
});