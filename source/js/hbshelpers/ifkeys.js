define('hbshelpers/ifkeys', ['handlebars'], function ( Handlebars ) {
  function ifkeys (obj, options ) {
    if(Object.keys(obj).length > 1) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
  }
  Handlebars.registerHelper( 'ifkeys', ifkeys );
  return ifkeys;
});