define('hbshelpers/replaceNewline', ['handlebars'], function(Handlebars) {
	function replaceNewline(variable) {
		return String(variable).replace(/\r?\n/g, '<br />');
	}
	Handlebars.registerHelper('replaceNewline', replaceNewline);
	return replaceNewline;
});