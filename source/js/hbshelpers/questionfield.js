define('hbshelpers/questionfield', ['handlebars'], function(Handlebars) {
    function questionfield(model, fields, options) {
        if (typeof model.question === "undefined") model.question = {};
        var str = "";
        for (var i = 0; i < fields.length; i++) {
            if (typeof model.question[fields[i]] === "undefined") model.question[fields[i]] = "";
            str += '<div class="form-group">' +
                        '<label>Extra field: ' + fields[i] + '</label>' +
                        '<input type="text" class="form-control"  data-key="question" data-subkey="' + fields[i] + '" value="' + model.question[fields[i]] + '" placeholder="Add extra field value">' +
                    '</div >';
        }
        return new Handlebars.SafeString(str);
    }
    Handlebars.registerHelper('questionfield', questionfield);
    return questionfield;
});