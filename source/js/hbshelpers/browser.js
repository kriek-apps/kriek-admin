define('hbshelpers/browser', ['handlebars'], function(Handlebars) {
	function browser(variable) {
		return detectBrowser(variable);
	}

	function detectBrowser(ua){
		var UA= ua;
		var temp;
		var browserVersion= UA.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
		if(browserVersion && (temp= UA.match(/version\/([\.\d]+)/i))!= null)
		browserVersion[2]= temp[1];
		browserVersion= browserVersion ? [browserVersion[1], browserVersion[2]] : "";
		return browserVersion;
	};

	Handlebars.registerHelper('browser', browser);
	return browser;
});