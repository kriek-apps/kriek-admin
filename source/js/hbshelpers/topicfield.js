define('hbshelpers/topicfield', ['handlebars'], function(Handlebars) {
    function topicfield(model, fields, options) {
        var str = "";
        if (model.data === undefined) model.data = {};
        for (var i = 0; i < fields.length; i++) {
            if (typeof model.data[fields[i]] === "undefined") model.data[fields[i]] = "";
            str += '<div class="form-group">' +
                        '<label>' + fields[i] + '</label>' +
                        '<input type="text" class="form-control"  data-key="question" data-subkey="' + fields[i] + '" value="' + model.data[fields[i]] + '" placeholder="Add extra field value">' +
                    '</div >';
        }
        return new Handlebars.SafeString(str);
    }
    Handlebars.registerHelper('topicfield', topicfield);
    return topicfield;
});