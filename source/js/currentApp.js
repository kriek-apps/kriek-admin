/*global define*/

define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    'use strict';

    var CurrentApp = {
        initialize : function() {
        },
        setModel : function(model) {
            this.model = model;
        },
        getModel : function() {
            return this.model;
        },
        getID : function() {
            return this.model.get("id");
        }
    };

    return CurrentApp;
});