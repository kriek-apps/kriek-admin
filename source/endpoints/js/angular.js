var url = '/api/endpoints';
var qiuckTest = true;
var admintoken = getQueryParams(document.location.search);
admintoken = "?admin_token="+admintoken.admin_token;
function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}


function Endpoints($scope,$http) {
  // $http.post('http://dev.kriekapps.com/api/auth/login',{"username":"kriek","password":"kriek"}).success(function(data) {
  //   admintoken = "?admin_token="+data.data.admin_token;
  //       $scope.load();
  //   });
	$scope.load = function(){
	  $http.get(url+admintoken).success(function(data) {
      console.log(data);
	      $scope.data = data;

        for (var i = data.length - 1; i >= 0; i--) {
          //var tempKeys = Object.keys(data[i]);
          data[i].method = [];
          data[i].users = [];
          data[i].actions = [];
          if(data[i].GET) {
            data[i].method.push("?GET");
            //
            if(data[i].GET.roles.admin) {
              data[i].users.push("?GET?admin");
              if(data[i].GET.roles.admin.before) {
                data[i].actions.push("?GET?before");
              }
              if(data[i].GET.roles.admin.after) {
                data[i].actions.push("?GET?after");
              }
            }
            if(data[i].GET.roles.user) {
              data[i].users.push("?GET?user");
              if(data[i].GET.roles.user.before) {
                data[i].actions.push("?GET?before");
              }
              if(data[i].GET.roles.user.after) {
                data[i].actions.push("?GET?after");
              }
            }
            if(data[i].GET.roles.unknown) {
              data[i].users.push("?GET?unknown");
              if(data[i].GET.roles.unknown.before) {
                data[i].actions.push("?GET?before");
              }
              if(data[i].GET.roles.unknown.after) {
                data[i].actions.push("?GET?after");
              }
            }
            //
          }
          if(data[i].POST) {
            data[i].method.push("?POST");
            //
            if(data[i].POST.roles.admin) {
              data[i].users.push("?POST?admin");
              if(data[i].POST.roles.admin.before) {
                data[i].actions.push("?POST?before");
              }
              if(data[i].POST.roles.admin.after) {
                data[i].actions.push("?POST?after");
              }
            }
            if(data[i].POST.roles.user) {
              data[i].users.push("?POST?user");
              if(data[i].POST.roles.user.before) {
                data[i].actions.push("?POST?before");
              }
              if(data[i].POST.roles.user.after) {
                data[i].actions.push("?POST?after");
              }
            }
            if(data[i].POST.roles.unknown) {
              data[i].users.push("?POST?unknown");
              if(data[i].POST.roles.unknown.before) {
                data[i].actions.push("?POST?before");
              }
              if(data[i].POST.roles.unknown.after) {
                data[i].actions.push("?POST?after");
              }
            }
            //
          }
          if(data[i].PUT) {
            data[i].method.push("?PUT");
            //
            if(data[i].PUT.roles.admin) {
              data[i].users.push("?PUT?admin");
              if(data[i].PUT.roles.admin.before) {
                data[i].actions.push("?PUT?before");
              }
              if(data[i].PUT.roles.admin.after) {
                data[i].actions.push("?PUT?after");
              }
            }
            if(data[i].PUT.roles.user) {
              data[i].users.push("?PUT?user");
              if(data[i].PUT.roles.user.before) {
                data[i].actions.push("?PUT?before");
              }
              if(data[i].PUT.roles.user.after) {
                data[i].actions.push("?PUT?after");
              }
            }
            if(data[i].PUT.roles.unknown) {
              data[i].users.push("?PUT?unknown");
              if(data[i].PUT.roles.unknown.before) {
                data[i].actions.push("?PUT?before");
              }
              if(data[i].PUT.roles.unknown.after) {
                data[i].actions.push("?PUT?after");
              }
            }
            //
          }
          if(data[i].DELETE) {
            data[i].method.push("?DELETE");
            //
            if(data[i].DELETE.roles.admin) {
              data[i].users.push("?DELETE?admin");
              if(data[i].DELETE.roles.admin.before) {
                data[i].actions.push("?DELETE?before");
              }
              if(data[i].DELETE.roles.admin.after) {
                data[i].actions.push("?DELETE?after");
              }
            }
            if(data[i].DELETE.roles.user) {
              data[i].users.push("?DELETE?user");
              if(data[i].DELETE.roles.user.before) {
                data[i].actions.push("?DELETE?before");
              }
              if(data[i].DELETE.roles.user.after) {
                data[i].actions.push("?DELETE?after");
              }
            }
            if(data[i].DELETE.roles.unknown) {
              data[i].users.push("?DELETE?unknown");
              if(data[i].DELETE.roles.unknown.before) {
                data[i].actions.push("?DELETE?before");
              }
              if(data[i].DELETE.roles.unknown.after) {
                data[i].actions.push("?DELETE?after");
              }
            }
            //
          }
        };

	  });
	};

	$scope.load();
  $scope.orderProp = 'collection';
  $scope.cell = function(obj){
  	if(obj) {
  		return "glyphicon glyphicon-pencil";
  	}
  	return "glyphicon glyphicon-plus";
  };
  $scope.openModal = function(obj,method){
  	$scope.temp = obj;
		$scope.modal = obj.collection;
		$scope.method = method;
    $scope.extractParams(obj.id);

		obj = obj[method];
		if(obj == null) { obj = {}; }
		editor.set(obj);
    $('#test_results').html('');
		$('#myModal').modal('show');
  };
  $scope.changeEndpoint = function(obj){
  		if(obj == null) {
  			obj = {"endpoint":"","GET":"","POST":"","PUT":"","DELETE":""};
  		}
  		$scope.temp = obj;
  		$scope.temp.name = obj.collection;
		$('#addEnd').modal('show');
  };
  $scope.addJSON = function(){
  		$scope.temp.name = $scope.temp.collection;
  		$scope.temp[$scope.method] = editor.get();
  		$scope.save();
  };
  $scope.save = function(){
  	if($scope.temp.id != null) {
      $scope.temp.params=$scope.variables;
      console.log($scope.temp);

      for (var key in $scope.temp) {
        if($scope.temp[key] == null) {
          $scope.temp[key] = '';
        }
      }

  		$http.put(url+"/"+$scope.temp.id+admintoken,$scope.temp).success(function(data) {
	      $scope.load();
	      $('#myModal').modal('hide');
	      $('#addEnd').modal('hide');
	  });
  	} else {
  		$http.post(url+admintoken,$scope.temp).success(function(data) {
	      $scope.load();
	      $('#addEnd').modal('hide');
	  });
  	}
  };

  $scope.openDelete = function(id){
  		$scope.id = id;
  		$('#Delete').modal('show');
  }

  $scope.delete = function(id){
  		$http.delete(url+"/"+$scope.id+admintoken).success(function(data) {
	      $scope.load();
	      $('#Delete').modal('hide');
	  });
  }
// new code
  $scope.testEndpoint = function(method){
    //console.log($scope.temp[method]);
    $scope.temp.params=$scope.variables;
    $('#test_results').html('');
    runTest($scope.temp,method);
  }
  $scope.extractParams = function(id){
    $scope.variables={};
      var poz;
      for (var i = $scope.data.length - 1; i >= 0; i--) {
        if($scope.data[i].id == id) {
          poz = i;
        }
      };

      var params = $scope.data[poz].collection;
      var temp = ['GET','POST','PUT','DELETE'];

      for(var i=0; i<temp.length; i++) {
        if($scope.data[poz][temp[i]] != null) {
          for (var key in $scope.data[poz][temp[i]].roles) {
            params += " " + $scope.data[poz][temp[i]].roles[key].sql;
          }
        }
      }

      params += " ";

      var strStart = 0;
      var variables = {};
      for (var i = 0; i<params.length; i++) {
        if(params.charAt(i) == ":") {
          strStart = i+1;
        }

        if(params.charAt(i) == " " || params.charAt(i) == "/") {
          if(strStart != 0) {
            if(!variables[params.substring(strStart,i)]) {
              if($scope.data[poz].params != null) {
                variables[params.substring(strStart,i)] = $scope.data[poz].params[params.substring(strStart,i)];
              } else {
                variables[params.substring(strStart,i)] = undefined;
              }
            }
            strStart = 0;
          }
        }
      };

      $scope.variables=variables;
  }
}