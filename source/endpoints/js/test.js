
	$( document ).ready(function() {
	    //init();
	});

	function getQuery(key){
        var temp = location.search.match(new RegExp(key + "=(.*?)($|\&)", "i"));
        if(!temp) return;
        return temp[1];
    }

		var baseurl = '/api/';
		var debug = false;
		var endpointsData;
		var progress = {
			all:0,
			current:0
		}

		var params = {
			app_id:446088798809557
		}
		var roles = {
			admin: {
				admin_token: getQuery('admin_token')
			},
			user: {
				access_token: ""
			},
			unknowm: {}
		};

		function init() {
			$.ajax({
			  type: "GET",
			  url: baseurl+"endpoints?admin_token="+roles.admin.admin_token
			}).done(function( data ) {
			    endpointsData = data;
			    runTest();
			 });
		}

		function runTest(obj,method) {
			if(qiuckTest) {
				endpointsData=new Array(obj);
			} else {
				$('#progress-bar').css('width',0);
				progress.all = 0;
				progress.current = 0;
				$('#results').html('');
				rendererTable();
			}

			roles.user.access_token = $("#access_token").val();
			if(roles.user.access_token == "") {
				alert("access token missing!");
				return;
			}

			for (var i =0;i<endpointsData.length;i++) {
				var tempUrl = insertUrlParam(baseurl+endpointsData[i]["collection"],i);

				if(qiuckTest) {
					for (var key in endpointsData[i][method]['roles']) {
						endpoint(method,tempUrl,{data:endpointsData[i]["params"],queryData:roles[key]},i,key);
					}
					return;
				}

				if(endpointsData[i]["POST"] != null) {
					for (var key in endpointsData[i]["POST"]['roles']) {
						endpoint('POST',tempUrl,{data:endpointsData[i]["params"],queryData:roles[key]},i,key);
					}
				} 
				if(endpointsData[i]["PUT"] != null) {
					for (var key in endpointsData[i]["PUT"]['roles']) {
						endpoint('PUT',tempUrl,{data:endpointsData[i]["params"],queryData:roles[key]},i,key);
					}
				} 
				if(endpointsData[i]["GET"] != null) {
					for (var key in endpointsData[i]["GET"]['roles']) {
						endpoint('GET',tempUrl,{data:endpointsData[i]["params"],queryData:roles[key]},i,key);
					}
				} 
				if(endpointsData[i]["DELETE"] != null) {
					for (var key in endpointsData[i]["DELETE"]['roles']) {
						//progress.all++;
					   //endpoint('DELETE',tempUrl,{data:endpointsData[i]["params"],queryData:roles[key]},i);
					}
				}
			};
		}

		//endpoint('GET',insertParam(baseurl+endPoint),{data:{test:3},queryData:{test:3}});

		function endpoint(mode,url,sendData,id,role){
			progress.all++;
			var request = {};

			if(sendData.queryData) {
				var temp = sendData.queryData;
				url += '?';
				for (var key in temp) {
				   url += key+'='+temp[key]+'&';
				}

				url = url.substr(0,url.length-1);
			}

			if(debug) {
				if(temp) {
					url += '&debug';
				} else {
					url += '?debug';
				}
			}

			request.url = url;
			request.type = mode;

			if(mode!="GET") {
				request.contentType = 'application/json';
				request.data = JSON.stringify(sendData.data);
			} else {
				request.data = sendData.data;
			}

			if(qiuckTest) {
				request.success = function (data) {
					var result = $('<div>');
					result.html('<h3>'+role+':</h3><div id="json_result_'+role+'"></div>');
					$('#test_results').append(result);

					
					var container = document.getElementById("json_result_"+role);
        			var editor = new jsoneditor.JSONEditor(container);
        			editor.set(data);
				}

				request.error = function (data) {
					var result = $('<div>');
					result.html('<h3>'+role+':</h3><div>'+data.responseText+'</div>');
					$('#test_results').append(result);
				}
			} else {
				request.success = function (data) {
					var result = $('<span style="color:green" class="glyphicon glyphicon-ok-sign"></span>');
						if(mode == "GET") {
							if($.isArray(data)) {
								if(data.length == 0) {
									result = $('<span style="color:orange" class="glyphicon glyphicon-question-sign"></span>');
								}
								result.attr('title',role+'-GET:array()-'+data.length+' results');
							} else {
								if(data==null || data==undefined || data=="") {
									result = $('<span style="color:red" class="glyphicon glyphicon-remove-sign"></span>');
								}
								else if(data!=null && typeof(data)=="object") {
									if(Object.keys(data).length == 0) {
										result = $('<span style="color:orange" class="glyphicon glyphicon-question-sign"></span>');
									}
										result.attr('title',role+'-GET:'+typeof(data)+'-'+Object.keys(data).length+' results');
								} else {
									var result = $('<span style="color:red" class="glyphicon glyphicon-remove-sign"></span>');
			        				result.attr('title',role+'-'+mode+'-'+data);
								}
							}
						} else {
							if(data==null || data==undefined || data=="") {
									result = $('<span style="color:red" class="glyphicon glyphicon-remove-sign"></span>');
								}
							else if(data.status == "error") {
								var result = $('<span style="color:red" class="glyphicon glyphicon-remove-sign"></span>');
			        			result.attr('title',role+'-'+mode+'-'+data);
							} else {
								result.attr('title',role+' '+mode+':OK');
							}
						}
					result.click(function() {
					  $('#myModal #myModalLabel').html(mode + " - " + request.url);
					  $('#myModal .modal-body').html(JSON.stringify(data));
					  $('#myModal').modal('show');
					});
					$('#'+mode+'_'+endpointsData[id]["id"]).append(result);
					showProcess();
		        };

		        request.error = function (data) {
		        	var result = $('<span style="color:red" class="glyphicon glyphicon-remove-sign"></span>');
		        	result.attr('title',role+'-'+mode+'-'+data.responseText);
		        	result.click(function() {
					  $('#myModal #myModalLabel').html(mode + " - " + request.url);
					  $('#myModal .modal-body').html(data.responseText);
					  $('#myModal').modal('show');
					});
					$('#'+mode+'_'+endpointsData[id]["id"]).append(result);
					showProcess();
		        };
		    }

			$.ajax(request);
		}

		function insertUrlParam(url,id){
			var temp = url.split('/');
			for (var i = 0; i<temp.length; i++) {
				if(temp[i].toString().substr(0,1) == ":") {
					var tempStr = temp[i].substr(1,temp[i].length);
					if(endpointsData[id]["params"] != null) {
						if(params[tempStr] != undefined) {
							temp[i]=params[tempStr];
						}

						if(endpointsData[id]["params"][tempStr] != undefined) {
							temp[i]=endpointsData[id]["params"][tempStr];
						}
					} else {
						temp[i]=params[tempStr];
					}
				}
			};
			return temp.join('/');
		}

		function rendererTable(){
			for (var i =0; i<endpointsData.length; i++) {
				var row = $('<tr>');
				row.append($('<td style="text-align:left"><button type="button" class="btn btn-default btn-xs">'+endpointsData[i]["collection"]+'</button></td>'));
				row.append($('<td>').attr('id','GET_'+endpointsData[i]["id"]));
				row.append($('<td>').attr('id','POST_'+endpointsData[i]["id"]));
				row.append($('<td>').attr('id','PUT_'+endpointsData[i]["id"]));
				row.append($('<td>').attr('id','DELETE_'+endpointsData[i]["id"]));
				$('#results').append(row);
			};
		}

		function showProcess(){
			progress.current++;
			$('#progress-bar').css('width',(progress.current/progress.all)*100+'%');

			if(progress.current/progress.all == 1){
				alert('The endpoint check has finished!');
			}
		}