<!doctype html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://dev.kriekapps.com/admin/favicon.ico">
    	<title>Check Endpoints</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="js/test.js"></script>
<script>
	var qiuckTest = false;
</script>
	<style>
	body{
		padding: 10px;
	}
	td{
		text-align: center;
	}
	table{
		border: solid 2px #ddd;
	}
	</style>
</head>
<body>
	<p><input placeholder="access_token" id="access_token" class="form-control" type="text"></p>
<p style="text-align:center"><button type="button" onclick="init()" class="btn btn-info">Run test</button></p>
	<div class="progress progress-striped active">
  <div class="progress-bar" id="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">
    <span class="sr-only"></span>
  </div>
</div>

    <table id="tbl" class="table table-condensed">
      <thead>
        <tr style="background-color:grey;color:white">
            <th>Endpoint</th>
            <th style="text-align:center">GET</th>
            <th style="text-align:center">POST</th>
            <th style="text-align:center">PUT</th>
            <th style="text-align:center">DELETE</th>
        </tr>
      </thead>
      <tbody id="results"></tbody>
    </table>


 <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="overflow-x:hidden;" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body" style="overflow-x:hidden;">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</body>
</html>