<!DOCTYPE HTML>
<html ng-app>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://dev.kriekapps.com/admin/favicon.ico">
    <title>Endpoint designer</title>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://jsoneditoronline.org/lib/jsoneditor/jsoneditor-min.css">
    <script type="text/javascript" src="http://jsoneditoronline.org/lib/jsoneditor/jsoneditor-min.js"></script>
    <script type="text/javascript" src="http://jsoneditoronline.org/lib/ace/ace-min.js"></script>
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
    <script src="js/angular.js"></script>
    <script src="js/test.js"></script>
    <style>
    ul.menu{
      /*top:600px !important;*/
    }
    </style>
</head>
<body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/admin"><img src="http://dev.kriekapps.com/admin/img/kriek_logo_white.png"></a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/admin">Endpoint Manager</a>
        </div>
      </div>
    </div>

<div class="container" style="margin-top:80px;" ng-controller="Endpoints"> 

<div class="row" style="margin-bottom:10px;">
  <div class="col-lg-4">
    <input ng-model="searchText" class="form-control" placeholder="Search..">
  </div>
  <div class="col-lg-3">
    <button type="button" ng-click="changeEndpoint(null)" class="btn btn-primary">Add new endpoint <span class="glyphicon glyphicon-plus"></span></button> 
    <button type="button" onclick="window.open('checkendpoints.php'+admintoken);" class="btn btn-warning">Test endpoints</button> 
  </div>
</div>

    <table id="tbl" class="table table-condensed">
      <thead>
        <tr style="background-color:grey;color:white">
            <th>No.</th>
            <th>Endpoint</th>
            <th style="text-align:center">GET</th>
            <th style="text-align:center">POST</th>
            <th style="text-align:center">PUT</th>
            <th style="text-align:center">DELETE</th>
            <th style="text-align:center"></th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="row in data | orderBy:orderProp | filter:searchText">
            <td>#{{$index+1}}</td>
            <td><button type="button" ng-click="changeEndpoint(row)" class="btn btn-default btn-xs">{{row.collection}}</button></td>
            <td style="text-align:center"><span style="cursor:pointer" ng-click="openModal(row,'GET')" ng-class="cell(row.GET)"></span></td>
            <td style="text-align:center"><span style="cursor:pointer" ng-click="openModal(row,'POST')" ng-class="cell(row.POST)"></span></td>
            <td style="text-align:center"><span style="cursor:pointer" ng-click="openModal(row,'PUT')" ng-class="cell(row.PUT)"></span></td>
            <td style="text-align:center"><span style="cursor:pointer" ng-click="openModal(row,'DELETE')" ng-class="cell(row.DELETE)"></span></td>
            <td style="text-align:center"><span style="cursor:pointer;color:red" ng-click="openDelete(row.id)" class="glyphicon glyphicon-remove-circle"></span></td>
        </tr>
      </tbody>
    </table>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">{{modal}} - {{method}}</h4>
        </div>
        <div class="modal-body">

          <div id="jsoneditor" style="width: 100%;"></div>
          <button onclick="AddTemplate()" class="btn btn-default" type="button" style="margin-top:10px">Add template</button>
          <button onclick="ClearTemplate()" class="btn btn-danger" type="button" style="margin-top:10px">Clear</button>

        </div>

        <div style="padding:10px">
          <h4>Test variables</h4>
          <p ng-repeat="(key, data) in variables">
            <span>{{key}}</span> : <input class="form-control" type="text" ng-model="variables[key]">
          </p>
          <p><input placeholder="access_token" id="access_token" class="form-control" type="text"></p>
          <button type="button" ng-click="testEndpoint(method)" class="btn btn-info">Test</button>
          <div style="overflow-x:hidden" id="test_results"></div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" ng-click="addJSON()" class="btn btn-primary">Save changes</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


    <!-- Modal -->
  <div class="modal fade" id="addEnd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Endpoints</h4>
        </div>
        <div class="modal-body">

          <input type="text" ng-model="temp.name" class="form-control" placeholder="New endpoint.." autofocus>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" ng-click="save()" class="btn btn-primary">Save changes</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


      <!-- Modal -->
  <div class="modal fade" id="Delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" style="color:red">Delete <span class="glyphicon glyphicon-minus-sign"></span></h4>
        </div>
        <div class="modal-body">

          Are you sure?

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" ng-click="delete()" class="btn btn-danger">Delete</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>

    <script type="text/javascript" >
        // create the editor
        var container = document.getElementById("jsoneditor");
        var editor = new jsoneditor.JSONEditor(container);

function ClearTemplate() {
editor.set(null);
}
         function AddTemplate() {
                    var json = {
    "roles": {
        "admin": {
            "sql": "",
            "before": [
                {
                    "function": []
                }
            ],
            "after": [
                {
                    "function": []
                }
            ]
        }
    },
    "settings": {
        "type": "collection"
    }
};

            var tempJson = editor.get();
            if(tempJson.roles == undefined) {
            //tempJson.temp_role=json;
            editor.set(json);
          }
         }
    </script>

</body>
</html>