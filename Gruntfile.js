//Read out main.js file to get the app config. 
//The config then can be accessed as custom.appConfig
//App ID is needed to find the aws directory to push to
var fs = require("fs");
var path = require("path");

//Run Grunt module
module.exports = function(grunt) {

	// load all grunt tasks matching the `grunt-*` pattern
	// For more information and options see:
	// https://www.npmjs.org/package/load-grunt-tasks
	require('load-grunt-tasks')(grunt);

	var credentials = grunt.file.readJSON(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + '/.canvas/credentials.json');
	process.env.CLOUDFLARE_API_KEY = credentials.cloudflare.key;
	process.env.CLOUDFLARE_EMAIL = credentials.cloudflare.email;
	process.env.CLOUDFLARE_DOMAIN = credentials.cloudflare.domain;
	var LIVERELOAD_PORT = 35729,
		lrSnippet = require('connect-livereload')({
			port: LIVERELOAD_PORT
		}),
		mountFolder = function(connect, dir) {
			return connect.static(require('path').resolve(dir));
		};

	//Configure Grunt
	grunt.initConfig({
		credentials: grunt.file.readJSON(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + '/.canvas/credentials.json'),
		pkg: grunt.file.readJSON('package.json'),
		connect: {
			server: {
				options: {
					port: 9000,
					base: "source",
					hostname: "*",
					protocol: "https",
					open: {
						target: "https://dev.cnvs.io:9000"
					},
					livereload: true,
					key: grunt.file.read(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + '/.canvas/server.key').toString(),
					cert: grunt.file.read(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + '/.canvas/server.crt').toString()
				}
			},
			livereload: {
				options: {
					middleware: function(connect) {
						return [
							lrSnippet,
							mountFolder(connect, './')
						];
					}
				}
			}
		},
		// REQUIREJS
		requirejs: {
			compile: {
				options: {
					appDir: "source",
					baseUrl: "js",
					mainConfigFile: 'source/js/main.js',
					dir: "build",
					removeCombined: true,
					//findNestedDependencies: true, //nested require() calls are built into main.js too
					preserveLicenseComments: false,
					modules: [{
						name: "main"
					}],
					onBuildWrite: function(name, path, contents) {
						return contents.replace(/console\.log\("([^\\"]|\\.)*"\);/g, "");
					}
				}
			}
		},
		banner: "// Developed using Canvas by Kriek \n" +
			'// Last build: <%= grunt.template.today("yyyy-mm-dd HH:MM:ss") %>',
		//Create banner
		usebanner: {
			build: {
				options: {
					position: "top",
					banner: "<%= banner %>"
				},
				files: {
					src: ["build/js/main*.js"]
				}
			},
			defineTop: {
				options: {
					position: "top",
					banner: "define("
				},
				files: {
					src: ["source/js/manifest.js"]
				}
			},
			defineBottom: {
				options: {
					position: "bottom",
					banner: ");"
				},
				files: {
					src: ["source/js/manifest.js"]
				}
			}
		},

		// WATCH
		watch: {
			css: {
				files: '**/*.less',
				tasks: ['less'],
				options: {
					livereload: {
						port: 1390,
						key: grunt.file.read(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + '/.canvas/server.key').toString(),
						cert: grunt.file.read(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + '/.canvas/server.crt').toString()
					}
				}
			},
			all: {
				files: ["source/**/*"],
				options: {
					livereload: {
						port: 35729,
						key: grunt.file.read(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + '/.canvas/server.key').toString(),
						cert: grunt.file.read(process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + '/.canvas/server.crt').toString()
					}
				}
			}
		},

		//LESS
		less: {
			compile: {
				options: {
					"compress": true,
					"livereload": true
				},
				src: ["source/css/less/default.less", "source/css/less/variables.less", "source/**/*.less", "!source/css/less/main.less",  "source/css/less/main.less"],
				dest: "source/css/main.css"
			}
		},

		//MANIFEST for image preloading
		//https://www.npmjs.org/package/grunt-preload-manifest
		manifest: {
			options: {},
			images: {
				cwd: "source/",
				src: ['img/**/*.{jpg,jpeg,gif,png}', '!img/sprites/*'],
				dest: 'source/js/manifest.js',
				filter: 'isFile'
			},
		},


		//CLEAN
		clean: {
			build: {
				src: ["build/css/less", "build/js/**/*", "build/img/sprites", "!build/js/main.js", "!build/js/require.js", "!build/js/global.js"]
			},
			dist: {
				src: ["dist/"]
			},
			docs: {
				src: ["docs/"]
			}
		},

		//JSDOC
		//**
		//Resources:
		//https://help.github.com/articles/github-flavored-markdown
		//https://github.com/krampstudio/grunt-jsdoc
		//https://github.com/terryweiss/docstrap  //-> The theme and it's options
		jsdoc: {
			dist: {
				// src: ['source/js/**/*.js', "!source/js/vendor/**/*", "!source/js/hbshelpers/**/*", "README.md"],
				src: ["source/js/modules/**/*.js", "README.md"],
				options: {
					destination: 'docs',
					configure: "jsdoc.json",
					template: "node_modules/grunt-jsdoc/node_modules/ink-docstrap/template"
				}
			}
		},
		copy: {
			main: {
				files: [{
					expand: true,
					cwd: ".",
					src: ["build/**/*", "!build/data", "!build/data/*"],
					dest: "dist/",
					rename: function(dest, src) {
						var path = require('path');
						return path.join(dest, path.dirname(src.replace("build/", "")) + "/" + path.basename(src));
					}
				}]
			},
			swf: {
				files: [{
					cwd: ".",
					src: ["source/js/vendor/uploadify/uploadify.swf"],
					dest: "build/js/vendor/uploadify/uploadify.swf"
				}]
			}
		},
		//CLOUDFLARE
		//Flush Cloudlfare cache
		cloudflare: {
			/* Action, default is to purge the cache */
			a: 'fpurge_ts',
			/* CloudFlare credentials */
			/* API key */
			tkn: process.env.CLOUDFLARE_API_KEY,
			/* CloudFlare e-mail */
			email: process.env.CLOUDFLARE_EMAIL,
			/* Domain */
			z: process.env.CLOUDFLARE_DOMAIN
		},
		cacheBusting: {
			requirejs: {
				replace: ['build/index.html'],
				replacement: 'js/main',
				file: 'build/js/main.js'
			},
			css: {
				replace: ['build/index.html'],
				replacement: 'main.css',
				file: 'build/css/main.css'
			}
		},
		s3: {
			options: {
				accessKeyId: "<%= credentials.aws.key %>",
				secretAccessKey: "<%= credentials.aws.secret %>",
				bucket: "admin.cnvs.io",
				access: "public-read",
				region: "eu-west-1",
				cache: true,
			},
			dist: {
				cwd: "dist/",
				src: ["**", "!data/", "!data/**"],
				dest: ""
			},
			staging: {
				options: {
					bucket: "<%= credentials.aws.stagingBucket %>",
				},
				cwd: "build/",
				src: ["**", "!data/", "!data/**"],
				dest: ""
			},
			restore: {
				cwd: "backup/previous",
				src: ["**", "!data/", "!data/**"],
				dest: ""
			}
		},
		rewriteAssetUrls: {
			src: "source/js/main.js"
		},
		shell: {
			options: {
				stdout: true
			},
			gitAdd: {
				command: "git add --all"
			},
			gitCommit: {
				command: function() {
					var message = grunt.option('message') || "Automatically generated on deploy by grunt";
					return "git commit -m \"" + message + "\"";
				}
			},
			gitPush: {
				command: "git push --all origin"
			}
		},
		sprite: {
			all: {
				src: 'source/img/sprites/*.png',
				destImg: 'source/img/spritesheet.png',
				destCSS: 'source/css/less/sprites.less',
				imgPath: "../img/spritesheet.png"
			}
		}

	});
	//Tasks
	grunt.registerTask('default', 'watch');
	grunt.registerTask('build', ['preload', 'requirejs', 'usebanner:build', 'clean:build', "cacheBusting"]);
	grunt.registerTask('deploy', ["clean:dist", "copy:main", "s3:dist", "backup"]);
	grunt.registerTask('preload', ["manifest", "usebanner:defineTop", "usebanner:defineBottom"]);
	grunt.registerTask('server', ["connect", "watch"]);
	grunt.registerTask('docs', ["clean:docs", "jsdoc"]);
	grunt.registerTask('git', ["shell:gitAdd", "shell:gitCommit", "shell:gitPush"]);
	grunt.registerTask('restore', ['s3:restore']);

	//Register custom task "rewriteAssetUrls".
	//This looks at the main.js file and replaces all amazon s3 absolute urls with relative ones.
	grunt.registerTask("rewriteAssetUrls", "Rewrite json data urls from absolute to relative to be used at deploy", function() {
		var file = grunt.file.read("dist/js/main.js");
		file = file.split("/* grunt */");
		var replacing = file[1];
		replacing = replacing.replace("https://apps.cnvs.io/\"+require.appConfig.id+\"", "..");
		replacing = replacing.replace("https://apps.cnvs.io/\"+require.appConfig.id+\"", "..");
		replacing = replacing.replace("https://apps.cnvs.io/\"+require.appConfig.id+\"", "..");
		console.log(replacing);
		file[1] = replacing;
		file = file.join("/* grunt */");
		grunt.file.write("dist/js/main.js", file);
	});

	//Register custom task "cacheBusting".
	//Appends a hash to main.js and main.css so browsers won't use cached version after something changes.
	//This is a modified version of the grunt-cache-busting plugin at https://www.npmjs.org/package/grunt-cache-busting
	grunt.registerMultiTask("cacheBusting", "Append a hash to main.js and main.css to prevent caching", function() {
		var fs = require('fs'),
			path = require('path'),
			crypto = require('crypto'),
			gruntTextReplace = require('grunt-text-replace/lib/grunt-text-replace');
		//main.js
		var fileContents = grunt.file.read(this.data.file),
			hash = crypto.createHash('md5').update(fileContents).digest("hex"),
			outputDir = path.dirname(this.data.file),
			fileExtension = path.extname(this.data.file),
			replacementExtension = path.extname(this.data.replacement),
			replacementWithoutExtension = this.data.replacement.slice(0, this.data.replacement.length - replacementExtension.length);
		//dist/js .js  js/main dist/js/js/main-3e91ff80c8eb0909d8200a6a63f65b40.js
		//dist/css .css .css main dist/css/main-4899ec93ee527de48c7c8006af3fb608.css
		if (replacementExtension === "") {
			replacementExtension = ".js";
		}
		if (replacementWithoutExtension === "js/main") {
			replacementWithoutExtension = "main"
		}
		var outputFile = outputDir + path.sep + replacementWithoutExtension + "-" + hash + fileExtension;

		fs.rename(this.data.file, outputFile);

		var prefix = fileExtension === ".js" ? "js/" : "";

		gruntTextReplace.replace({
			src: this.data.replace,
			overwrite: true,
			replacements: [{
				from: this.data.replacement,
				to: prefix + replacementWithoutExtension + "-" + hash + replacementExtension
			}]
		});

	});

	//Register custom task "backup".
	//Creates a backup system whenever you deploy
	grunt.registerTask("backup", "Backup the current build folder on deploy", function() {
		var fs = require('fs-extra'),
			mkdirp = require('mkdirp'),
			rimraf = require('rimraf');

		//Make sure the backup directory exists
		if (fs.existsSync("./backup")) {} else {
			mkdirp.sync('./backup');
		}
		//Delete previous, if exists
		rimraf.sync("backup/previous");

		//If a current directory exists, rename it to previous
		if (fs.existsSync("backup/current")) {
			fs.copySync("backup/current", "backup/previous");
		}

		//Reset current directory and Copy build folder to backup/current
		rimraf.sync("backup/current");
		mkdirp.sync("backup/current");

		fs.copySync("dist", "backup/current");
	});

	//Register custom task "templates".
	//Creates a module which has all templates in the js/templates directory as dependencies and attached
	//to a main object
	grunt.registerTask("templates", "Create a module with templates appended", function() {
		//Get all templates in the templates directory
		var array = grunt.file.expand({
				cwd: "./source/js"
			},"templates/*");
		//read the contents of the file templates.js
		var moduleContents = grunt.file.read("./source/js/templates.js");
		var dependencies = "";
		for (var i = 0; i < array.length; i++) {
			array[i] = array[i].replace(".hbs", "");
			dependencies += "\t\"hbs!" + array[i] + "\"";
			if (i !== array.length - 1) dependencies += ",";
			dependencies += "\n"; 
		}
		moduleContents = moduleContents.replace(/(\/\* grunt-dependencies-start \*\/)((.|\n)*)(\/\* grunt-dependencies-end \*\/)/, function(match, p1, p2, p3, p4, offset, string) {
			return p1 + "\n" + dependencies + "\t" + p4;
		});
		var parameters = "";
		for (var i = 0; i < array.length; i++) {
			parameters += "\t" + array[i].split("/")[array[i].split("/").length -1];
			if (i !== array.length -1) parameters += ",";
			parameters += "\n";
		}
		moduleContents = moduleContents.replace(/(\/\* grunt-parameters-start \*\/)((.|\n)*)(\/\* grunt-parameters-end \*\/)/, function(match, p1, p2, p3, p4, offset, string) {
			return p1 + "\n" + parameters + "\t" + p4;
		});
		var templates = "";
		for (var i = 0; i < array.length; i++) {
			var name = array[i].split("/")[array[i].split("/").length -1];
			templates += "\tTemplates." + name + " = " + name + ";\n";
		}
		moduleContents = moduleContents.replace(/(\/\* grunt-templates-start \*\/)((.|\n)*)(\/\* grunt-templates-end \*\/)/, function(match, p1, p2, p3, p4, offset, string) {
			return p1 + "\n" + templates + "\t" + p4;
		});
		grunt.file.write("./source/js/templates.js", moduleContents);

	});


};